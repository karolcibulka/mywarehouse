<?php


class Front_model extends CI_model
{

    public function getPropertySettings($property_id){
        $data = $this->db->select('web')
            ->from('property_settings_web')
            ->where('property_id',$property_id)
            ->get()
            ->row_array();

        if(isset($data['web']) && !empty($data['web'])){
            return $data['web'];
        }
        return false;
    }
}