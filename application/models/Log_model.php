<?php
/**
 * Created by PhpStorm.
 * User: karol
 * Date: 2019-10-14
 * Time: 08:19
 */

class Log_model extends CI_Model
{
    public function saveResponse($data){
        $this->db->insert('payment_log',$data);
    }

    public function storeLog($data){
        $this->db->insert('dashboard_log',$data);
    }

    public function getLogs($start,$limit,$filters=array()){
        $data = $this->db->select('p.name as "property",u.email as "email",dl.item_id,dl.id,dl.method,dl.controller,dl.created_at')
            ->from('dashboard_log as dl')
            ->join('property as p','p.id = dl.property_id','left')
            ->join('users as u','u.id = dl.user_id','left')
            ->limit($limit,$start);
            if(!empty($filters)){
                foreach($filters as $key => $val){
                    if($val !== ''){
                        $data->where('dl.'.$key,$val);
                    }
                }
            }
            $result = $data->order_by('created_at','desc')
            ->get()->result_array();


        return $result;
    }

    public function getLogsCount($filters=array()){
        $data = $this->db->select('dl.*')
            ->from('dashboard_log as dl');
            if(!empty($filters)){
                foreach($filters as $key => $val){
                    if($val !== ''){
                        $data->where('dl.'.$key,$val);
                    }
                }
            }
          $result = $data->get()->num_rows();
          return $result;
    }

    public function getProperties(){
        return $this->db->select('*')
            ->from('property')
            ->where('deleted','0')
            ->get()
            ->result_array();
    }

    public function getUsers(){
        return $this->db->select('*')
            ->from('users')
            ->get()
            ->result_array();
    }

    public function storeLogsBatch($data){
        $this->db->insert_batch('logs',$data);
    }

    public function getAllLogs($filters){
        $q = $this->db->select('
            l.id as log_id,
            l.user_id as user_id,
            l.property_id as property_id,
            u.email as user_email,
            l.controller as controller,
            l.method as method,
            l.item as item,
            l.ip as ip,
            l.created_at as created_at,
            p.name as property_name
        ')->from('logs as l');

        if(!empty($filters)){
            foreach($filters as $key => $value){
                if(!empty($value) && $key != '_'){
                    if($key === 'month'){
                        $q->where('MONTH(l.created_at)',$filters['month']);
                    }
                    elseif($key === 'year'){
                        $q->where('YEAR(l.created_at)', $filters['year']);
                    }
                    elseif($key === 'user'){
                        $q->group_start();
                            $q->like('u.email',$value);
                            $q->or_like('u.id',$value);
                        $q->group_end();
                    }
                    elseif($key === 'property'){
                        $q->group_start();
                            $q->like('p.name',$value);
                            $q->or_like('l.property_id',$value);
                        $q->group_end();
                    }
                    elseif($key === 'created_at'){
                        $q->where('DATE(l.created_at)',date('Y-m-d',strtotime($value)));
                    }
                    else{
                        $q->like('l.'.$key,$value);
                    }
                }
            }
        }

        $q->join('users as u','u.id = l.user_id','left');
        $q->join('property as p','p.id = l.property_id','left')->order_by('l.id','desc');

        $data = $q->get()->result_array();

        $response = array();

        if(!empty($data)){
            foreach($data as $d){
                $response[] = array(
                        $d['log_id'],
                        $d['user_email'].' ( ID='.$d['user_id'].' )',
                        $d['controller'],
                        $d['method'],
                        $d['item'],
                        $d['property_name'].' ( ID='.$d['property_id'].' )',
                        $d['ip'],
                        date('d.m.Y H:i:s',strtotime($d['created_at']))
                    );
            }
        }

        return $response;

    }
}