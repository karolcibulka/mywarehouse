<?php
/**
 * Created by PhpStorm.
 * User: karol
 * Date: 2019-09-26
 * Time: 15:46
 */

class Notifications_model extends CI_Model
{

    public function getLang($reservationID){
        $lang = $this->db->select('lang')
            ->from('reservations as r')
            ->where('r.id',$reservationID)
            ->get()
            ->row_array();

        $lang = $lang['lang'];
        return $lang;
    }

    public function getProperty($propertyID){
        $data =  $this->db->select('*')
            ->from('property_settings_web')
            ->where('property_id',$propertyID)
            ->get()
            ->row_array();

        if(isset($data['logo']) && !empty($data['logo'])){
            $data['logo'] = upload_url($propertyID.'/logo/'.$data['logo']);
            $data['contact']['email'] = $data['email'];
            $data['contact']['phone'] = $data['phone'];
        }

        return $data;
    }

    public function getSubscriber($propertyID,$token){

    }

	public function getPropertySettingsSms($property_id){
        $data = $this->db->select('sms')
            ->from('property_settings_web')
            ->where('property_id',$property_id)
            ->get()
            ->row_array();

        if(isset($data['sms']) && !empty($data['sms'])){
           $smsSettings =  unserialize($data['sms']);
           if(isset($smsSettings['integration_id']) && !empty($smsSettings['integration_id']) && isset($smsSettings['sms_farm']) && !empty($smsSettings['sms_farm'])){
               $response = array(
                   'integration_id' => $smsSettings['integration_id'],
                   'code' => $smsSettings['sms_farm'],
                   'name' => isset($smsSettings['sender']) && !empty($smsSettings['sender']) ? $smsSettings['sender'] : 'BlueGastro',
                   'text' => isset($smsSettings['text']) && !empty($smsSettings['text']) ? $smsSettings['text'] : '',
               );

               return $response;
           }
        }

        return false;
    }

    public function getPropertySettingsNotificationEmail($property_id){
        $data = $this->db->select('notification_email')
            ->from('property_settings_web')
            ->where('property_id',$property_id)
            ->get()
            ->row_array();

        if(!empty($data['notification_email'])){
            return $data['notification_email'];
        }
        return false;

    }

    public function userExistAndIsNotActive($propertyID,$token){
        $data = $this->db->select('*')
            ->from('subscribers')
            ->where('token',$token)
            ->where('active','0')
            ->where('property_id',$propertyID)
            ->get()
            ->result_array();

        if(!empty($data)){
            return true;
        }
        return false;
    }

    public function updateSubscriber($propertyID,$token,$data){
        $this->db->where('token',$token)->where('property_id',$propertyID)->update('subscribers',$data);
    }

    public function userExistAndIsActive($propertyID,$token){
        $data = $this->db->select('*')
            ->from('subscribers')
            ->where('token',$token)
            ->where('active','1')
            ->where('property_id',$propertyID)
            ->get()
            ->result_array();

        if(!empty($data)){
            return true;
        }
        return false;
    }

    public function getTable($property_id,$table_token){
        return $this->db->select('*')
            ->from('tables')
            ->where('property_id',$property_id)
            ->where('token',$table_token)
            ->get()
            ->row_array();
    }
}
