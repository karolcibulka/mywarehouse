<!-- Main navbar -->

<style>
    .activeLang{
        background-color:#10a69a;
        color:white;
    }
    .activeLang:hover{
        background-color:#10a69a;
        color:white;
    }
</style>
<div class="navbar navbar-expand-md navbar-dark">
    <div class="navbar-brand">
        <a href="<?=base_url('dashboard/dashboard')?>" class="d-inline-block">
            <h5 style="margin:0;font-weight: bold;color:white;">GASTRO<small>Reservations</small></h5>

        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    <span><?=$this->session->userdata('email');?></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="<?=base_url('dashboard/user/changePassword')?>" class="dropdown-item"><i class="icon-user-check"></i>Zmeniť heslo</a>
                    <a href="<?=base_url();?>dashboard/auth/logout" class="dropdown-item"><i class="icon-switch2"></i><?=lang('menu.logout')?></a>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->