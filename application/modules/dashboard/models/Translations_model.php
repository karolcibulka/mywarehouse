<?php


class Translations_model extends CI_Model
{

    public function storeLanguage($data){
        $this->db->insert('languages',$data);
        return $this->db->insert_id();
    }

    public function deleteLanguage($language){
        $this->db->where('name',$language)->delete('languages');
    }

    public function getLanguage($language){
        return $this->db->select('*')->from('languages')->where('name',$language)->get()->row_array();
    }

    public function updateLanguage($language,$data){
        $this->db->where('name',$language)->update('languages',$data);
    }
}