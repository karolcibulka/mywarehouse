<?php
/**
 * Created by PhpStorm.
 * User: karol
 * Date: 2019-03-08
 * Time: 11:00
 */

class Permission_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAllPermissions(){
        $data = $this->db->select('up.show as "show",up.create as "create",up.edit as "edit",up.delete as "delete",c.name as "controller",u.username as "uname",u.id as "id"')
            ->from('user_permissions as up')
            ->join('controllers as c','up.controller_id = c.id','LEFT')
            ->join('users as u','up.user_id = u.id','LEFT')
            ->get()
            ->result_array();

       foreach($data as $d){
           $userPerms[$d['id']][$d['controller']] = $d;
       }
        return $userPerms;
    }

    public function getUserPermissions($id,$email = false,$property_id){

        $userGroups = $this->db->select('ug.group_id')
            ->from('users_groups as ug')
            ->where('ug.user_id',$id)
            ->get()
            ->result_array();

        $group_ids=array();
        foreach($userGroups as $ug){
            $group_ids[]=$ug['group_id'];
        }

        $property = $this->db->select('controllers')
            ->from('property as p')
            ->where('p.id',$property_id)
            ->join('program as prog','prog.id = p.program_id')
            ->get()
            ->row_array();

        if(isset($property['controllers']) && !empty($property['controllers'])){
            $controllers = json_decode($property['controllers'],true);
        }


        if($userGroups){
            $data = $this->db->select('c.id as "controller_id",up.show as "show",up.create as "create",up.edit as "edit",up.delete as "delete",c.name as "controller",up.user_group_id')
                ->from('user_permissions as up')
                ->where_in('up.user_group_id',$group_ids)
                ->join('controllers as c','up.controller_id = c.id')
                ->get()
                ->result_array();


            foreach($data as $d){
                if(!isset($userPerms[$d['controller']])){
                    if(in_array($d['controller_id'],$controllers)){
                        $up = array(
                            'show' => $d['show'],
                            'create' => $d['create'],
                            'edit' => $d['edit'],
                            'delete' => $d['delete'],
                        );
                        $userPerms[$d['controller']] = $up;
                    }
                    else{
                        $up = array(
                            'show' => '0',
                            'create' => '0',
                            'edit' => '0',
                            'delete' => '0',
                        );
                        $userPerms[$d['controller']] = $up;
                    }

                }
                else{
                    if(in_array($d['controller_id'],$controllers)) {
                        $up = array(
                            'show' => $userPerms[$d['controller']]['show'] == TRUE ?: $d['show'],
                            'create' => $userPerms[$d['controller']]['create'] == TRUE ?: $d['create'],
                            'edit' => $userPerms[$d['controller']]['edit'] == TRUE ?: $d['edit'],
                            'delete' => $userPerms[$d['controller']]['delete'] == TRUE ?: $d['delete'],
                        );
                        $userPerms[$d['controller']] = $up;
                    }
                    else{
                        $up = array(
                            'show' => '0',
                            'create' => '0',
                            'edit' => '0',
                            'delete' => '0',
                        );
                        $userPerms[$d['controller']] = $up;
                    }
                }

            }
            $this->session->set_userdata('permissions',$userPerms);

            return $userPerms;
        }

    }



        public function createController($data){
            if(isset($data) && !empty($data)){
                $this->db->insert('controllers', $data);
            }
        }

        public function getAllControllers(){
            $data = $this->db->select('c.id as "id",c.name as "name"')
                ->from('controllers as c')
                ->get()
                ->result_array();

            return $data;
        }

        public function getGroupsWithPermissions(){
            $groups = $this->db->select('g.*')
                ->from('groups as g')
                ->order_by('g.order','asc')
                ->get()
                ->result_array();

            $controllers = $this->db->select('c.*')
                ->from('controllers as c')
                ->where('c.deleted',0)
                ->get()
                ->result_array();

            $permissions = $this->db->select('up.*')
                ->from('user_permissions as up')
                ->get()
                ->result_array();

            $data = array();

            if(!empty($groups)){
                foreach($groups as $key => $g){
                    $data[$g['name']]=$g;
                    if(!empty($controllers)){
                        foreach($controllers as $controllerKey =>$c){
                            $data[$g['name']]['controllers'][$controllerKey] = $c;
                            if(!empty($permissions)){
                                foreach($permissions as $p){
                                    if($p['user_group_id'] == $g['id'] && $p['controller_id'] == $c['id']){
                                        $permission = array(
                                            'show' => $p['show'],
                                            'delete' => $p['delete'],
                                            'create' => $p['create'],
                                            'edit' => $p['edit']
                                        );
                                        $data[$g['name']]['controllers'][$controllerKey]['permissions'] = $permission;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return $data;
        }

        public function checkPermission($cid,$gid){
            $data = $this->db->select('*')
                ->from('user_permissions')
                ->where('user_group_id',$gid)
                ->where('controller_id',$cid)
                ->get()
                ->row_array();

            if(!empty($data)){
                return true;
            }
            else{
                return false;
            }
        }

        public function updatePermission($cid,$gid,$data){
            return $this->db->where('user_group_id', $gid)
                ->where('controller_id',$cid)
                ->update('user_permissions', $data);
        }

        public function insertNewPermission($data){
            if(isset($data) && !empty($data)){
                $this->db->insert('user_permissions', $data);
            }
        }

        public function getController($id){
            $data = $this->db->select('*')
                ->from('controllers')
                ->where('id',$id)
                ->get()
                ->row_array();
                return $data;
        }

        public function updateController($id,$data){
            return $this->db->where('id',$id)
                ->update('controllers',$data);
        }

        public function updateGroupOrder($id,$data){
            $this->db->where('id',id)->update('groups',$data);
        }
}