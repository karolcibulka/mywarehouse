<?php
class Global_model extends CI_Model
{

    public function get_languages()
    {
        return $this->db->select('*')
            ->from('languages')
            ->get()
            ->result_array();
    }

    public function getLanguages(){
        return $this->db->select('*')
            ->from('languages')
            ->get()
            ->result_array();
    }

    public function getWeb($propertyID){
        $data = $this->db->select('web')
            ->from('property_settings_web')
            ->where('property_id',$propertyID)
            ->get()
            ->row_array();

        return $data['web'];
    }

    public function propertySelling($propertyID){
        $data = $this->db->select('selling')
            ->from('property')
            ->where('id',$propertyID)
            ->get()
            ->row_array();

        if(isset($data['selling'])){
            return $data['selling'];
        }

        return '0';
    }
}