<?php


class User_model extends CI_Model
{

    public function updatePassword($id,$data){
        $this->db->where('id',$id)->update('users',$data);
        return true;
    }

    public function getUsers($limit,$per_page,$filter=array()){
        $result = $this->db->select('u.id,u.email,GROUP_CONCAT(g.id SEPARATOR "|") as group_id,GROUP_CONCAT(g.description SEPARATOR "|") as group_descriptions')
            ->from('users as u')
            ->limit($per_page,$limit)
            ->join('users_groups as ug','ug.user_id = u.id','left')
            ->join('groups as g','g.id = ug.group_id','left')
            ->order_by('u.id,g.order asc')
            ->group_by('u.id');

        if(!empty($filter)){
            foreach($filter as $key => $value){
                if($value !== ''){
                    if($key == 'email'){
                        $result->like('u.email',$value);
                    }
                    elseif($key == 'role'){
                        $result->where('g.id',$value);
                    }
                    else{
                        $result->where('u.'.$key,$value);
                    }
                }
            }
        }

        $data = $result->get()->result_array();

        $response = array();
        if(!empty($data)){
            foreach($data as $d){
                $group_ids = explode('|',$d['group_id']);
                $roles = explode('|',$d['group_descriptions']);
                $response[$d['id']]['id'] = $d['id'];
                $response[$d['id']]['email'] = $d['email'];
                foreach($group_ids as $key => $group_id){
                    $response[$d['id']]['roles'][$group_id] = $roles[$key];
                }
            }
        }

        return $response;
    }

    public function getRoles(){
        $data = $this->db->select('*')
            ->from('groups as g')
            ->order_by('g.order','asc')
            ->get()
            ->result_array();

        $response = array();
        if(!empty($data)){
            foreach($data as $d){
                $response[$d['id']] = $d['description'];
            }
        }

        return $response;
    }

    public function getUsersCount($filter=array()){
        $query = $this->db->query('SELECT * FROM users');
        return $query->num_rows();
    }

    public function deleteUser($id){
        $this->db->where('id',$id)->delete('users');
    }
}