<?php
/**
 * Created by PhpStorm.
 * User: karol
 * Date: 2019-05-30
 * Time: 15:04
 */

class Cron_model extends CI_Model
{
    public function getReservations(){
        $current_time = date('Y-m-d H:i:s');
        //pre_r($current_time);
        $data = $this->db->select('r.*,c.*,p.name as "property_name",r.id as "reservation_id"')
            ->from('reservations as r')
            ->where('r.timeFrom>',$current_time)
            ->where('DATE(r.timeFrom)',date('Y-m-d'))
            ->where('r.deleted','0')
            ->where('r.notificated','0')
            ->join('customers as c','r.customer_id = c.id')
            ->join('property as p','r.property_id = p.id')
            ->get()
            ->result_array();

        foreach($data as $key => $d){
            if(strtotime(date('H:i',strtotime($d['timeFrom'].'-30 minutes'))) == strtotime(date('H:i'))){
                continue;
            }
            else{
                unset($data[$key]);
            }
        }
        return $data;
    }



    public function getDoneReservations(){
        $current_time = date('Y-m-d H:i:s');
        $data = $this->db->select('r.*,c.*,p.name as "property_name",r.id as "reservation_id",c.email as "customer_email",p.id as "property_id", r.id as "reservation_id",r.token as "token",c.first_name as "firstName",c.last_name as "lastName"')
            ->from('reservations as r')
            ->where('DATE(r.timeTo)',date('Y-m-d'))
            ->where('r.deleted','0')
            ->where('r.notificatedbyemail','0')
            ->join('customers as c','r.customer_id = c.id')
            ->join('property as p','r.property_id = p.id')
            ->get()
            ->result_array();

        foreach($data as $key => $d){
            if(strtotime(date('H:i',strtotime($d['timeTo'].'+ 1 hour'))) == strtotime(date('H:i'))){
                continue;
            }
            else{
                unset($data[$key]);
            }
        }

        return $data;
    }

    public function notificated($data,$id){
        $this->db->where('id',$id)
            ->update('reservations',$data);
    }
}