<?php
/**
 * Created by PhpStorm.
 * User: karol
 * Date: 2019-03-15
 * Time: 14:44
 */

class Properties_model extends CI_Model
{

    public function getPropertySettings($id){
        $data = $this->db->select('ps.*')
            ->from('property_settings as ps')
            ->where('id',$id)
            ->get()
            ->row_array();

        return $data;
    }

    public function getRoles(){
        return $this->db->select('*')
            ->from('groups')
            ->order_by('order','asc')
            ->get()
            ->result_array();
    }

    public function getVersions(){
        return $this->db->select('*')
            ->from('versions')
            ->get()
            ->result_array();
    }

    public function getPrograms(){
        return $this->db->select('*')
            ->from('program')
            ->where('deleted','0')
            ->where('active','1')
            ->get()
            ->result_array();
    }

    public function getPropertySettingsByProperty($id){
        $data = $this->db->select('ps.*')
            ->from('property_settings as ps')
            ->where('property_id',$id)
            ->get()
            ->row_array();

        return $data;
    }

    public function getPropertyDev($id){
        return $this->db->select('p.version,p.program_id,p.id,p.name,p.system_id,p.server,p.port,p.cash_desk_id,group_concat(up.user_id separator "|") as "users"')
            ->where('p.id',$id)
            ->from('property as p')
            ->join('users_properties as up','up.property_id = p.id','left')
            ->get()
            ->row_array();
    }

    public function getProperty($id){
        $data = $this->db->select('ps.*,p.*,ps.id as setting_id, p.id as property_id,pn.*')
            ->from('property as p')
            ->where('p.id',$id)
            ->join('property_settings as ps','ps.property_id = p.id','left')
            ->join('property_notification as pn','pn.property_id = p.id','left')
            ->get()
            ->result_array();

        $result = array();
        foreach($data as $d){
            $result['id'] = $d['id'];
            $result['cron_email'] = $d['cron_email'];
            $result['time_sequence'] = $d['time_sequence'];
            $result['primary_color'] = $d['primary_color'];
            $result['secondary_color'] = $d['secondary_color'];
            $result['event_email'] = $d['event_email'];
            $result['event_phone'] = $d['event_phone'];
            $result['street'] = $d['street'];
            $result['city'] = $d['city'];
            $result['name'] = $d['name'];
            $result['postalcode'] = $d['postalcode'];
            $result['telephone'] = $d['telephone'];
            $result['email'] = $d['email'];
            $result['taxid'] = $d['taxid'];
            $result['url'] = $d['url'];
            $result['capacity'] = $d['capacity'];
            $result['integrationID'] = $d['integrationID'];
            $result['smsFarmCode'] = $d['smsFarmCode'];
            $result['token'] = $d['token'];
            $result['active_token'] = $d['active_token'];
            $result['iban'] = $d['iban'];
            $result['swift'] = $d['swift'];
            $result['setting_id'] = $d['setting_id'];
            $result['property_color'] = $d['property_color'];
            $result['langs'][$d['lang']]['confirm_email'] = $d['confirm_email'];
            $result['langs'][$d['lang']]['pending_email'] = $d['pending_email'];
            $result['langs'][$d['lang']]['cancel_email'] = $d['cancel_email'];
            $result['langs'][$d['lang']]['rating_email'] = $d['rating_email'];
            $result['langs'][$d['lang']]['confirm_sms'] = $d['confirm_sms'];
            $result['langs'][$d['lang']]['pending_sms'] = $d['pending_sms'];
            $result['langs'][$d['lang']]['cancel_sms'] = $d['cancel_sms'];
            $result['langs'][$d['lang']]['accepted_email'] = $d['accepted_email'];
            $result['langs'][$d['lang']]['declined_sms'] = $d['declined_sms'];
            $result['langs'][$d['lang']]['declined_email'] = $d['declined_email'];
            $result['langs'][$d['lang']]['accepted_sms'] = $d['accepted_sms'];
            $result['langs'][$d['lang']]['success_pay_email'] = $d['success_pay_email'];
            $result['langs'][$d['lang']]['unsuccess_pay_email'] = $d['unsuccess_pay_email'];
            $result['langs'][$d['lang']]['request_pay_email'] = $d['request_pay_email'];
            $result['langs'][$d['lang']]['success_pay_sms'] = $d['success_pay_sms'];
            $result['langs'][$d['lang']]['unsuccess_pay_sms'] = $d['unsuccess_pay_sms'];
            $result['langs'][$d['lang']]['request_pay_sms'] = $d['request_pay_sms'];
        }
        //pre_r($data);exit;
        return $result;
    }

    public function getProperties(){
        $data = $this->db->select('ps.*,p.*,ps.id as setting_id, p.id as property_id')
            ->from('property as p')
            ->join('property_settings as ps','ps.property_id = p.id','left')
            ->where('p.deleted','0')
            ->get()
            ->result_array();

        return $data;
    }




    public function removePropertySettings($property_id){
        $this->db->where('property_id',$property_id)
            ->delete('property_notification');
    }

    public function checkProperty($id){
        $data = $this->db->select('p.id')
            ->from('property as p')
            ->where('id',$id)
            ->get()
            ->result_array();

        if(empty($data)){
            return false;
        }
        else{
            return true;
        }
    }

    public function checkPropertySettings($id){
        $data = $this->db->select('ps.property_id')
            ->from('property_settings as ps')
            ->where('property_id',$id)
            ->get()
            ->result_array();

        if(empty($data)){
            return false;
        }
        else{
            return true;
        }
    }

    public function checkPropertyNotifications($id){
        $data = $this->db->select('property_id')
            ->from('property_notification')
            ->where('property_id',$id)
            ->get()
            ->result_array();

        if(empty($data)){
            return false;
        }
        else{
            return true;
        }
    }

    public function updatePropertyNotifications($notificationData, $property_id){
        $this->db->where('property_id',$property_id)
            ->update('property_notification',$notificationData);
    }

    public function insertPropertyNotifications($data){
        $this->db->insert('property_notification',$data);
    }

    public function removeProperty($id){
        $this->db->where('id',$id)->delete('property');
    }

    public function insertProperty($data){
        if(isset($data) && !empty($data)) {
            $this->db->insert('property', $data);
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    public function updateProperty($data,$id){
        return $this->db->where('id', $id)
            ->update('property', $data);
    }

    public function insertPropertySettings($data){
        if(isset($data) && !empty($data)) {
            $this->db->insert('property_settings', $data);
        }
    }

    public function updatePropertySettings($data,$id){
        return $this->db->where('property_id', $id)
            ->update('property_settings', $data);
    }

    public function getPreregistrations()
    {
        $data = $this->db->select('*')
            ->from('property_preregistration')
            ->get()
            ->result_array();

        return $data;
    }

    public function getPreregistration($id)
    {
        $data = $this->db->select('*')
            ->from('property_preregistration')
            ->where('id',$id)
            ->get()
            ->row_array();

        return $data;
    }

    public function createUser($data){
        if(isset($data) && !empty($data)) {
            $this->db->insert('users', $data);
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    public function insertUserProperties($user_id, $property_id){
        return $this->db->insert('users_properties', array('user_id' => $user_id, 'property_id' => $property_id));
    }

    public function removePreregistration($id){
        return $this->db->where('id', $id)
            ->delete('property_preregistration');
    }

    public function getUsers(){
        return $this->db->select('*')
            ->from('users')
            ->get()
            ->result_array();
    }

    public function deleteUserProperties($user_id, $property_id)
    {
        return $this->db->where('user_id', $user_id)
            ->where('property_id', $property_id)
            ->delete('users_properties');
    }

    public function deleteAllUsersFromProperty($property_id){
        $this->db->where('property_id',$property_id)->delete('users_properties');
    }

    public function getPropertyUsers($property_id)
    {
        $data = $this->db->select('*')
            ->where('property_id',$property_id)
            ->from('users_properties')
            ->get()
            ->result_array();

        $out = array();
        foreach ($data as $d){
            $out[$d['user_id']] = $d;
        }

        return $out;
    }

    public function emailExist($email){
        $user = $this->db->select('*')
            ->from('users')
            ->where('email',$email)
            ->get()
            ->row_array();

        if(!empty($user)){
            return false;
        }

        return true;
    }

    public function insertUser($data){
        $this->db->insert('users',$data);
        return $this->db->insert_id();
    }

    public function insertConnection($data){
        $this->db->insert('users_properties',$data);
    }

    public function insertConnectionPermission($data){
        $this->db->insert('users_groups',$data);
    }
}
