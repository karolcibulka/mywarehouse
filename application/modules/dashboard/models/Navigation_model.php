<?php
/**
 * Created by PhpStorm.
 * User: karol
 * Date: 2019-03-08
 * Time: 11:00
 */

class Navigation_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    private function buildTree($elements, $parentId = 0)
    {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }


    public function getNavItems()
    {

        $data = $this->db->select('navigation.navigation_category navigation_category_id,navigation_category.name as navigation_category_name,navigation.id as "id",navigation.name as "name",navigation.active as "active",navigation.deleted as "deleted",controllers.name as "path",navigation.order as "order",icons.value as "icon",navigation_type.value as "type",navigation.parent as "parent",navigation.link,navigation.placeholder,navigation.link_path')
            ->from('navigation')
            ->join('navigation_type', 'navigation_type.id = navigation.type')
            ->join('controllers', 'controllers.id = navigation.controller', 'LEFT')
            ->join('navigation_category', 'navigation.navigation_category = navigation_category.id')
            ->join('icons', 'icons.id = navigation.icon', 'LEFT')
            ->where('navigation.active', 1)
            ->where('navigation.deleted', 0)
            ->order_by('navigation_category.order', 'asc')
            ->order_by('navigation.order', 'asc')
            ->get()
            ->result_array();


        $result = array();
        if (!empty($data)) {
            foreach ($data as $key => $d) {
                $result[$d['navigation_category_id']]['name']            = $d['navigation_category_name'];
                $result[$d['navigation_category_id']]['items'][$d['id']] = $d;
            }
        }

        foreach ($result as $category_id => $resultItem) {
            $result[$category_id]['items'] = $this->buildTree($result[$category_id]['items'], 0);
        }

        return $result;
    }


    public function getAllChilds($id)
    {
        return $this->db->select('navigation.id as "id",navigation.name as "name",navigation.active as "active",navigation.deleted as "deleted",navigation.controller as "path",navigation.order as "order",navigation.icon as "icon",navigation.type as "type",navigation.parent as "parent",navigation.link,navigation.placeholder,navigation.link_path')
            ->from('navigation')
            ->where('parent', $id)
            ->order_by('order', 'asc')
            ->get()
            ->result_array();


    }

    public function updateController($id, $data)
    {
        $this->db->where('id', $id)->update('controllers', $data);
    }

    public function getCategories()
    {
        return $this->db->select('*')
            ->from('navigation_category')
            ->where('deleted', 0)
            ->order_by('order','asc')
            ->get()
            ->result_array();
    }

    public function getControllers()
    {
        return $this->db->select('id,name,description')
            ->from('controllers')
            ->where('active', 1)
            ->where('deleted', 0)
            ->order_by('order', 'asc')
            ->get()
            ->result_array();
    }

    public function getNavTypes()
    {
        return $this->db->select('navigation_type.value as "value", navigation_type.name as "name",navigation_type.id as "id"')
            ->from('navigation_type')
            ->get()
            ->result_array();
    }

    public function storeNavigationItem($data){
        $this->db->insert('navigation',$data);
        return $this->db->insert_id();
    }

    public function getIcons()
    {
        return $this->db->select('value,name,id')
            ->from('icons')
            ->get()
            ->result_array();
    }

    public function getHighestOrderNum()
    {
        return $this->db->select('MAX("order") as "num"')
            ->from('navigation')
            ->get()
            ->row_array();
    }

    public function insertNewMenuOption($data)
    {
        if (isset($data) && !empty($data)) {
            $this->db->insert('navigation', $data);
        }
    }

    public function getCMSitemByID($id)
    {
        return $this->db->select('navigation.id as "id",navigation.name as "name",navigation.active as "active",navigation.deleted as "deleted",navigation.controller as "path",navigation.order as "order",navigation.icon as "icon",navigation.type as "type",navigation.parent as "parent",navigation.placeholder as "placeholder", navigation.link as "link", navigation.link_path as "link_path"')
            ->from('navigation')
            ->where('id', $id)
            ->get()
            ->row_array();
    }

    public function editMenuOption($data)
    {
        $this->db->where('id', $data['id'])->update('navigation', $data);
    }

    public function deleteMenuOption($data, $id)
    {
        if (isset($data) && !empty($data)) {
            return $this->db->where('id', $id)
                ->update('navigation', $data);
        }
    }

    public function updateMenuOrder($data, $id)
    {
        return $this->db->where('id', $id)->update('navigation', $data);
    }

    public function updateNavigation($id, $data)
    {
        $this->db->where('id', $id)->update('navigation', $data);
        return $id;
    }

    public function updateNavigationCategory($id, $data)
    {
        $this->db->where('id', $id)->update('navigation_category', $data);
    }

    public function getCategory($id){
        return $this->db->select('*') //SELECT * FROM 'navigation_category' WHERE ID = {$id}
            ->from('navigation_category')
            ->where('id',$id)
            ->get()
            ->row_array();
    }

    public function updateCategory($id,$data){
        $this->db->where('id',$id)->update('navigation_category',$data);
        return $id;
    }

    public function getTest($id){
        $data = $this->db->select('
            nv.name as category_name,
            nv.id as category_id,
            GROUP_CONCAT(n.id SEPARATOR "|") as concat_items_ids,
        ')
            ->from('navigation as n')
            ->where('nv.id',$id)
            ->join('navigation_category as nv','n.navigation_category = nv.id','left')
            ->group_by('nv.id')
            ->get()
            ->row_array();

        if(isset($data['concat_items_ids']) && !empty($data['concat_items_ids'])){
            $data['concat_items_ids'] = explode('|',$data['concat_items_ids']);
        }


        return $data;




    }

    public function getNavigation($id){
        return $this->db->select('*')
            ->from('navigation')
            ->where('id',$id)
            ->get()
            ->row_array();
    }

}