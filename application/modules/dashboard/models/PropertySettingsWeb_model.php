<?php


class PropertySettingsWeb_model extends CI_Model
{

    public function getData($property_id){
        return $this->db->select('*')
            ->from('property_settings_web')
            ->where('property_id',$property_id)
            ->get()
            ->row_array();
    }

    public function getDeliveries($property_id){
        return $this->db->select('*')
            ->from('transportation')
            ->where('property_id',$property_id)
            ->where('is_delivery','0')
            ->get()
            ->result_array();
    }

    public function updateSettings($property_id,$data){
        $this->db->where('property_id',$property_id)->update('property_settings_web',$data);
    }

    public function getLanguages($property_id){
        $languages = $this->db->select('*')
            ->from('property_languages')
            ->where('property_id',$property_id)
            ->get()
            ->result_array();

        $response = array();
        if(!empty($languages)){
            foreach($languages as $lang){
                $response[] = $lang['language_code'];
            }
        }

        return $response;
    }

    public function getCurrencies(){
        return $this->db->select('*')
            ->from('currencies')
            ->get()
            ->result_array();
    }

    public function insertSettings($data){
        $this->db->insert('property_settings_web',$data);
    }

    public function removeAllPropertyLanguages($property_id){
        $this->db->where('property_id',$property_id)->delete('property_languages');
    }

    public function insertLang($data){
        $this->db->insert('property_languages',$data);
    }

}