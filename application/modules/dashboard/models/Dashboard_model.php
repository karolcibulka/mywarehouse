<?php
/**
 * Created by PhpStorm.
 * User: karol
 * Date: 2019-06-05
 * Time: 17:32
 */

class Dashboard_model extends CI_Model
{
    public function getDailyMenus($property_id, $type)
    {
        $this->db->select('*')
            ->from('daily_menu as dm')
            ->where('dm.property_id', $property_id)
            ->where('dm.deleted', '0');
        if ($type == 'today') {
            $this->db->where('DATE(dm.date)', date('Y-m-d'));
        } elseif ($type == 'week') {
            $this->db->where('DATE(dm.date)>=', date('Y-m-d', strtotime('monday this week')));
            $this->db->where('DATE(dm.date)<=', date('Y-m-d', strtotime('sunday this week')));
        } elseif ($type == 'month') {
            $this->db->where('YEAR(dm.date)', date('Y'));
            $this->db->where('MONTH(dm.date)', date('m'));
        } elseif ($type == 'year') {
            $this->db->where('YEAR(dm.date)', date('Y'));
        }

        $data = $this->db->get()->result_array();

        $response = array();
        if (!empty($data)) {
            foreach ($data as $d) {
                $response[$d['id']]           = $d;
                $response[$d['id']]['selled'] = '0';
                $response[$d['id']]['color']  = random_color();
            }
        }

        return $response;
    }

    public function getDiscounts($property_id){
        $data = $this->db->select('*')
            ->from('discounts as d')
            ->where('d.property_id',$property_id)
            ->where('d.active','1')
            ->where('d.deleted','0')
            ->get()
            ->result_array();

        $response = array();
        if(!empty($data)){
            foreach($data as $d){
                $response[$d['id']] = $d;
                $response[$d['id']]['selled'] = 0;
                $response[$d['id']]['color'] = random_color();
            }
        }

        return $response;
    }

    public function getStats($type, $property_id)
    {
        $transportations = $this->getTransportations($property_id);
        $offers          = $this->getOffers($property_id);
        $dailyMenus      = $this->getDailyMenus($property_id, $type);
        $packings        = $this->getPacking($property_id);
        $discounts       = $this->getDiscounts($property_id);

        $this->db->select('*,ri.count as "offerCount",r.discount_price as "resPrice",ri.type as "offerType"')
            ->from('reservations as r')
            ->join('reservations_item as ri', 'ri.reservation_id = r.id', 'left')
            ->where('r.property_id', $property_id)
            ->where('r.status', savedToBGStatus());
        if ($type == 'today') {
            $this->db->where('DATE(r.created_at)', date('Y-m-d'));
        } elseif ($type == 'week') {
            $this->db->where('DATE(r.created_at)>=', date('Y-m-d', strtotime('monday this week')));
            $this->db->where('DATE(r.created_at)<=', date('Y-m-d', strtotime('sunday this week')));
        } elseif ($type == 'month') {
            $this->db->where('YEAR(r.created_at)', date('Y'));
            $this->db->where('MONTH(r.created_at)', date('m'));
        } elseif ($type == 'year') {
            $this->db->where('YEAR(r.created_at)', date('Y'));
        }
        $data = $this->db->get()->result_array();

        $response = array();
        if (!empty($data)) {
            foreach ($data as $d) {
                $response[$d['reservation_id']]['created_at']                      = $d['created_at'];
                $response[$d['reservation_id']]['transportation_id']               = $d['transportation_id'];
                $response[$d['reservation_id']]['price']                           = $d['resPrice'];
                $response[$d['reservation_id']]['packingPrice']                    = $d['packing_price'];
                $response[$d['reservation_id']]['itemsPrice']                      = $d['items_price'];
                $response[$d['reservation_id']]['menuPrice']                       = $d['menu_price'];
                $response[$d['reservation_id']]['currency']                        = $d['currency'];
                $response[$d['reservation_id']]['offers'][$d['offer_id']]['id']    = $d['offer_id'];
                $response[$d['reservation_id']]['offers'][$d['offer_id']]['count'] = $d['offerCount'];
                $response[$d['reservation_id']]['offers'][$d['offer_id']]['type']  = $d['offerType'];
            }
        }

        $statsHours          = array();
        $offersHours         = array();
        $dailyMenuHours      = array();
        $discountsHours      = array();
        $transportationPrice = 0;
        $packingPrice        = 0;
        $reservationsPrice   = 0;
        $dailyMenuPrice      = 0;
        $fullPrice           = 0;
        $currency            = '';
        $reservationsCount   = 0;
        for ($i = 0; $i <= 23; $i++) {
            $key                  = $i < 10 ? '0' . $i : $i;
            $statsHours[$key]     = 0;
            $offersHours[$key]    = 0;
            $dailyMenuHours[$key] = 0;
            $discountsHours[$key] = 0;
        }
        foreach ($response as $res) {
            $reservationsCount                                    += 1;
            $currency                                             = $res['currency'];
            $statsHours[date('H', strtotime($res['created_at']))] += 1;
            if (isset($transportations[$res['transportation_id']]) && !empty($transportations[$res['transportation_id']])) {
                $transportations[$res['transportation_id']]['selled'] += 1;
                $transportationPrice                                  += $transportations[$res['transportation_id']]['price'];
                $fullPrice                                            += $transportations[$res['transportation_id']]['price'];
            }
            $reservationsPrice += $res['itemsPrice'];
            $dailyMenuPrice    += $res['menuPrice'];
            $fullPrice         += $res['price'];
            $packingPrice      += $res['packingPrice'];
            if (!empty($res['offers'])) {
                foreach ($res['offers'] as $offerID => $offer) {
                    if(isset($offer['discount_id']) && !empty($offer['discount_id'])){
                        if(isset($discounts[$offer['discount_id']]) && !empty($discounts[$offer['discount_id']])){
                            $discounts[$offer['discount_id']]['selled'] += 1;
                            $discountsHours[date('H', strtotime($res['created_at']))] += 1;
                        }
                    }
                    if ($offer['type'] == 'offer') {
                        if (isset($offers[$offerID]) && !empty($offers[$offerID])) {
                            $offersHours[date('H', strtotime($res['created_at']))] += 1;
                            $offers[$offerID]['selled'] += $offer['count'];
                        }
                    } else {
                        if (isset($dailyMenus[$offerID]) && !empty($dailyMenus[$offerID])) {
                            $dailyMenuHours[date('H', strtotime($res['created_at']))] += 1;
                            $dailyMenus[$offerID]['selled'] += $offer['count'];
                        }
                    }
                }
            }
        }

        $finalRes                         = array();
        $finalRes['hours']                = $statsHours;
        $finalRes['offerHours']           = $offersHours;
        $finalRes['dailyMenuHours']       = $dailyMenuHours;
        $finalRes['discountsHours']       = $discountsHours;
        $finalRes['currency']             = $currency;
        $finalRes['reservationsCount']    = $reservationsCount;
        $finalRes['transportationsPrice'] = formatNumApi($transportationPrice);
        $finalRes['reservationsPrice']    = formatNumApi($reservationsPrice);
        $finalRes['packingPrice']         = formatNumApi($packingPrice);
        $finalRes['fullPrice']            = formatNumApi($fullPrice);
        $finalRes['menuPrice']            = formatNumApi($dailyMenuPrice);
        $finalRes['transportations']      = $transportations;
        $finalRes['packings']             = $packings;
        $finalRes['offers']               = $offers;
        $finalRes['dailyMenus']           = $dailyMenus;
        $finalRes['discounts']           = $discounts;

        return $finalRes;
    }

    public function getTransportations($property_id)
    {
        $data = $this->db->select('internal_name,price,id')
            ->from('transportation as t')
            ->where('t.property_id', $property_id)
            ->where('t.active', '1')
            ->where('t.deleted', '0')
            ->get()
            ->result_array();

        $response = array();
        if (!empty($data)) {
            foreach ($data as $d) {
                $response[$d['id']]['id']            = $d['id'];
                $response[$d['id']]['price']         = $d['price'];
                $response[$d['id']]['internal_name'] = $d['internal_name'];
                $response[$d['id']]['selled']        = 0;
                $response[$d['id']]['color']         = random_color();
            }
        }

        return $response;
    }

    public function getOffers($property_id)
    {
        $data = $this->db->select('o.internal_name,o.unit_price,o.id,o.packing_id')
            ->from('offers as o')
            ->where('o.property_id', $property_id)
            ->join('category_offers as co', 'co.id = o.category_id')
            ->where('o.active', '1')
            ->where('co.active', '1')
            ->where('o.deleted', '0')
            ->where('co.deleted', '0')
            ->get()
            ->result_array();

        $response = array();
        if (!empty($data)) {
            foreach ($data as $d) {
                $response[$d['id']]['id']            = $d['id'];
                $response[$d['id']]['price']         = $d['unit_price'];
                $response[$d['id']]['internal_name'] = $d['internal_name'];
                $response[$d['id']]['packing_id']    = $d['packing_id'];
                $response[$d['id']]['selled']        = 0;
                $response[$d['id']]['color']         = random_color();
            }
        }

        return $response;
    }

    public function getPacking($property_id)
    {
        $data = $this->db->select('internal_name,price,id')
            ->from('packing as p')
            ->where('p.property_id', $property_id)
            ->where('p.deleted', '0')
            ->get()
            ->result_array();

        $response = array();
        if (!empty($data)) {
            foreach ($data as $d) {
                $response[$d['id']]['id']            = $d['id'];
                $response[$d['id']]['price']         = $d['price'];
                $response[$d['id']]['internal_name'] = $d['internal_name'];
                $response[$d['id']]['selled']        = 0;
            }
        }

        return $response;
    }

    public function getWeb($property_id)
    {
        $data = $this->db->select('web,default_lang')
            ->from('property_settings_web')
            ->where('property_id', $property_id)
            ->get()
            ->row_array();

        if (!empty($data['web'])) {
            return $data['web'] . '/' . $data['default_lang'];
        }
        return false;

    }

    public function updateProperty($propertyID, $data)
    {
        $this->db->where('id', $propertyID)->update('property', $data);
    }
}