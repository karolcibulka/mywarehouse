<?php


class Log extends DASH_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('Log_model','log_model');
    }

    public function index(){
        if(has_permission('show')){

            $get = $this->input->get();
            //$get['month'] = date('m');

            if(!isset($get['month']) || empty($get['month'])){
                $get['month'] = date('m');
            }

            if(!isset($get['year']) || empty($get['year'])){
                $get['year'] = date('Y');
            }

            $data['get'] = $get;


            $this->template->load('master','logs/show',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function getData(){
        $get = $this->input->get();

        $data = $this->log_model->getAllLogs($get);

        echo json_encode(array('data'=>$data));
    }
}