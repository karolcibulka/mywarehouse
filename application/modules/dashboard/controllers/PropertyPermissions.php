<?php
class PropertyPermissions extends DASH_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Properties_model', 'propertiesModel');

    }

    /**
     * @return object
     */

    //public function index(){
    //    pre_r('nasrat');
    //}

    public function index()
    {
        if (has_permission('show')) {
            $data['title'] = 'WMS - CMS Menu';
            $data['properties'] = $this->propertiesModel->getProperties();
            $data['users'] = $this->propertiesModel->getUsers();

            $this->template->load('master', 'propertyPermissions/show', $data);
        }
        else{
            $this->wrongState();
        }
    }

    public function getPropertyUsersPermissions($property_id)
    {
        if(has_permission('edit')){
            $data['users'] = $this->propertiesModel->getUsers();
            $data['userProperties'] = $this->propertiesModel->getPropertyUsers($property_id);
            $data['property'] = $this->propertiesModel->getProperty($property_id);

            $view = $this->load->view('propertyPermissions/usersProperties',$data, true);

            $response=array(
                'status' => '1',
                'view' => $view
            );

            echo json_encode($response);
        }
        else{
            $this->wrongState();
        }
    }

    public function handlePropertyPermission($property_id, $user_id, $value){
        if(has_permission('edit')){
            if($value == 1) {
                $this->propertiesModel->insertUserProperties($user_id, $property_id);
            }else{
                $this->propertiesModel->deleteUserProperties($user_id, $property_id);
            }
        }
        else{
            $this->wrongState();
        }
    }
}