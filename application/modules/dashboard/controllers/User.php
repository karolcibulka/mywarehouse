<?php

class User extends DASH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model', 'user_model');

    }


    public function changePassword()
    {
        $this->template->load('master','user/show');
    }

    public function changePasswordPost(){
        $postData = $this->input->post();

        if(isset($postData['new_password']) && !empty($postData['new_password']) && isset($postData['new_password_confirm']) && $postData['new_password_confirm']){
            if($postData['new_password'] == $postData['new_password_confirm']){
                $updateData = array(
                    'password' => $this->bcrypt->hash($postData['new_password']),
                );
                $this->user_model->updatePassword($this->session->userdata('user_id'),$updateData);
                $this->loglib->storeLog('users','change_password',$this->session->userdata('user_id'));
                return redirect(base_url('/dashboard'));
            }
            else{
                //nezhoduju sa
                $this->session->set_flashdata('changePasswordError','Vaše heslá sa nezhodujú');
                return redirect(base_url('/dashboard/user/changePassword'));
            }
        }
        $this->session->set_flashdata('changePasswordError','Musíte vyplniť údaje!');
        return redirect(base_url('/dashboard/user/changePassword'));
    }
}