<?php
/**
 * Created by PhpStorm.
 * User: karol
 * Date: 2019-03-05
 * Time: 09:11
 */

class Permission extends DASH_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * @return object
     */
    public function index()
    {
        if(has_permission('show')) {
            if ($this->ion_auth->logged_in()) {
                $data['title'] = 'WMS - Permissions';
                $data['groups'] = $this->permission_model->getGroupsWithPermissions();
                $this->template->load('master', 'permission/show', $data);
            }
        }
        else{
            $this->wrongState();
        }

    }

    public function createNewController(){
        if(has_permission('create')){
            $postData = $this->input->post();
            if(has_permission('create')) {
                if(isset($postData) && !empty ($postData)){
                    $newController = array(
                        'name' => $postData['name'],
                        'menu' => 'controller',
                        'description' => $postData['description'],
                        'active' => '1'
                    );
                    $this->permission_model->createController($newController);
                    redirect('dashboard/permission');
                }
            }
            else{
                $this->wrongState();
            }
        }
    }

    public function handleEditPermission(){
        if(has_permission('edit')){
        $postData = $this->input->post();

            if($this->permission_model->checkPermission($postData['data']['cid'],$postData['data']['gid'])){
                $updateArray = array(
                    $postData['data']['method'] => ($postData['checkbox'] == 'false') ? '0' : '1',
                );
                $this->permission_model->updatePermission($postData['data']['cid'],$postData['data']['gid'],$updateArray);
            }
            else{
                $insertData = array(
                    'user_group_id' => $postData['data']['gid'],
                    'controller_id' => $postData['data']['cid'],
                    $postData['data']['method'] => ($postData['checkbox'] == 'false') ? '0' : '1',
                );
                $this->permission_model->insertNewPermission($insertData);
            }
        }
        else{
            $this->wrongState();
        }

    }

    public function handleEditController(){
        if(has_permission('edit')){
            $postData = $this->input->post();

            $data['controller'] = $this->permission_model->getController($postData['cid']);
            if($data){

                $pageView =  $this->load->view('dashboard/permission/partials/editController',$data,true);

                $response = array(
                    'status' => '1',
                    'view' => $pageView,
                );

            } else {
                $response = array(
                    'status' => 0,
                );
            }
            echo json_encode($response);
        }
        else{
            $this->wrongState();
        }
        }

        public function handleSendEditController(){
            if(has_permission('edit')){
                $postData=$this->input->post();
                $updateData=array(
                    'name' => $postData['name'],
                    'description' => $postData['description'],
                );

                if($this->permission_model->updateController($postData['id'],$updateData)){

                    $data['groups'] = $this->permission_model->getGroupsWithPermissions();

                    $view2 = $this->load->view('dashboard/permission/partials/setPermission',$data,true);
                    $view = $this->load->view('dashboard/permission/partials/createController','',true);

                    $response = array(
                        'status' => '1',
                        'view' => $view,
                        'view2' => $view2,

                    );

                } else {
                    $response = array(
                        'status' => 0,
                    );
                }
                echo json_encode($response);
            }
            else{
                $this->wrongState();
            }
    }

    public function handleReturnToAdd(){
        $postData =$this->input->post();
        if($postData['one'] == '1'){
            $view = $this->load->view('permission/partials/createController',true);

            $response=array(
                'status' => '1',
                'view' => $view
            );
        }
        else{
            $response=array(
                'status' => '0'
            );
        }

        json_encode($response);
    }

    public function handleChangeGroupOrder(){
        $postData = $this->input->post();
        
        if( isset($postData['json']) && !empty($postData['json']) ){
            $data = $postData['json'];
            foreach($data as $order => $d){
                $this->permission_model->updateGroupOrder($d['id'],array('order'=>$order));
            }
        }
    }

}