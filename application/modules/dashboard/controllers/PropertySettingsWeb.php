<?php


class PropertySettingsWeb extends DASH_Controller
{

    public $property_id;
    public $upload_dir;
    public $file_dir;
    public $favicon_dir;
    public $image_sizes;


    public function __construct()
    {
        parent::__construct();
        $this->property_id = $this->session->userdata('active_property');
        $this->image_sizes = faviconSizes();
        $this->upload_dir = './uploads/' . $this->property_id . '/logo/';
        $this->file_dir = './uploads/' . $this->property_id . '/files/';
        $this->favicon_dir = './uploads/' . $this->property_id . '/favicon/';
        $this->load->model('PropertySettingsWeb_model','psw_model');
        $this->load->library('FrontCache',array('property_id'=>$this->property_id,'type'=>'property'),'frontCache');
    }

    public function index(){
        if(has_permission('show') && has_permission('edit')){
            $data['title'] = 'Nastavenie zariadenia';
            $data['languages'] = $this->psw_model->getLanguages($this->property_id);
            $data['currencies'] = $this->psw_model->getCurrencies();
            $data['psw'] = $this->psw_model->getData($this->property_id);
            $data['deliveries'] = $this->psw_model->getDeliveries($this->property_id);

            if(isset($data['psw']['notification_email']) && !empty($data['psw']['notification_email'])){
                $data['psw']['notification_email'] = explode(',',$data['psw']['notification_email']);
            }

            if(isset($data['psw']['terms_and_condition_type']) && !empty($data['psw']['terms_and_condition_type']) && $data['psw']['terms_and_condition_type'] == 'file'){
                if(isset($data['psw']['terms_and_conditions']) && !empty($data['psw']['terms_and_conditions'])){
                    $data['psw']['terms_and_conditions_file'] = upload_url($this->property_id.'/files/'.$data['psw']['terms_and_conditions']);
                }
            }

            if(isset($data['psw']['gdpr_condition_type']) && !empty($data['psw']['gdpr_condition_type']) && $data['psw']['gdpr_condition_type'] == 'file'){
                if(isset($data['psw']['gdpr_conditions']) && !empty($data['psw']['gdpr_conditions'])){
                    $data['psw']['gdpr_conditions_file'] = upload_url($this->property_id.'/files/'.$data['psw']['gdpr_conditions']);
                }
            }


            $data['openingHours'] =  isset($data['psw']['openingHours']) && !empty($data['psw']['openingHours']) ? unserialize($data['psw']['openingHours']) : array();
            $data['sms'] =  isset($data['psw']['sms']) && !empty($data['psw']['sms']) ? unserialize($data['psw']['sms']) : array();
            if(isset($data['sms']['integration_id']) && !empty($data['sms']['integration_id']) && isset($data['sms']['sms_farm']) && !empty($data['sms']['sms_farm'])){
               $data['sms_credit'] = $this->getRemainingCredit($data['sms']['integration_id'],$data['sms']['sms_farm']);
            }
            $this->template->load('master', 'propertySettingsWeb/show', $data);
        }
        else{
            $this->wrongState();
        }
    }

    private function getRemainingCredit($code,$integration_id){

        $signature = substr(md5($code.$integration_id), 10, 11);

        $values = array(
            "integration_id" => $integration_id,
            "signature" => $signature
        );

        $client = new SoapClient("http://app.smsfarm.sk/api/?wsdl");

        $result = $client->GetCreditAmount($values);
        if(!empty($result)){
            return $result->GetCreditAmountResult;
        }
        else{
            return '0.0';
        }
    }

    public function editProcess(){
        $post = $this->input->post();

        $languages = $post['language'];
        $openingHours = $post['openingHours'];
        $smsData = $post['sms'];

        if(isset($post['notification_email']) && !empty($post['notification_email'])){
            $post['notification_email'] = implode(',',$post['notification_email']);
        }
        else{
            $post['notification_email'] = '';
        }

        $web = !empty($post['web']) ? rtrim($post['web'],'/') : '';
        $post['web'] = $web;

        if(isset($post['removeImg']) && !empty($post['removeImg']) && $post['removeImg'] == '1'){
            unlink($this->upload_dir.$post['oldImg']);
        }

        if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
            $temp_name  = $_FILES['image']['name'];
            $extension  = strtolower(substr(strrchr($temp_name, '.'), 1));
            $img_name = generateToken(30);

            if (!file_exists($this->upload_dir)) {
                mkdir($this->upload_dir, 0775, true);
            }

            $image_name = $img_name . '.' . $extension;

            copy($_FILES['image']['tmp_name'], $this->upload_dir . $image_name);

            if(isset($post['hasImg']) && !empty($post['hasImg']) && $post['hasImg'] == '1' && empty($post['removeImg'])){
                unlink($this->upload_dir.$post['oldImg']);
            }

            $post['logo'] = $image_name;
        }

        if(isset($post['removeFavicon']) && !empty($post['removeFavicon']) && isset($post['oldFavicon']) && !empty($post['oldFavicon'])){
            foreach($this->image_sizes as $key => $favicon_size){
                $img = explode('.',$post['oldFavicon']);
                unlink($this->favicon_dir.$img[0].'-'.$key.'.'.$img[1]);
            }
            unlink($this->favicon_dir.$post['oldFavicon']);
            $post['favicon'] = '';
        }


        if(isset($_FILES['favicon']['name']) && !empty($_FILES['favicon']['name'])){
            $temp_name  = $_FILES['favicon']['name'];
            $extension  = strtolower(substr(strrchr($temp_name, '.'), 1));
            $favicon_name = generateToken(30);


            if (!file_exists($this->favicon_dir)) {
                mkdir($this->favicon_dir, 0775, true);
            }

            copy($_FILES['favicon']['tmp_name'], $this->favicon_dir . $favicon_name.'.'.$extension);

            $this->load->library('uploader');
            $this->uploader->resizeImage($this->favicon_dir, $favicon_name, $extension, $this->image_sizes);

            if(isset($post['hasFavicon']) && !empty($post['hasFavicon']) && isset($post['oldFavicon']) && !empty($post['oldFavicon']) && empty($post['removeFavicon'])){
                foreach($this->image_sizes as $key => $favicon_size){
                    $img = explode('.',$post['oldFavicon']);
                    unlink($this->favicon_dir.$img[0].'-'.$key.'.'.$img[1]);
                }
                unlink($this->favicon_dir.$post['oldFavicon']);
                $post['favicon'] = '';
            }

            $post['favicon'] = $favicon_name.'.'.$extension;
        }

        if(isset($_FILES['gdpr_condition_file']['name']) && !empty($_FILES['gdpr_condition_file']['name']) && $post['gdpr_condition_type'] == 'file'){
            $temp_name  = $_FILES['gdpr_condition_file']['name'];
            $extension  = strtolower(substr(strrchr($temp_name, '.'), 1));
            $gdpr = generateToken(30);

            if (!file_exists($this->file_dir)) {
                mkdir($this->file_dir, 0775, true);
            }

            $gdpr_file_name = $gdpr . '.' . $extension;

            copy($_FILES['gdpr_condition_file']['tmp_name'], $this->file_dir . $gdpr_file_name);

            if(isset($post['old_gdpr_condition_file']) && !empty($post['old_gdpr_condition_file'])){
                unlink($this->file_dir.$post['old_gdpr_condition_file']);
            }

            $post['gdpr_conditions'] = $gdpr_file_name;
        }

        if(isset($_FILES['terms_and_condition_file']['name']) && !empty($_FILES['terms_and_condition_file']['name']) && $post['terms_and_condition_type'] == 'file'){
            $temp_name  = $_FILES['terms_and_condition_file']['name'];
            $extension  = strtolower(substr(strrchr($temp_name, '.'), 1));
            $tac = generateToken(30);


            if (!file_exists($this->file_dir)) {
                mkdir($this->file_dir, 0775, true);
            }

            $tac_file_name = $tac . '.' . $extension;

            copy($_FILES['terms_and_condition_file']['tmp_name'], $this->file_dir . $tac_file_name);

            if(isset($post['old_terms_and_condition_file']) && !empty($post['old_terms_and_condition_file'])){
                unlink($this->file_dir.$post['old_terms_and_condition_file']);
            }

            $post['terms_and_conditions'] = $tac_file_name;
        }

        if($post['gdpr_condition_type'] == 'link'){
            if(isset($post['old_gdpr_condition_file']) && !empty($post['old_gdpr_condition_file'])){
                unlink($this->file_dir.$post['old_gdpr_condition_file']);
            }
            $post['gdpr_conditions'] = $post['gdpr_condition_link'];
        }
        if($post['terms_and_condition_type'] == 'link'){
            if(isset($post['old_terms_and_condition_file']) && !empty($post['old_terms_and_condition_file'])){
                unlink($this->file_dir.$post['old_terms_and_condition_file']);
            }
            $post['terms_and_conditions'] = $post['terms_and_condition_link'];
        }
        if($post['gdpr_condition_type'] == ''){
            if(isset($post['old_gdpr_condition_file']) && !empty($post['old_gdpr_condition_file'])){
                unlink($this->file_dir.$post['old_gdpr_condition_file']);
            }
            $post['gdpr_conditions'] = '';
        }
        if($post['terms_and_condition_type'] == ''){
            if(isset($post['old_terms_and_condition_file']) && !empty($post['old_terms_and_condition_file'])){
                unlink($this->file_dir.$post['old_terms_and_condition_file']);
            }
            $post['terms_and_conditions'] = '';
        }

        unset($post['terms_and_condition_link']);
        unset($post['gdpr_condition_link']);
        unset($post['old_gdpr_condition_file']);
        unset($post['old_terms_and_condition_file']);

        $this->psw_model->removeAllPropertyLanguages($this->property_id);
        if(isset($languages) && !empty($languages)){
            foreach($languages as $lang => $language){
                $insertLang = array(
                    'property_id' => $this->property_id,
                    'language_code' => $lang,
                );
                $this->psw_model->insertLang($insertLang);
            }
        }
        unset($post['openingHours']);

        $post['openingHours'] = serialize($openingHours);
        unset($post['sms']);
        $post['sms'] = serialize($smsData);

        unset($post['hasImg']);
        unset($post['oldImg']);
        unset($post['removeImg']);
        unset($post['hasFavicon']);
        unset($post['oldFavicon']);
        unset($post['removeFavicon']);
        unset($post['language']);

        $this->psw_model->updateSettings($this->property_id,$post);

        $this->frontCache->deleteCache();
        $this->loglib->storeLog('propertySettingsWeb','edit',$this->property_id);
        $this->session->set_flashdata(array('successMessage'=>'Záznam bol úspešne upravený!'));
        return redirect('dashboard/propertySettingsWeb');
    }

    public function createProcess(){
        $post = $this->input->post();

        $languages = isset($post['language']) && !empty($post['language']) ? $post['language'] : array();
        $openingHours = $post['openingHours'];
        $smsData = $post['sms'];
        $post['property_id'] = $this->property_id;

        if(isset($post['notification_email']) && !empty($post['notification_email'])){
            $post['notification_email'] = implode(',',$post['notification_email']);
        }
        else{
            $post['notification_email'] = '';
        }


        $web = !empty($post['web']) ? rtrim($post['web'],'/') : '';
        $post['web'] = $web;

        if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
            $temp_name  = $_FILES['image']['name'];
            $extension  = strtolower(substr(strrchr($temp_name, '.'), 1));
            $img_name = generateToken(30);

            if (!file_exists($this->upload_dir)) {
                mkdir($this->upload_dir, 0775, true);
            }

            $image_name = $img_name . '.' . $extension;

            copy($_FILES['image']['tmp_name'], $this->upload_dir . $image_name);


            $post['logo'] = $image_name;
        }

        if(isset($_FILES['gdpr_condition_file']['name']) && !empty($_FILES['gdpr_condition_file']['name']) && $post['gdpr_condition_type'] == 'file'){
            $temp_name  = $_FILES['gdpr_condition_file']['name'];
            $extension  = strtolower(substr(strrchr($temp_name, '.'), 1));
            $gdpr = generateToken(30);

            if (!file_exists($this->file_dir)) {
                mkdir($this->file_dir, 0775, true);
            }

            $gdpr_file_name = $gdpr . '.' . $extension;

            copy($_FILES['gdpr_condition_file']['tmp_name'], $this->file_dir . $gdpr_file_name);

            if(isset($post['old_gdpr_condition_file']) && !empty($post['old_gdpr_condition_file'])){
                unlink($this->file_dir.$post['old_gdpr_condition_file']);
            }

            $post['gdpr_conditions'] = $gdpr_file_name;
        }

        if(isset($_FILES['terms_and_condition_file']['name']) && !empty($_FILES['terms_and_condition_file']['name']) && $post['terms_and_condition_type'] == 'file'){
            $temp_name  = $_FILES['terms_and_condition_file']['name'];
            $extension  = strtolower(substr(strrchr($temp_name, '.'), 1));
            $tac = generateToken(30);


            if (!file_exists($this->file_dir)) {
                mkdir($this->file_dir, 0775, true);
            }

            $tac_file_name = $tac . '.' . $extension;

            copy($_FILES['terms_and_condition_file']['tmp_name'], $this->file_dir . $tac_file_name);

            if(isset($post['old_terms_and_condition_file']) && !empty($post['old_terms_and_condition_file'])){
                unlink($this->file_dir.$post['old_terms_and_condition_file']);
            }

            $post['terms_and_conditions'] = $tac_file_name;
        }

        if($post['gdpr_condition_type'] == 'link'){
            if(isset($post['old_gdpr_condition_file']) && !empty($post['old_gdpr_condition_file'])){
                unlink($this->file_dir.$post['old_gdpr_condition_file']);
            }
            $post['gdpr_conditions'] = $post['gdpr_condition_link'];
        }

        if(isset($_FILES['favicon']['name']) && !empty($_FILES['favicon']['name'])){
            $temp_name  = $_FILES['favicon']['name'];
            $extension  = strtolower(substr(strrchr($temp_name, '.'), 1));
            $favicon_name = generateToken(30);


            if (!file_exists($this->favicon_dir)) {
                mkdir($this->favicon_dir, 0775, true);
            }

            copy($_FILES['favicon']['tmp_name'], $this->favicon_dir . $favicon_name.'.'.$extension);

            $this->load->library('uploader');
            $this->uploader->resizeImage($this->favicon_dir, $favicon_name, $extension, $this->image_sizes);

            $post['favicon'] = $favicon_name.'.'.$extension;
        }


        if($post['terms_and_condition_type'] == 'link'){
            if(isset($post['old_terms_and_condition_file']) && !empty($post['old_terms_and_condition_file'])){
                unlink($this->file_dir.$post['old_terms_and_condition_file']);
            }
            $post['terms_and_conditions'] = $post['terms_and_condition_link'];
        }
        if($post['gdpr_condition_type'] == ''){
            if(isset($post['old_gdpr_condition_file']) && !empty($post['old_gdpr_condition_file'])){
                unlink($this->file_dir.$post['old_gdpr_condition_file']);
            }
            $post['gdpr_conditions'] = '';
        }
        if($post['terms_and_condition_type'] == ''){
            if(isset($post['old_terms_and_condition_file']) && !empty($post['old_terms_and_condition_file'])){
                unlink($this->file_dir.$post['old_terms_and_condition_file']);
            }
            $post['terms_and_conditions'] = '';
        }

        unset($post['terms_and_condition_link']);
        unset($post['gdpr_condition_link']);
        unset($post['old_gdpr_condition_file']);
        unset($post['old_terms_and_condition_file']);
        unset($post['hasImg']);
        unset($post['removeImg']);
        unset($post['oldImg']);
        unset($post['hasFavicon']);
        unset($post['removeFavicon']);
        unset($post['oldFavicon']);

        if(isset($languages) && !empty($languages)){
            foreach($languages as $lang => $language){
                $insertLang = array(
                    'property_id' => $this->property_id,
                    'language_code' => $lang,
                );
                $this->psw_model->insertLang($insertLang);
            }
        }
        unset($post['openingHours']);
        $post['openingHours'] = serialize($openingHours);
        unset($post['sms']);
        $post['sms'] = serialize($smsData);

        unset($post['language']);
        $this->psw_model->insertSettings($post);
        $this->frontCache->deleteCache();
        $this->loglib->storeLog('propertySettingsWeb','create',$this->property_id);
        $this->session->set_flashdata(array('successMessage'=>'Záznam bol úspešne vytvorený!'));
        return redirect('dashboard/propertySettingsWeb');
    }

    public function getGeoLocation(){
        $post = $this->input->post();

        $address = $post['address'].', '.$post['city'];
        $request = array(
            'key' => '4ac76fcb1f4b49af94ebc2da9285c9e9',
            'q' => $address
        );

        $this->load->library('curl');
        $this->curl->create('https://api.opencagedata.com/geocode/v1/json?'.http_build_query($request));
        $response = json_decode($this->curl->execute(),true);

        if(isset($response['results']['0']['geometry']) && !empty($response['results']['0']['geometry'])){
            $response = array(
                'status' => '1',
                'longitude' => number_format(str_replace(',','.',$response['results']['0']['geometry']['lng']),'6','.',''),
                'latitude' => number_format(str_replace(',','.',$response['results']['0']['geometry']['lat']),'6','.',''),
            );
        }
        else{
            $response = array(
                'status' => '0',
            );
        }

        echo json_encode($response);

    }
}