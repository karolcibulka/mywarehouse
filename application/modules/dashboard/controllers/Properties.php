<?php

class Properties extends DASH_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Properties_model', 'propertiesModel');
        //$this->load->model('ExternalSystem_model', 'es_model');

    }

    /**
     * @return object
     */

    public function handleChangeProperty($id)
    {
        $this->session->set_userdata('active_property', $id);
        echo json_encode(array(
            'status' => '1'
        ));
    }

    public function index()
    {
        if (has_permission('show')) {
            $data['title']      = 'Zariadenia';
            $data['properties'] = $this->propertiesModel->getProperties();
            $data['programs']   = $this->propertiesModel->getPrograms();
            //$data['versions']   = $this->propertiesModel->getVersions();
            $this->template->load('master', 'properties/show', $data);
        } else {
            $this->wrongState();
        }
    }

    public function create()
    {
        if (has_permission('edit')) {
            $data['title']    = 'Vytvorenie zariadenia';
            $data['users']    = $this->propertiesModel->getUsers();
            $data['roles']    = $this->propertiesModel->getRoles();
            //$data['versions'] = $this->propertiesModel->getVersions();
            $data['programs'] = $this->propertiesModel->getPrograms();
            $this->template->load('master', 'properties/partials/create', $data);
        } else {
            $this->wrongState();
        }
    }

    public function createProcess()
    {
        $postData = $this->input->post();


        if (isset($postData['name']) && !empty($postData['name'])) {
            $propertyID = $this->propertiesModel->insertProperty(
                array(
                    'program_id' => $postData['program_id'],
                    'name' => $postData['name'],
                    'system_id' => $postData['system_id'],
                    'server' => $postData['server'],
                    'port' => $postData['port'],
                    'cash_desk_id' => $postData['cash_desk_id'],
                    'version' => $postData['version'],
                    'active' => '1',
                    'deleted' => '0',
                    'token' => generateToken(50)
                )
            );

            if (isset($postData['user']) && !empty($postData['user'])) {
                foreach ($postData['user'] as $userID => $user) {
                    $true = $this->propertiesModel->insertUserProperties($user, $propertyID);
                }
            }

            if (isset($postData['first_user']) && !empty($postData['first_user']) && $postData['createUser'] == '1') {
                if ($this->propertiesModel->emailExist($postData['first_user']['email'])) {
                    $insertUser = array(
                        'email' => $postData['first_user']['email'],
                        'first_name' => $postData['first_user']['first_name'],
                        'last_name' => $postData['first_user']['last_name'],
                        'lang' => 'sk',
                        'active' => '1',
                        'password' => $this->bcrypt->hash($postData['first_user']['password']),
                    );

                    if ($insertID = $this->propertiesModel->insertUser($insertUser)) {
                        $this->propertiesModel->insertConnection(array('user_id' => $insertID, 'property_id' => $propertyID));

                        if (isset($postData['first_user']['roles']) && !empty($postData['first_user']['roles'])) {
                            foreach ($postData['first_user']['roles'] as $role) {
                                $this->propertiesModel->insertConnectionPermission(array('user_id' => $insertID, 'group_id' => $role));
                            }
                        }
                    }
                }
            }

            foreach (developerIDs() as $id) {
                $this->propertiesModel->insertConnection(array('user_id' => $id, 'property_id' => $propertyID));
            }

            $this->session->set_userdata('active_property', $propertyID);
            $this->loglib->storeLog('properties', 'create', $propertyID);
            $this->session->set_flashdata(array('successMessage' => 'Záznam bol úspešne vytvorený!'));
            redirect(base_url('dashboard/properties'));
        } else {
            redirect(base_url('dashboard/properties/create'));
        }

    }

    public function editProcess($id)
    {
        $postData = $this->input->post();

        if (isset($postData['name']) && !empty($postData['name'])) {
            $this->propertiesModel->updateProperty(
                array(
                    'version'=>$postData['version'],
                    'name' => $postData['name'],
                    'program_id' => $postData['program_id'],
                    'system_id' => $postData['system_id'],
                    'server' => $postData['server'],
                    'port' => $postData['port'],
                    'cash_desk_id' => $postData['cash_desk_id']
                ), $id);
            if (isset($postData['user']) && !empty($postData['user'])) {

                $this->propertiesModel->deleteAllUsersFromProperty($id);

                foreach ($postData['user'] as $userID => $user) {
                    $true = $this->propertiesModel->insertUserProperties($user, $id);
                }
            }

            $this->loglib->storeLog('properties', 'edit', $id);
            $this->session->set_flashdata(array('successMessage' => 'Záznam bol úspešne upravený!'));
            redirect(base_url('dashboard/properties'));
        } else {
            redirect(base_url('dashboard/properties/edit/' . $id));
        }
    }

    public function changeProgram()
    {
        $postData = $this->input->post();
        $this->propertiesModel->updateProperty(array('program_id' => $postData['program_id']), $postData['id']);
        $this->loglib->storeLog('properties', 'changeProgram', $postData['id']);
        echo json_encode(array(
            'status' => '1'
        ));
    }

    public function changeVersion()
    {
        $postData = $this->input->post();
        $this->propertiesModel->updateProperty(array('version' => $postData['version']), $postData['id']);
        $this->loglib->storeLog('properties', 'changeVersion', $postData['id']);
        echo json_encode(array(
            'status' => '1'
        ));
    }


    public function edit($id)
    {
        if (has_permission('edit')) {
            $data['title']          = 'Vytvorenie zariadenia';
            $data['property']       = $this->propertiesModel->getPropertyDev($id);
            $data['property']['id'] = $id;
            //$data['versions']       = $this->propertiesModel->getVersions();
            $data['programs']       = $this->propertiesModel->getPrograms();
            if (isset($data['property']['users']) && !empty($data['property']['users'])) {
                $data['property']['users'] = explode('|', $data['property']['users']);
            }
            $data['users'] = $this->propertiesModel->getUsers();

            $this->template->load('master', 'properties/partials/edit', $data);
        } else {
            $this->wrongState();
        }
    }

    public function activity($id, $state)
    {
        if (has_permission('edit')) {
            $this->loglib->storeLog('properties', 'active', $id);
            $this->propertiesModel->updateProperty(array('active' => $state), $id);
            $this->session->set_flashdata(array('successMessage' => 'Záznam bol úspešne upravený!'));
            return redirect('dashboard/properties');
        } else {
            $this->wrongState();
        }
    }

    public function checkConnection($id)
    {
        $settings = $this->propertiesModel->getPropertyDev($id);
        $this->loglib->storeLog('properties', 'checkConnection', $id);
        if (isset($settings['id']) && !empty($settings['id']) && isset($settings['server']) && !empty($settings['server']) && isset($settings['port']) && !empty($settings['port'])) {
            $this->load->library('ExternalSystems/BlueGastro', $settings, 'blueGastro');

            if ($this->blueGastro->checkConnection()) {
                $res = array(
                    'status' => '1'
                );
            } else {
                $res = array(
                    'status' => '0',
                );
            }
        } else {
            $res = array(
                'status' => '0',
            );
        }

        echo json_encode($res);
    }

    public function sync($id)
    {
        $settings = $this->propertiesModel->getPropertyDev($id);
        $this->loglib->storeLog('properties', 'sync', $id);
        if (isset($settings['id']) && !empty($settings['id']) && isset($settings['server']) && !empty($settings['server']) && isset($settings['port']) && !empty($settings['port'])) {
            $this->load->library('ExternalSystems/BlueGastro', $settings, 'blueGastro');

            if ($this->blueGastro->checkConnection()) {
                if ($data = $this->blueGastro->getPaymentTypes()) {
                    $this->es_model->deactivate($settings['id'], 'external_payment_type_list');
                    foreach ($data as $d) {
                        $storeItem = array(
                            'property_id' => $settings['id'],
                            'active' => '1',
                            'external_id' => $d['id'],
                            'name' => $d['name'],
                            'enabled' => $d['enabled'],
                        );

                        $this->es_model->store('external_payment_type_list', $storeItem);
                    }
                }
                if ($data2 = $this->blueGastro->getOrderTypes()) {
                    $this->es_model->deactivate($settings['id'], 'external_order_type_list');
                    foreach ($data2 as $d) {
                        $storeItem = array(
                            'property_id' => $settings['id'],
                            'active' => '1',
                            'external_id' => $d['id'],
                            'name' => $d['name'],
                        );

                        $this->es_model->store('external_order_type_list', $storeItem);
                    }

                }
                if ($data3 = $this->blueGastro->getDiscountTypes()) {
                    $this->es_model->deactivate($settings['id'], 'external_discount_type_list');
                    foreach ($data3 as $d) {
                        $storeItem = array(
                            'property_id' => $settings['id'],
                            'active' => '1',
                            'external_id' => $d['id'],
                            'enabled' => $d['enabled'],
                            'type' => $d['type'],
                            'name' => $d['name'],
                        );

                        $this->es_model->store('external_discount_type_list', $storeItem);
                    }

                }

                $res = array(
                    'status' => '1'
                );
            } else {
                $res = array(
                    'status' => '0',
                );
            }
        } else {
            $res = array(
                'status' => '0',
            );
        }

        echo json_encode($res);
    }

    public function delete($id)
    {
        if (has_permission('delete')) {

            $this->propertiesModel->updateProperty(array('deleted' => '1'), $id);
            $this->loglib->storeLog('properties', 'delete', $id);
            $this->session->set_flashdata(array('successMessage' => 'Záznam bol úspešne zmazaný!'));
            return redirect('dashboard/properties');
        } else {
            $this->wrongState();
        }
    }


    public function validateUser()
    {
        $postData = $this->input->post();
        $status   = '0';

        if ($this->propertiesModel->emailExist($postData['email'])) {
            $status = '1';
        }

        echo json_encode(array(
            'status' => $status
        ));
    }

    public function setProperty($id)
    {
        if (has_permission('edit')) {
            $this->session->set_userdata('active_property', $id);
            return redirect('dashboard/dashboard');
        }
    }

    public function createNewToken($id)
    {
        if (has_permission('edit')) {
            $updateData = array(
                'token' => generateToken(50)
            );
            $this->propertiesModel->updateProperty($updateData, $id);
            return redirect('dashboard/properties');
        } else {
            return redirect('dashboard/dashboard');
        }
    }
}