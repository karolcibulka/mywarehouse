<?php
/**
 * Created by PhpStorm.
 * User: karol
 * Date: 2019-03-05
 * Time: 09:11
 */

class Dashboard extends DASH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Dashboard_model','dashboard_model');
    }
    /**
     * @return object
     */
    public function index()
    {
        $data['title'] = 'Dashboard';
        $this->template->load('master','show',$data);
    }

    public function getStats($type){
       $data['stats'] =  $this->dashboard_model->getStats($type, $this->property_id);
       $view = $this->load->view('dashboard/dashboard/statistics',$data,true);

       echo json_encode(array(
           'status' => '1',
           'view' => $view
       ));
    }

    public function redirectToPage(){
        if($web = $this->dashboard_model->getWeb($this->property_id)){
            redirect($web,'location',301);
        }
        else{
            return false;
        }
    }

    public function changePropertySelling($state,$propertyID = false){
        if($propertyID){
            $this->dashboard_model->updateProperty($propertyID,array('selling'=>$state));
            $this->loglib->storeLog('properties','selling',$propertyID);
        }
        else{
            $this->dashboard_model->updateProperty($this->property_id,array('selling'=>$state));
            $this->loglib->storeLog('properties','selling',$this->property_id);
        }

        $this->load->library('FrontCache',array('property_id'=>$this->property_id,'type'=>'property'),'frontCache');
        $this->frontCache->deleteCache();

        if($state === '0'){
            $this->session->set_flashdata(array('successMessage'=>'Predaj zakázaný!'));
        }
        else{
            $this->session->set_flashdata(array('successMessage'=>'Predaj povolený!'));
        }

        if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])){
            redirect($_SERVER['HTTP_REFERER'],'location',301);
        }
        else{
            redirect(base_url('dashboard/dashboard'));
        }
    }
}