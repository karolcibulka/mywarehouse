<?php

class Navigation extends DASH_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Navigation_model','navigation_model');

    }

    /**
     * @return object
     */


    public function index(){
        if(has_permission('show')){
            $data['navs'] = $this->navigation_model->getNavItems();
            $data['categories'] = $this->navigation_model->getCategories();
            $data['controllers'] = $this->navigation_model->getControllers();

            if(!$data['icons'] = $this->cache->file->get('icons')){
                $data['icons'] = $this->navigation_model->getIcons();
                $this->cache->file->save('icons',$data['icons'],$this->cache_store_length);
            }

            $this->template->load('master','navigation/show',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function storeNavigation(){
        if(has_permission('create')){
            $post = $this->input->post();

            if($id = $this->navigation_model->storeNavigationItem($post)){
                setSuccessState('Úspešne uložený záznam');
                $this->cache->file->delete('navigation');
            }

            redirect('dashboard/navigation');

        }
        else{
            $this->wrongState();
        }
    }

    public function categories(){
        if(has_permission('show')) {
            $data['navs'] = $this->navigation_model->getCategories();
            $this->template->load('master','navigation/categories',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function controllers(){
        if(has_permission('show')){
            $data['controllers'] = $this->navigation_model->getControllers();
            $this->template->load('master','navigation/controllers',$data);
        }
    }

    public function handleReorderControllers()
    {
        if (has_permission('edit')) {
            $post = $this->input->post();
            foreach ($post['json'] as $order_1 => $item) {
                $this->navigation_model->updateController($item['id'], array('order' => $order_1));
            }
        }
    }

    public function handleDeleteController($id){

        $response = array(
            'status' => 0,
        );

        if(has_permission('delete')){
            $this->navigation_model->updateController($id, array('deleted'=>1));

            $response['status'] = 1;
            $response['message'] = 'Úspešne zmazaný prvok!';
        }



        echo json_encode($response);


    }

    public function handleDeleteNavigation($id){
        $response = array('status'=>0);
        if(has_permission('delete')){
            $this->navigation_model->updateNavigation($id,array('deleted'=>1));
            $response['status'] = 1;
            $response['message'] = 'Úspešne zmazaný prvok!';
        }

        $this->cache->file->delete('navigation');

        echo json_encode($response);
    }

    public function reorder(){
        $post = $this->input->post();

        if(has_permission('edit')){
            foreach($post['json'] as $order_1 => $item){
                $this->navigation_model->updateNavigation($item['id'],array('order'=>$order_1,'parent'=>0));
                if(isset($item['children']) && !empty($item['children'])){
                    foreach($item['children'] as $order_2 => $children){
                        $this->navigation_model->updateNavigation($children['id'],array('order'=>$order_2,'parent'=>$item['id']));
                        if(isset($children['children']) && !empty($children['children'])){
                            foreach($children['children'] as $order_3 => $grand_children){
                                $this->navigation_model->updateNavigation($grand_children['id'],array('order'=>$order_3,'parent'=>$children['id']));
                            }
                        }
                    }
                }
            }

            $this->cache->file->delete('navigation');
        }
    }

    public function editNavigation($navigation_id){
        if(has_permission('edit')){

            $data['navigation'] = $this->navigation_model->getNavigation($navigation_id);
            $data['controllers'] = $this->navigation_model->getControllers();
            $data['categories'] = $this->navigation_model->getCategories();

            if(!$data['icons'] = $this->cache->file->get('icons')){
                $data['icons'] = $this->navigation_model->getIcons();
                $this->cache->file->save('icons',$data['icons'],$this->cache_store_length);
            }

            $this->template->load('master','navigation/edit',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function editNavigationProcess($id){
        if(has_permission('edit')){

            $post = $this->input->post();

            if($id = $this->navigation_model->updateNavigation($id,$post)){
                setSuccessState('Úspešne upravený záznam');
                $this->cache->file->delete('navigation');
            }

            redirect('dashboard/navigation');
        }
        else{
            $this->wrongState();
        }
    }


    public function deleteNavigationCategory($id){
        if(has_permission('delete')){
            if($id = $this->navigation_model->updateNavigationCategory($id,array('deleted'=>1))){
                setSuccessState('Úspešne upravený záznam');
                $this->cache->file->delete('navigation');
            }

            redirect('dashboard/navigation/categories');
        }
        else{
            $this->wrongState();
        }
    }

    public function editNavigationCategoryProcess($id){
        if(has_permission('edit')){

            $post = $this->input->post();

            if($id = $this->navigation_model->updateCategory($id,$post)){
                setSuccessState('Úspešne upravená kategória');
                $this->cache->file->delete('navigation');
            }

            redirect('dashboard/navigation/categories');
        }
        else{
            $this->wrongState();
        }
    }

    public function editNavigationCategory($id){
        if(has_permission('edit')){

            $data['category'] = $this->navigation_model->getCategory($id);
            $this->template->load('master','navigation/editCategory',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function reorderCategories(){
        $post = $this->input->post();

        if(has_permission('edit')) {
            foreach($post['json'] as $order_1 => $item){
                $this->navigation_model->updateNavigationCategory($item['id'],array('order'=>$order_1));
            }
        }

        $this->cache->file->delete('navigation');
    }


}
