<?php


class Translations extends DASH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Translations_model','translations_model');

    }

    public function index(){
        if(has_permission('show')){

            $data['folders'] = $this->getLanguages();
            $data['files'] = $this->getLanguageFiles();

            $this->template->load('master','translations/show',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function editLanguage($language){
        if(has_permission('edit')){
            $data['language'] = $this->translations_model->getLanguage($language);
            $this->template->load('master','translations/edit',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function editProcess($language){
        if(has_permission('edit')){
            $post = $this->input->post();

            $path = APPPATH.'/language/'.$language;
            $new_path = APPPATH.'/language/'.$post['name'];

            rename($path,$new_path);

            $this->translations_model->updateLanguage($language,$post);

            redirect('dashboard/translations');
        }
        else{
            $this->wrongState();
        }
    }

    public function show($file){
        if(has_permission('show')){

            $data['translations'] = $this->getTranslationsByFile($file);
            $data['languages'] = $this->getLanguages();
            $data['file'] = $file;

            $this->template->load('master','translations/item',$data);
        }
        else{
            $this->wrongState();
        }
    }

    public function storeFile(){
        $post = $this->input->post();
        if(has_permission('create')){
            $languages = $this->getLanguages();
            foreach($languages as $language){
                $path = APPPATH.'language/'.$language.'/'.$post['name'].'.php';

                $template = file_get_contents(APPPATH.'/views/matches_templates/language_template.txt');
                $template = str_replace('{{FILE}}',$post['name'],$template);

                if(!file_exists($path)){
                    $file = fopen($path, 'a+');
                    fwrite($file, $template);
                    fclose($file);
                }
            }

            $this->cache->file->delete('language_files');

            redirect('dashboard/translations');
        }
        else{
            $this->wrongState();
        }
    }

    private function _deleteLanguage($path,$language,$delete = true){
        if(is_dir($path)){
            $files = $this->getLanguageFiles('slovak');
            foreach($files as $file){
                $file_path = $path.'/'.$file.'.php';
                unlink($file_path);
            }

            rmdir($path);

            if($delete){
                $this->translations_model->deleteLanguage($language);
            }
        }
    }

    public function deleteLanguage($language){
        if(has_permission('delete')){
            $path = APPPATH.'/language/'.$language;

            $this->_deleteLanguage($path,$language,true);
            $this->cache->file->delete('languages');

            redirect('dashboard/translations');
        }
        else{
            $this->wrongState();
        }
    }

    public function storeTranslations(){
        if(has_permission('create') && has_permission('edit')){
            $post = $this->input->post();
            $post = isset($post['data']) && !empty($post['data']) ? json_decode($post['data'],true) : array();

            $response = array();

            if(!empty($post)){
                foreach($post as $item){
                    $response[$item['language']][$item['file']][$item['key']] = $item['value'];
                }
            }

            if(!empty($response)){
                $this->_remakeLanguages($response);
            }
        }

        echo json_encode(array('status' => 1));
    }

    private function _remakeLanguages($data){
        if(!empty($data)){
            foreach($data as $language => $d){
                if(!empty($d)){
                    foreach($d as $file => $item){
                        $path = APPPATH.'language/'.$language.'/'.$file.'.php';
                        //unlink($path);

                        $template = "<?php \n\n";
                        if(!empty($item)){
                            foreach($item as $key => $i){
                                $template .= '$lang[\''.$key.'\'] = \''.str_replace("'","\'",$i).'\';'."\n";
                            }
                        }

                        file_put_contents($path,$template);
                    }
                }
            }
        }
    }

    public function delete($file){
        if(has_permission('delete')){
            $languages = $this->getLanguages();
            foreach($languages as $language){
                $path = APPPATH.'/language/'.$language.'/'.$file.'.php';

                if(file_exists($path)){
                    unlink($path);
                }
            }

            $this->cache->file->delete('language_files');

            redirect('dashboard/translations');
        }
        else{
            $this->wrongState();
        }
    }

    public function cloneSlovakFolder($folder){
        $files = $this->getLanguageFiles();

        if(!file_exists(APPPATH.'/language/'.$folder)){
            mkdir(APPPATH.'/language/'.$folder,0777,true);
        }

        if(!empty($files)){
            foreach($files as $file){
                $path = APPPATH.'/language/slovak/'.$file.'.php';
                $new_path = APPPATH.'/language/'.$folder.'/'.$file.'.php';

                if(file_exists($path)){
                    $content = file_get_contents($path);
                    $file = fopen($new_path, 'a+');
                    fwrite($file, $content);
                    fclose($file);
                }
            }
        }
    }

    public function getTranslationsByFile($file){
        $folders = $this->getLanguages();

        $response = array();

        $languages = array();

        if(!empty($folders)){
            foreach($folders as $folder){
                $path = APPPATH.'language/'.$folder.'/'.$file.'.php';
                if(file_exists($path)){
                    $languages[$folder] = $this->lang->load($file,$folder,true);
                }
            }
        }

        if(!empty($languages)){
            foreach($languages as $language => $language_item){
                if(!empty($language_item)){
                    foreach($language_item as $key_in_file => $value){
                        $response[$key_in_file][$language] = $value;
                    }
                }
            }
        }

       return $response;
    }

    public function getLanguageFiles($language = 'slovak'){
        $path = APPPATH.'/language/slovak';

        $response = array();

        $files = scandir($path);

        if(!empty($files)){
            foreach($files as $file){
                if($file !== '.' && $file !== '..'){
                    $name = str_replace('.php','',$file);
                    $response[$name] = $name;
                }

            }
        }

        return $response;
    }

    public function getLanguages(){
        $path = APPPATH.'/language';

        $response = array();

        $folders = scandir($path);

        if(!empty($folders)){
            foreach($folders as $folder){
                if($folder !== 'index.html' && $folder !== '.' && $folder !== '..'){
                    $response[$folder] = $folder;
                }
            }
        }

        return $response;
    }

    public function storeLanguage(){
        $post = $this->input->post();
        if(has_permission('delete')){
            if(isset($post['name']) && !empty($post['name'])){
                if(!file_exists(APPPATH.'/language/'.$post['name'])){
                    if($id = $this->translations_model->storeLanguage($post)){
                        $this->cloneSlovakFolder($post['name']);
                    }
                }
            }

            $this->cache->file->delete('languages');
            setSuccessState('Úspešne pridaný jazyk');

            redirect('dashboard/translations');
        }
        else{
            $this->wrongState();
        }
    }

    public function getLanguageContent($language){
        $path = APPPATH.'/language/'.$language.'/global_lang.php';

        if(file_exists($path)){
            $language = $this->lang->load('global_lang',$language,true);
        }
        else{
            die('neexistuje');
        }

        //pre_r($language);

    }

    public function createLanguage($language){

        $template = file_get_contents(APPPATH.'language/slovak/global_lang.php');

        $path = APPPATH.'/language/'.$language;
        if(!file_exists($path)){
            mkdir($path,0777,true);
        }

        $path = $path.'/global_lang.php';
        if(!file_exists($path)){
            $file = fopen($path, 'a+');
            fwrite($file, $template);
            fclose($file);
        }
        else{
            die('tento subor uz existuje');
        }

    }
}