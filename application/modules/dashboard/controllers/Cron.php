<?php
/**
 * Created by PhpStorm.
 * User: karol
 * Date: 2019-05-30
 * Time: 15:02
 */

class Cron extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Cron_model','cron_model');
        $this->load->library('email');
    }


    public function sendCronSummary(){
        $emailData = $this->ci->notifications_model->getCronSummaryData();
        if(isset($emailData) && !empty($emailData)) {
            foreach ($emailData as $data) {
                if (isset($data['reciever']) && !empty($data['reciever'])) {
                    $this->notifications->sendCronSummary($data);
                }
            }
        }
    }
}