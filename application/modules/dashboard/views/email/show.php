<style>
    div.bootstrap-datetimepicker-widget.dropdown-menu.usetwentyfour.bottom {
        width: auto; /*what ever width you want*/
    }
    div.bootstrap-datetimepicker-widget.dropdown-menu.usetwentyfour.top {
        width: auto; /*what ever width you want*/
    }
    div.bootstrap-datetimepicker-widget.dropdown-menu.bottom {
        width: auto; /*what ever width you want*/
    }
    div.bootstrap-datetimepicker-widget.dropdown-menu.top {
        width: auto; /*what ever width you want*/
    }

    .input-group{
        display:block;
    }

    .form-check{
        padding-left:0px;
    }
</style>

<!-- Include Editor style. -->
<link href='https://cdn.jsdelivr.net/npm/froala-editor@3.0.5/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />

<!-- Include JS file. -->
<script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@3.0.5/js/froala_editor.pkgd.min.js'></script>

<div class="content-wrapper">
    <?php
    $this->load->view('partials/breadcrumb',array('noButton'=>'1'));
    ?>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold"><?=lang('segment.email')?></span>
                    </div>
                    <div class="card-body">
                        <form action="<?=base_url('dashboard/email/create')?>" method="post" id="goToCreateForm">
                            <div class="row justify-content-md-center">
                                <div class="col-md-3 col">
                                    <input type="text" name="name" class="form-control goToCreateInput" style="margin:0 auto;" placeholder="Názov emailovej šablóny">
                                </div>

                                <div class="col-md-3 col">
                                    <button type="button" disabled class="btn btn-outline-primary goToCreate" style="margin:0 auto;">Pridať novú emailovú šablónu</button>
                                </div>
                            </div>
                            <div class="row justify-content-md-center">
                                <div class="col-md-3 col" style="margin-top:10px;">
                                    <input type="hidden" name="defaultTemplate" value="0">
                                    <label><input type="checkbox" name="defaultTemplate" value="1">  Chcem upravovať systémovú tému a tú uložiť ako vlastnú</label>
                                </div>
                            </div>
                        </form>
                        <div id="emailContent">
                            <?php
                            $this->load->view('email/partials/main')
                            ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.goToCreateInput').on('input',function(){
        if($(this).val()!==""){
            $('.goToCreate').attr('disabled',false);
        }
        else{
            $('.goToCreate').attr('disabled',true);
        }
    });
    $('.goToCreate').on('click',function(){
        $('#goToCreateForm').submit();
    });
</script>

