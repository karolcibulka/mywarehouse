<link href="https://cdn.jsdelivr.net/npm/grapesjs@0.15.5/dist/css/grapes.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/combine/npm/grapesjs@0.15.5,npm/grapesjs-mjml@0.0.31/dist/grapesjs-mjml.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.8.0/ckeditor.js"></script>
<script src="<?=asset_url('mjml/dist/grapesjs-mjml.min.js')?>"></script>
<script src="<?=asset_url('mjml/dist/grapesjs-plugin-ckeditor.min.js')?>"></script>
<script src="<?=asset_url('mjml/src/grapeck.js')?>"></script>
<style>
    body,
    html {
        height: 100%;
        margin: 0;
    }


    .cke_top {
        background: #fff !important;
    }

    .cke_chrome {
        border: none !important;
    }

    .cke_toolgroup {
        border: none !important;
        background: transparent !important;
        box-shadow: none !important;
    }
    table.table thead>tr>th{
        background-color:white;
        color:black;
    }
    table.table tbody>tr>td{
        background-color:white;
        color:black;
    }
</style>

<div class="savingAbsolute" style="position:absolute;top:50%;left:50%;z-index:10000;font-size:30px;color:black;display:none;">
    Ukladám...
</div>

<div id="gjs" style="height:0px; overflow:hidden">
    <?php if(isset($defaultTemplate) && !empty($defaultTemplate)):?>
        <?=$defaultTemplate?>
    <?php else:?>
        <mjml>
            <mj-body>

            </mj-body>
        </mjml>
    <?php endif;?>
</div>


<script type="text/javascript">
    CKEDITOR.dtd.$editable.a = 1;
	var variables = '<table class="table"> <thead> <tr> <th style="background-color: white;color:black;">Premenná</th> <th style="background-color: white;color:black;">Význam</th> </tr></thead> <tbody> <tr> <td colspan="2" style="background-color: aliceblue;padding-left:40px;color:black;"><strong>Komponenty</strong></td></tr><tr> <td style="background-color: white;color:black;">{body}</td><td style="background-color: white;color:black;">Text prednastavený v emailových nastaveniach</td></tr><tr> <td style="background-color: white;color:black;">{button}</td><td style="background-color: white;color:black;">Tlačidlo na zrušenie rezervácie/hodnotenie rezervácie</td></tr><tr> <td colspan="2" style="background-color: aliceblue;padding-left:40px;color:black;"><strong>Zákazaník</strong></td></tr><tr> <td style="background-color: white;color:black;">{customerName}</td><td style="background-color: white;color:black;">Meno zákazníka</td></tr><tr> <td style="background-color: white;color:black;">{customerLastName}</td><td style="background-color: white;color:black;">Priezvisko zákazníka</td></tr><tr> <td style="background-color: white;color:black;">{customerPhoneNumber}</td><td style="background-color: white;color:black;">Telefónne číslo zákazníka</td></tr><tr> <td style="background-color: white;color:black;">{customerEmail}</td><td style="background-color: white;color:black;">Email zákazníka</td></tr><tr> <td colspan="2" style="padding-left: 40px;background-color: aliceblue;color:black;"><strong>Rezervácia</strong></td></tr><tr> <td style="background-color: white;color:black;">{reservationPersons}</td><td style="background-color: white;color:black;">Počet osôb v rezervácii</td></tr><tr> <td style="background-color: white;color:black;">{reservationChildSeat}</td><td style="background-color: white;color:black;">Detská sedačka v rezervácii</td></tr><tr> <td style="background-color: white;color:black;">{reservationDate}</td><td style="background-color: white;color:black;">Dátum rezervácie</td></tr><tr> <td style="background-color: white;color:black;">{reservationTimeStart}</td><td style="background-color: white;color:black;">Časový údaj začiatku rezervácie</td></tr><tr> <td style="background-color: white;color:black;">{reservationTimeEnd}</td><td style="background-color: white;color:black;">Časový údaj konca rezervácie</td></tr><tr> <td colspan="2" style="background-color: aliceblue;padding-left: 40px;color:black;"><strong>Akcie - rezervácia</strong></td></tr><tr> <td style="background-color: white;color:black;">{reservationRemoveLink}</td><td style="background-color: white;color:black;">Link pre zákazníka - zmazanie rezervácie</td></tr><tr> <td style="background-color: white;color:black;">{reservationRateLink}</td><td style="background-color: white;color:black;">Link pre zákazníka - ohodnotenie služieb</td></tr><tr> <td colspan="2" style="background-color: aliceblue;padding-left: 40px;color:black;"><strong>Zariadenie</strong></td></tr><tr> <td style="background-color: white;color:black;">{propertyName}</td><td style="background-color: white;color:black;">Názov zariadenia</td></tr><tr> <td style="background-color: white;color:black;">{propertyAdress}</td><td style="background-color: white;color:black;">Adresa na ktorom sa nachádza zariadenie</td></tr><tr> <td style="background-color: white;color:black;">{propertyCity}</td><td style="background-color: white;color:black;">Mesto v ktorom sa nachádza zariadenie</td></tr><tr> <td style="background-color: white;color:black;">{propertyUrl}</td><td style="background-color: white;color:black;">Webová stránka zariadenia</td></tr><tr> <td style="background-color: white;color:black;">{propertyPhone}</td><td style="background-color: white;color:black;">Telefónne číslo zariadenia</td></tr><tr> <td style="background-color: white;color:black;">{propertyEmail}</td><td style="background-color: white;color:black;">Email zariadenia</td></tr><tr> <td colspan="2" style="background-color: aliceblue;padding-left: 40px;color:black;"><strong>Zóna</strong></td></tr><tr> <td style="background-color: white;color:black;">{zoneName}</td><td style="background-color: white;color:black;">Názov zóny</td></tr><tr> <td colspan="2" style="background-color: aliceblue;padding-left: 40px;color:black;"><strong>Dátumové údaje</strong></td></tr><tr> <td style="background-color: white;color:black;">{day}</td><td style="background-color: white;color:black;">Deň odoslania emailu</td></tr><tr> <td style="background-color: white;color:black;">{month}</td><td style="background-color: white;color:black;">Mesiac odoslania emailu</td></tr><tr> <td style="background-color: white;color:black;">{year}</td><td style="background-color: white;color:black;">Rok odoslania emailu</td></tr></tbody></table>';
	localStorage.clear();
    var editor = grapesjs.init({
        height: '100%',
        noticeOnUnload: 0,
        storageManager: { autoload: 0 },
        container: '#gjs',
        fromElement: true,
        plugins: ['grapesjs-mjml', 'gjs-plugin-ckeditor'],
        pluginsOpts: {
            'gjs-plugin-ckeditor': {
                position: 'center',
                options: {
                    startupFocus: true,
                    extraAllowedContent: '*(*);*{*}', // Allows any class and any inline style
                    allowedContent: true, // Disable auto-formatting, class removing, etc.
                    enterMode: CKEDITOR.ENTER_BR,
                    extraPlugins: 'sharedspace,justify,colorbutton,panelbutton,font',
                    toolbar: [
                        { name: 'styles', items: ['Font', 'FontSize' ] },
                        ['Bold', 'Italic', 'Underline', 'Strike'],
                        {name: 'paragraph', items : [ 'NumberedList', 'BulletedList']},
                        {name: 'links', items: ['Link', 'Unlink']},
                        {name: 'colors', items: [ 'TextColor', 'BGColor' ]},
                    ],
                }
            }
        },
        richTextEditor: {

        },
        assetManager: {
            upload: 0,
            uploadText: 'Uploading is not available',
        },
    });


    editor.Panels.addButton('options', [
        {
            id: 'variables',
            className: 'fa fa-list',
            command: function(editor1, sender) {
                const modal = editor.Modal;
                modal.setTitle('Premenné, ktoré je možné použiť v emailovej šablóne');
                modal.setContent(variables);
                modal.open();
            },
            attributes: { title: 'Save Template' }
        },
        {
            id: 'save',
            className: 'fa fa-save',
            command: function(editor1, sender) {
                $('#gjs').css('opacity','0.3');
                $('.savingAbsolute').show();
                var html = editor.runCommand('mjml-get-code');
                var exportHtml = html.html;
                var mjml = editor.getHtml();
                var name = '<?=$name?>';

                $.ajax({
                    type: "POST",
                    url: '<?=base_url('dashboard/email/handleCreateEmailTemplate')?>',
                    data: {template:exportHtml,mjml:mjml,name:name},
                    dataType:"json",
                    success : function(data){
                        if(data.status === '1'){
                            window.location.href = '<?=base_url('dashboard/email')?>';
                        }
                    }
                });
            },
            attributes: { title: 'Save Template' }
        }
    ]);

    window.editor = editor;
</script>
