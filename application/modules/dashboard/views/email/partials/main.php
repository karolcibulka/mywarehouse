<style>
    .emailTemplateName{
        font-weight:500;
    }
</style>

<div class="mt-2"></div>
<div id="accordion-child1" style="">
    <?php if(isset($templates) && !empty($templates)):?>
        <div class="card">
            <div class="card-header ">
                <h6 class="card-title">
                    <span class="" data-id="10" style=""><span>Systémová téma</span></span>
                    <span class="template_10" style="float:right;z-index:10000;position:absolute;top:-6px;right:0;display: inline-flex;"><a href="<?=base_url('dashboard/email/checkTemplate/').'10'?>" target="_blank" class="btn btn-primary">Zobraziť</a></span>
                </h6>
            </div>
        </div>
        <?php foreach($templates as $key => $template):?>
            <?php if($template['id']!='10'):?>
                <div class="card">
                    <div class="card-header ">
                        <h6 class="card-title">
                            <span class="changeEmailTemplateName changeEmailTemplate_<?=$template['id']?>" data-id="<?=$template['id']?>" style="cursor:pointer;"><span class="emailTemplateName"><?=$template['name']?></span></span>
                            <span class="template_<?=$template['id']?>" style="float:right;z-index:10000;position:absolute;top:-6px;right:0;display: inline-flex;"><a href="<?=base_url('dashboard/email/checkTemplate/').$template['id']?>" target="_blank" class="btn btn-primary"><span class="activity_<?=$template['id']?>">Zobraziť</span></a> <a href="<?=base_url('dashboard/email/edit/').$template['id']?>" class="btn btn-danger" style="background-color: #26a59a;margin-left:10px;"><span class="activity_<?=$template['id']?>">Upraviť</span></a> <a href="#" class="btn btn-danger deleteEmailTemplate" data-id="<?=$template['id']?>" style="margin-left:10px;"><span class="activity_<?=$template['id']?>">Vymazať</span></a></span>
                        </h6>
                    </div>
                </div>
            <?php endif;?>
        <?php endforeach;?>
    <?php endif;?>
</div>

<script>
    $(document).on('click','.changeEmailTemplateName',function(){
        var id = $(this).data().id;
        var name = $(this).find('.emailTemplateName').html();
        $('.template_'+id).css('top','0');
        var content = '<div style="display: inline-flex;"><input type="text" value="'+name+'" class="form-control" style="max-width:200px;"> <span style="margin-left:10px;" data-id="'+id+'" class="btn btn-success saveNameChange">Uložiť</span></div>';
        $(this).removeClass('changeEmailTemplateName');
        $('.changeEmailTemplate_'+id).html(content);
    });

    $(document).on('click','.saveNameChange',function(){
        var id = $(this).data().id;
        var name = $(this).parent().find('input').val();
        var content = '<span class="emailTemplateName">'+name+'</span>';
        $('.template_'+id).css('top','-6px');
        $('.changeEmailTemplate_'+id).html(content);
        $('.changeEmailTemplate_'+id).addClass('changeEmailTemplateName');
        $.ajax({
            type: "POST",
            url: '<?=base_url('dashboard/email/handleChangeName')?>',
            data: {name:name,id:id},
            dataType:"json",
            success : function(data){
                if(data.status === '1'){

                }
            }
        });
    });

    $(document).on('click','.deleteEmailTemplate',function(){
        var id = $(this).data().id;
        if(confirm('Naozaj chcete zmazať emailovú šablónu?')){
            $(this).closest('.card').remove();
            $.ajax({
                type: "POST",
                url: '<?=base_url('dashboard/email/handleSoftDelete')?>',
                data: {id:id},
                dataType:"json",
                success : function(data){
                    if(data.status === '1'){
                    }
                }
            });
        }
    });
</script>
