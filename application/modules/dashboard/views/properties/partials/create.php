<div class="container">
    <div class="col-sm-12">
        <div class="card card-custom card-stretch">
            <div class="card-header">
                <div class="card-title">
                    <h3>Vytvorenie zariadenia</h3>
                </div>
            </div>
            <div class="card-body">
                <form action="<?=base_url('dashboard/properties/createProcess')?>" method="POST">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Názov zariadenia</label>
                                <input type="text" class="form-control" name="name" value="" placeholder="Názov zariadenia">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Server</label>
                                <input type="text" class="form-control" name="server" value="" placeholder="Server">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Port</label>
                                <input type="text" class="form-control" name="port" value="" placeholder="Port">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>CashDeskID</label>
                                <input type="text" class="form-control" name="cash_desk_id" value="" placeholder="CashDeskID">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>SystemID</label>
                                <input type="text" class="form-control" name="system_id" value="" placeholder="SystemID">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Verzia zápisu</label>
                                <select name="version" class="form-control">
                                    <?php foreach($versions as $version):?>
                                        <option  value="<?=$version['id']?>"><?=$version['name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Program</label>
                                <select name="program_id" class="form-control">
                                    <?php if(isset($programs) && !empty($programs)):?>
                                        <?php foreach($programs as $program):?>
                                            <option value="<?=$program['id']?>"><?=$program['internal_name']?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label style="color:white;">asd</label>
                                <button type="button" class="btn btn-success w-100 createUser">Vytvoriť prvého užívateľa</button>
                            </div>
                        </div>

                        <input type="hidden" name="createUser" value="0">
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-12 createUserWrapper" style="display:none;">

                            <div class="row">
                                <div class="col-md-12 alert alert-info text-center" style="display:none;">
                                   <strong>Message</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" placeholder="Email" name="first_user[email]" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Meno</label>
                                        <input type="text" placeholder="Meno" name="first_user[first_name]" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Priezvisko</label>
                                        <input type="text" placeholder="Priezvisko" name="first_user[last_name]" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Heslo</label>
                                        <input type="text" placeholder="Heslo" name="first_user[password]" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label style="color:white;">Heslo znovu</label>
                                        <button type="button" class="btn btn-primary w-100 validate_user">Overiť</button>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h5>Priradiť rolu ku účtu</h5>
                                </div>
                                <?php foreach($roles as $role):?>
                                <div class="col-md-2">
                                    <label class="checkbox">
                                        <input type="checkbox" name="first_user[roles][]" <?=$role['name']!='developer' ? 'checked' : ''?> value="<?=$role['id']?>">
                                       <span></span>
                                        &nbsp; &nbsp;<?=$role['description']?>
                                    </label>
                                </div>
                                <?php endforeach;?>
                            </div>
                            <hr>
                        </div>
                        <button class="w-100 btn btn-primary">Uložiť</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $('.validate_user').on('click',function(e){
        e.preventDefault();
        e.stopPropagation();

        var _email = $('[name="first_user[email]"]').val(),
            _alert = $('.alert');
        if(_email !== ""){
            $.ajax({
                type:"post",
                url:"<?=base_url('dashboard/properties/validateUser')?>",
                data:{email:_email},
                dataType:"json",
                success:function(data){
                    if(data.status === '1'){
                        _alert.show().find('strong').html('Overené!');
                    }
                    else{
                        _alert.show().find('strong').html('Užívateľ existuje!');
                    }
                }
            })
        }

    });

    $('.createUser').on('click',function(){
        var _wrapper =  $('.createUserWrapper'),
            _this = $(this),
            _name = $('[name="createUser"]');
        if(_this.hasClass('btn-success')){
            _wrapper.show();
            _this.removeClass('btn-success').addClass('btn-danger');
            _this.html('Zrušiť vytváranie užívateľa');
            _name.val('1');
        }
        else{
            _wrapper.hide();
            _this.removeClass('btn-danger').addClass('btn-success');
            _this.html('Vytvoriť prvého užívateľa');
            _name.val('0');
        }

    });
</script>