<div class="container">
    <div class="col-md-12">
        <div class="card card-custom card-stretch">
            <div class="card-header">
                <div class="card-title">
                    <h3>Vytvorenie zariadenia</h3>
                </div>
            </div>
            <div class="card-body">
                <form action="<?=base_url('dashboard/properties/editProcess/'.$property['id'])?>" method="POST">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Názov zariadenia</label>
                                <input type="text" class="form-control" name="name" value="<?=$property['name']?>" placeholder="Názov zariadenia">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Server</label>
                                <input type="text" class="form-control" name="server" value="<?=$property['server']?>" placeholder="Server">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Port</label>
                                <input type="text" class="form-control" name="port" value="<?=$property['port']?>" placeholder="Port">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>CashDeskID</label>
                                <input type="text" class="form-control" name="cash_desk_id" value="<?=$property['cash_desk_id']?>" placeholder="CashDeskID">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>SystemID</label>
                                <input type="text" class="form-control" name="system_id" value="<?=$property['system_id']?>" placeholder="SystemID">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Verzia zápisu</label>
                                <select name="version" class="form-control">
                                    <?php foreach($versions as $version):?>
                                        <option <?=$property['version'] == $version['id'] ? 'selected' : ''?> value="<?=$version['id']?>"><?=$version['name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Program</label>
                                <select name="program_id" class="form-control">
                                    <?php if(isset($programs) && !empty($programs)):?>
                                        <?php foreach($programs as $program):?>
                                            <option <?=$program['id'] == $property['program_id'] ? 'selected' : ''?> value="<?=$program['id']?>"><?=$program['internal_name']?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        </div>
                        <legend></legend>
                        <div class="col-md-12">
                            <h5>Užívatelia pre zariadenie</h5>
                        </div>
                        <?php if(isset($users) && !empty($users)):?>
                            <?php foreach($users as $user):?>
                                <div class="col-md-12">
                                    <label class="checkbox">
                                        <input type="checkbox" <?=isset($property['users']) && !empty($property['users']) && in_array($user['id'],$property['users']) ? 'checked' : ''?> name="user[<?=$user['id']?>]" value="<?=$user['id']?>">
                                        <span></span>
                                        &nbsp;&nbsp;<?=$user['first_name'].' '.$user['last_name'].' <small>('.$user['email'].')</small> ' ?>
                                    </label>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <legend></legend>
                        <button class="w-100 btn btn-primary">Uložiť</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>