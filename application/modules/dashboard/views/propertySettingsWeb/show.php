<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css">

<style>
    .select2{
        width:100% !important;
    }
</style>
<div class="content-wrapper">
    <?php $this->load->view('partials/breadcrumb',array('noButton'=>'1'))?>
    <div class="content">
        <div class="card">
            <div class="card-header">
                <h5>Nastavenie zariadenia</h5>
            </div>
            <div class="card-body">
                <form action="<?=isset($psw['property_id']) && !empty($psw['property_id']) ? base_url('dashboard/propertySettingsWeb/editProcess/'.$psw['property_id']) : base_url('dashboard/propertySettingsWeb/createProcess')?>" method="post" enctype="multipart/form-data">
                    <ul class="nav nav-tabs nav-tabs-solid nav-justified rounded border-0">
                        <li class="nav-item"><a href="#solid-rounded-justified-tab1" class="nav-link rounded-left active" data-toggle="tab">Základné nastavenia</a></li>
                        <li class="nav-item"><a href="#solid-rounded-justified-tab2" class="nav-link" data-toggle="tab">Otváracie hodiny</a></li>
                        <li class="nav-item"><a href="#solid-rounded-justified-tab3" class="nav-link" data-toggle="tab">Notifikácie</a></li>
                        <li class="nav-item"><a href="#solid-rounded-justified-tab4" class="nav-link" data-toggle="tab">GDPR</a></li>
                        <li class="nav-item"><a href="#solid-rounded-justified-tab5" class="nav-link" data-toggle="tab">Extérny systém</a></li>
                    </ul>
                    <div class="tab-content">
                        <legend></legend>
                        <div class="tab-pane fade show active" id="solid-rounded-justified-tab1">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Názov</label>
                                        <input type="text" autocomplete="_off" class="form-control" name="name" placeholder="Názov" value="<?=isset($psw['name']) && !empty($psw['name']) ? $psw['name'] : ''?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" autocomplete="_off" class="form-control" name="email" placeholder="Email" value="<?=isset($psw['email']) && !empty($psw['email']) ? $psw['email'] : ''?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Telefónne číslo</label>
                                        <input type="text" autocomplete="_off" class="form-control" name="phone" placeholder="Telefónne číslo" value="<?=isset($psw['phone']) && !empty($psw['phone']) ? $psw['phone'] : ''?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Defaultný jazyk</label>
                                        <select name="default_lang" class="form-control">
                                            <?php foreach(applicationLangs() as $lang):?>
                                                <option <?=(isset($psw['default_lang']) && !empty($psw['default_lang']) && $psw['default_lang'] == $lang) ? 'selected' : ''?> value="<?=$lang?>"><?=lang('langshort.'.$lang)?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Mena</label>
                                        <select name="currency" class="form-control">
                                            <?php foreach($currencies as $currency):?>
                                                <option <?=(isset($psw['currency']) && !empty($psw['currency']) && $psw['currency'] == $currency['id']) ? 'selected' : ''?> value="<?=$currency['id']?>"><?=$currency['name']?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label>Webová stránka</label>
                                        <input class="form-control" autocomplete="_off" type="text" name="web" value="<?=$psw['web']?>" placeholder="Webová stránka">
                                    </div>
                                </div>
                                <legend></legend>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Štát</label>
                                        <input type="text" autocomplete="_off" class="form-control" name="country" placeholder="Štát" value="<?=isset($psw['country']) && !empty($psw['country']) ? $psw['country'] : ''?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Adresa</label>
                                        <input type="text" autocomplete="_off" class="form-control" name="address" placeholder="Adresa" value="<?=isset($psw['address']) && !empty($psw['address']) ? $psw['address'] : ''?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>PSČ</label>
                                        <input type="text" autocomplete="_off" class="form-control" name="zip" placeholder="PSČ" value="<?=isset($psw['zip']) && !empty($psw['zip']) ? $psw['zip'] : ''?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Mesto</label>
                                        <input type="text" autocomplete="_off" class="form-control" name="city" placeholder="Mesto" value="<?=isset($psw['city']) && !empty($psw['city']) ? $psw['city'] : ''?>">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Zamepisná dĺžka</label>
                                        <input type="text" autocomplete="_off" class="form-control" name="longitude" placeholder="Zamepisná dĺžka" value="<?=isset($psw['longitude']) && !empty($psw['longitude']) ? $psw['longitude'] : ''?>">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Zemepisná šírka</label>
                                        <input type="text" autocomplete="_off" class="form-control" name="latitude" placeholder="Zemepisná šírka" value="<?=isset($psw['latitude']) && !empty($psw['latitude']) ? $psw['latitude'] : ''?>">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="text-white">asd</label>
                                        <button type="button" class="btn btn-primary w-100 getGeoLocations">Získať súradnice</button>
                                    </div>
                                </div>
                                <legend></legend>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Facebook odkaz</label>
                                        <input type="text" autocomplete="_off" class="form-control" name="facebook" placeholder="Facebook odkaz" value="<?=isset($psw['facebook']) && !empty($psw['facebook']) ? $psw['facebook'] : ''?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Instagram odkaz</label>
                                        <input type="text" autocomplete="_off" class="form-control" name="instagram" placeholder="Instagram odkaz" value="<?=isset($psw['instagram']) && !empty($psw['instagram']) ? $psw['instagram'] : ''?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Twitter odkaz</label>
                                        <input type="text" autocomplete="_off" class="form-control" name="twitter" placeholder="Twitter odkaz" value="<?=isset($psw['twitter']) && !empty($psw['twitter']) ? $psw['twitter'] : ''?>">
                                    </div>
                                </div>
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 mb-2">
                                        <b>Povolené jazyky</b>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php foreach(applicationLangs() as $lang):?>
                                        <div class="col-md-2">
                                            <label>
                                                <input type="checkbox" name="language[<?=$lang?>]" <?=(isset($languages) && !empty($languages) && in_array($lang,$languages)) ? 'checked' : ''?> value="<?=$lang?>">
                                                <?=lang('langshort.'.$lang)?>
                                            </label>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Logo</label>
                                            <input type="file" name="image" class="form-control">
                                        </div>
                                        <?php if(isset($psw['logo']) && !empty($psw['logo'])):?>
                                            <button type="button" class="btn btn-danger removeImg w-100">Zmazať logo</button>
                                        <?php endif;?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php if(isset($psw['logo']) && !empty($psw['logo'])):?>
                                            <div class="previewLogo" style="width:100%;height:200px;background:url('<?=upload_url($this->session->userdata('active_property').'/logo/'.$psw['logo'])?>');background-repeat: no-repeat;background-position: center;background-size:contain;"></div>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="hasImg" value="<?=isset($psw['logo']) && !empty($psw['logo']) ? '1' : '0'?>">
                            <input type="hidden" name="removeImg" value="0">
                            <input type="hidden" name="oldImg" value="<?=isset($psw['logo']) && !empty($psw['logo']) ? $psw['logo'] : '0'?>">
                            <legend></legend>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Favicon</label>
                                            <input type="file" name="favicon" class="form-control">
                                        </div>
                                        <?php if(isset($psw['favicon']) && !empty($psw['favicon'])):?>
                                            <button type="button" class="btn btn-danger removeFavicon w-100">Zmazať favicon</button>
                                        <?php endif;?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php if(isset($psw['favicon']) && !empty($psw['favicon'])):?>
                                            <div class="previewFavicon" style="width:100%;height:200px;background:url('<?=upload_url($this->session->userdata('active_property').'/favicon/'.$psw['favicon'])?>');background-repeat: no-repeat;background-position: center;background-size:contain;"></div>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="hasFavicon" value="<?=isset($psw['favicon']) && !empty($psw['favicon']) ? '1' : '0'?>">
                            <input type="hidden" name="removeFavicon" value="0">
                            <input type="hidden" name="oldFavicon" value="<?=isset($psw['favicon']) && !empty($psw['favicon']) ? $psw['favicon'] : '0'?>">
                        </div>
                        <div class="tab-pane fade" id="solid-rounded-justified-tab2">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Nepovoliť rezerváciu pred zatvorením prevádzky</label>
                                    <select name="last_reservation" class="form-control">
                                        <option <?=isset($psw['last_reservation']) && !empty($psw['last_reservation']) && $psw['last_reservation'] == 0 ? 'selected'  : ''?> value="0"><?='0 minút'?></option>
                                        <?php for($i=1;$i<9;$i++):?>
                                            <option <?=isset($psw['last_reservation']) && !empty($psw['last_reservation']) && $psw['last_reservation'] == ($i*15) ? 'selected'  : ''?> value="<?=15*$i?>"><?=15*$i.' minút'?></option>
                                        <?php endfor;?>
                                    </select>
                                </div>
                            </div>
                            <legend></legend>
                            <div class="row m-2">
                                <div class="col-md-2">Pondelok :</div>
                                <div class="col-md-5">
                                    <select name="openingHours[monFrom]" class="form-control">
                                        <option value="-">Zatvorené</option>
                                        <?=generateOpeningHours(isset($openingHours['monFrom']) && !empty($openingHours['monFrom']) ? $openingHours['monFrom'] : null)?>
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <select name="openingHours[monTo]" class="form-control">
                                        <option value="-">Zatvorené</option>
                                        <?=generateOpeningHours(isset($openingHours['monTo']) && !empty($openingHours['monTo']) ? $openingHours['monTo'] : null)?>
                                    </select>
                                </div>
                            </div>
                            <div class="row m-2">
                                <div class="col-md-2">Utorok :</div>
                                <div class="col-md-5">
                                    <select name="openingHours[tueFrom]" class="form-control">
                                        <option value="-">Zatvorené</option>
                                        <?=generateOpeningHours(isset($openingHours['tueFrom']) && !empty($openingHours['tueFrom']) ? $openingHours['tueFrom'] : null)?>
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <select name="openingHours[tueTo]" class="form-control">
                                        <option value="-">Zatvorené</option>
                                        <?=generateOpeningHours(isset($openingHours['tueTo']) && !empty($openingHours['tueTo']) ? $openingHours['tueTo'] : null)?>
                                    </select>
                                </div>
                            </div>
                            <div class="row m-2">
                                <div class="col-md-2">Streda :</div>
                                <div class="col-md-5">
                                    <select name="openingHours[wedFrom]" class="form-control">
                                        <option value="-">Zatvorené</option>
                                        <?=generateOpeningHours(isset($openingHours['wedFrom']) && !empty($openingHours['wedFrom']) ? $openingHours['wedFrom'] : null)?>
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <select name="openingHours[wedTo]" class="form-control">
                                        <option value="-">Zatvorené</option>
                                        <?=generateOpeningHours(isset($openingHours['wedTo']) && !empty($openingHours['wedTo']) ? $openingHours['wedTo'] : null)?>
                                    </select>
                                </div>
                            </div>
                            <div class="row m-2">
                                <div class="col-md-2">Štvrtok :</div>
                                <div class="col-md-5">
                                    <select name="openingHours[thuFrom]" class="form-control">
                                        <option value="-">Zatvorené</option>
                                        <?=generateOpeningHours(isset($openingHours['thuFrom']) && !empty($openingHours['thuFrom']) ? $openingHours['thuFrom'] : null)?>
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <select name="openingHours[thuTo]" class="form-control">
                                        <option value="-">Zatvorené</option>
                                        <?=generateOpeningHours(isset($openingHours['thuTo']) && !empty($openingHours['thuTo']) ? $openingHours['thuTo'] : null)?>
                                    </select>
                                </div>
                            </div>
                            <div class="row m-2">
                                <div class="col-md-2">Piatok :</div>
                                <div class="col-md-5">
                                    <select name="openingHours[friFrom]" class="form-control">
                                        <option value="-">Zatvorené</option>
                                        <?=generateOpeningHours(isset($openingHours['friFrom']) && !empty($openingHours['friFrom']) ? $openingHours['friFrom'] : null)?>
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <select name="openingHours[friTo]" class="form-control">
                                        <option value="-">Zatvorené</option>
                                        <?=generateOpeningHours(isset($openingHours['friTo']) && !empty($openingHours['friTo']) ? $openingHours['friTo'] : null)?>
                                    </select>
                                </div>
                            </div>
                            <div class="row m-2">
                                <div class="col-md-2">Sobota :</div>
                                <div class="col-md-5">
                                    <select name="openingHours[satFrom]" class="form-control">
                                        <option value="-">Zatvorené</option>
                                        <?=generateOpeningHours(isset($openingHours['satFrom']) && !empty($openingHours['satFrom']) ? $openingHours['satFrom'] : null)?>
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <select name="openingHours[satTo]" class="form-control">
                                        <option value="-">Zatvorené</option>
                                        <?=generateOpeningHours(isset($openingHours['satTo']) && !empty($openingHours['satTo']) ? $openingHours['satTo'] : null)?>
                                    </select>
                                </div>
                            </div>
                            <div class="row m-2">
                                <div class="col-md-2">Nedeľa :</div>
                                <div class="col-md-5">
                                    <select name="openingHours[sunFrom]" class="form-control">
                                        <option value="-">Zatvorené</option>
                                        <?=generateOpeningHours(isset($openingHours['sunFrom']) && !empty($openingHours['sunFrom']) ? $openingHours['sunFrom'] : null)?>
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <select name="openingHours[sunTo]" class="form-control">
                                        <option value="-">Zatvorené</option>
                                        <?=generateOpeningHours(isset($openingHours['sunTo']) && !empty($openingHours['sunTo']) ? $openingHours['sunTo'] : null)?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="solid-rounded-justified-tab3">
                            <div class="form-group row">
                                <div class="col-md-3">Kód SMS Farma</div>
                                <div class="col-md-9">
                                    <input type="text" autocomplete="_off" name="sms[sms_farm]" placeholder="Kód SMS Farma" class="form-control" value="<?=isset($sms['sms_farm']) && !empty($sms['sms_farm']) ? $sms['sms_farm'] : ''?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">IntegrationID SMS farma</div>
                                <div class="col-md-9">
                                    <input type="text" autocomplete="_off" name="sms[integration_id]" placeholder="IntegrationID SMS farma" class="form-control" value="<?=isset($sms['integration_id']) && !empty($sms['integration_id']) ? $sms['integration_id'] : ''?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">SMS Odosielateľ</div>
                                <div class="col-md-9">
                                    <input type="text" autocomplete="_off" maxlength="11" name="sms[sender]" placeholder="SMS Odosielateľ" class="form-control" value="<?=isset($sms['sender']) && !empty($sms['sender']) ? $sms['sender'] : ''?>">
                                    <small>Maximálna dĺžka - 11 znakov, je možné použiť len čísla a písmena</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">Zostávajúci kredit</div>
                                <div class="col-md-9">
                                    <input type="text" autocomplete="_off" class="form-control" value="<?=isset($sms_credit) && !empty($sms_credit) ? $sms_credit.' €' : '0.0 €'?>" disabled>
                                </div>
                            </div>
                            <legend></legend>
                            <div class="form-group row">
                                <div class="col-md-3">Text SMS správy</div>
                                <div class="col-md-9">
                                    <input type="text" autocomplete="_off" name="sms[text]" placeholder="Text SMS správy" class="form-control" value="<?=isset($sms['text']) && !empty($sms['text']) ? $sms['text'] : ''?>">
                                    <small>
                                        <b>Premenné:</b><br/>
                                        <ul>
                                            <li>{first_name} = Meno zákazníka</li>
                                            <li>{last_name} = Priezvisko zákazníka</li>
                                            <li>{total_price} = Cena objednávky</li>
                                            <li>{reservation_number} = Cena objednávky</li>
                                        </ul>
                                    </small>
                                </div>
                            </div>
                            <legend></legend>
                            <div class="form-group row">
                                <div class="col-md-3">Notifikačný email - pre zariadenie</div>
                                <div class="col-md-9">
                                    <select name="notification_email[]" class="form-control form-control-tagging2 w-100" multiple="multiple">
                                        <?php if(isset($psw['notification_email']) && !empty($psw['notification_email'])):?>
                                            <?php foreach($psw['notification_email'] as $key => $email):?>
                                                <option selected value="<?=$email?>"><?=$email?></option>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="solid-rounded-justified-tab4">
                            <div class="form-group row">
                                <div class="col-md-3">Obchodné podmienky - typ</div>
                                <div class="col-md-9">
                                    <select name="terms_and_condition_type" class="form-control">
                                        <option value="">Žiadne</option>
                                        <option <?=isset($psw['terms_and_condition_type']) && !empty($psw['terms_and_condition_type']) && $psw['terms_and_condition_type'] == 'link' ? 'selected' : ''?> value="link">Link</option>
                                        <option <?=isset($psw['terms_and_condition_type']) && !empty($psw['terms_and_condition_type']) && $psw['terms_and_condition_type'] == 'file' ? 'selected' : ''?> value="file">Súbor</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row terms_and_condition_type terms_and_condition_type_file" style="<?=isset($psw['terms_and_condition_type']) && !empty($psw['terms_and_condition_type']) && $psw['terms_and_condition_type'] == 'file' ? '' : 'display:none'?>">
                                <div class="col-md-3">Obchodné podmienky - súbor</div>
                                <div class="col-md-9">
                                    <input type="file" name="terms_and_condition_file" class="form-control">
                                    <?php if(isset($psw['terms_and_condition_type']) && !empty($psw['terms_and_condition_type']) && $psw['terms_and_condition_type'] == 'file'):?>
                                        <?php if(isset($psw['terms_and_conditions_file']) && !empty($psw['terms_and_conditions_file'])):?>
                                            <a target="_blank" class="btn btn-success mt-2" href="<?=$psw['terms_and_conditions_file']?>">Zobraziť súbor</a>
                                            <input type="hidden" name="old_terms_and_condition_file" value="<?=$psw['terms_and_conditions']?>">
                                        <?php endif;?>
                                    <?php endif;?>
                                </div>
                            </div>
                            <div class="form-group row terms_and_condition_type terms_and_condition_type_link" style="<?=isset($psw['terms_and_condition_type']) && !empty($psw['terms_and_condition_type']) && $psw['terms_and_condition_type'] == 'link' ? '' : 'display:none'?>">
                                <div class="col-md-3">Obchodné podmienky - link</div>
                                <div class="col-md-9">
                                    <input type="text" autocomplete="_off" name="terms_and_condition_link" value="<?=isset($psw['terms_and_condition_type']) && !empty($psw['terms_and_condition_type']) && $psw['terms_and_condition_type'] == 'link' ? $psw['terms_and_conditions'] : ''?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">GDPR podmienky - typ</div>
                                <div class="col-md-9">
                                    <select name="gdpr_condition_type" class="form-control">
                                        <option value="">Žiadne</option>
                                        <option <?=isset($psw['gdpr_condition_type']) && !empty($psw['gdpr_condition_type']) && $psw['gdpr_condition_type'] == 'link' ? 'selected' : ''?> value="link">Link</option>
                                        <option <?=isset($psw['gdpr_condition_type']) && !empty($psw['gdpr_condition_type']) && $psw['gdpr_condition_type'] == 'file' ? 'selected' : ''?> value="file">Súbor</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row gdpr_condition_type gdpr_condition_type_file" style="<?=isset($psw['gdpr_condition_type']) && !empty($psw['gdpr_condition_type']) && $psw['gdpr_condition_type'] == 'file' ? '' : 'display:none'?>">
                                <div class="col-md-3">GDPR podmienky - súbor</div>
                                <div class="col-md-9">
                                    <input type="file" name="gdpr_condition_file" class="form-control">
                                    <?php if(isset($psw['gdpr_condition_type']) && !empty($psw['gdpr_condition_type']) && $psw['gdpr_condition_type'] == 'file'):?>
                                        <?php if(isset($psw['gdpr_conditions_file']) && !empty($psw['gdpr_conditions_file'])):?>
                                            <a target="_blank" class="btn btn-success mt-2" href="<?=$psw['gdpr_conditions_file']?>">Zobraziť súbor</a>
                                            <input type="hidden" name="old_gdpr_condition_file" value="<?=$psw['gdpr_conditions']?>">
                                        <?php endif;?>
                                    <?php endif;?>
                                </div>
                            </div>
                            <div class="form-group row gdpr_condition_type gdpr_condition_type_link" style="<?=isset($psw['gdpr_condition_type']) && !empty($psw['gdpr_condition_type']) && $psw['gdpr_condition_type'] == 'link' ? '' : 'display:none'?>">
                                <div class="col-md-3">GDPR podmienky - link</div>
                                <div class="col-md-9">
                                    <input type="text" autocomplete="_off" name="gdpr_condition_link" value="<?=isset($psw['gdpr_condition_type']) && !empty($psw['gdpr_condition_type']) && $psw['gdpr_condition_type'] == 'link' ? $psw['gdpr_conditions'] : ''?>" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="solid-rounded-justified-tab5">
                            <div class="form-group row">
                                <div class="col-md-3">Doručenie pri rezervácii na stôl</div>
                                <div class="col-md-9">
                                    <select name="default_delivery_id" class="form-control">
                                        <option value="">Žiadne</option>
                                        <?php if(isset($deliveries) && !empty($deliveries)):?>
                                            <?php foreach($deliveries as $delivery):?>
                                            <option <?=$psw['default_delivery_id'] == $delivery['id'] ? 'selected' : ''?> value="<?=$delivery['id']?>"><?=$delivery['internal_name']?></option>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">Zobraziť zľavy</div>
                                <div class="col-md-9">
                                    <select name="show_discount" class="form-control">
                                        <option <?=(isset($psw['show_discount']) && $psw['show_discount']=='0') ? 'selected': ''?> value="0">Nie</option>
                                        <option <?=(isset($psw['show_discount']) && $psw['show_discount']=='1') ? 'selected' : ''?> value="1">Áno</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <legend></legend>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-primary w-100">Uložiť</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $('.removeImg').on('click',function(){
        $('[name="removeImg"]').val('1');
        $('.previewLogo').remove();
        $(this).remove();
    });

    $('.removeFavicon').on('click',function(){
        $('[name="removeFavicon"]').val('1');
        $('.previewFavicon').remove();
        $(this).remove();
    });

    $(".form-control-tagging2").select2({
        tags: true,
        tokenSeparators: [',', ' ']
    });

    $('[name="terms_and_condition_type"]').on('change',function(e){
        e.preventDefault();
        e.stopPropagation();
        $('.terms_and_condition_type').hide();
        $('.terms_and_condition_type_'+$(this).val()).show();
    });

    $('[name="gdpr_condition_type"]').on('change',function(e){
        e.preventDefault();
        e.stopPropagation();
        $('.gdpr_condition_type').hide();
        $('.gdpr_condition_type_'+$(this).val()).show();
    });

    $('.getGeoLocations').on('click',function(e){
        e.preventDefault();
        e.stopPropagation();

        var _address = $('[name="address"]').val(),
            _city = $('[name="city"]').val();

        //console.log({address:_address,city:_city});return;
        $.ajax({
            type:'post',
            url:'<?=base_url('dashboard/propertySettingsWeb/getGeoLocation')?>',
            data:{address:_address,city:_city},
            dataType:'json',
            success:function(data){
                if(data.status === '1'){
                    $('[name="longitude"]').val(data.longitude);
                    $('[name="latitude"]').val(data.latitude);
                }
            }
        });
    });
</script>