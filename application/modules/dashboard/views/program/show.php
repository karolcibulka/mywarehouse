<?=getDataTable()?>

<div class="container">
    <div class="col-md-12">
        <div class="card card-custom card-stretch">
            <div class="card-header">
                <div class="card-title">
                    <h3>Programy</h3>
                </div>
                <div class="card-toolbar">
                    <a href="<?=base_url('dashboard/program/create')?>" class="btn btn-primary">Vytvoriť program</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Interný názov</th>
                                <th>Akcia</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(isset($programs) && !empty($programs)):?>
                            <?php foreach($programs as $program):?>
                                <tr>
                                    <td><?=$program['id']?></td>
                                    <td><?=$program['internal_name']?></td>
                                    <td>
                                        <?php if(has_permission('edit')):?>
                                            <?php if($program['active'] === '1'):?>
                                                <a href="<?=base_url('dashboard/program/active/'.$program['id'].'/0')?>"><span class="badge badge-success">Aktívne</span></a>
                                            <?php else:?>
                                                <a href="<?=base_url('dashboard/program/active/'.$program['id'].'/1')?>"><span class="badge badge-warning">Neaktívne</span></a>
                                            <?php endif;?>
                                            <a href="<?=base_url('dashboard/program/edit/'.$program['id'])?>"><span class="badge badge-primary">Upraviť</span></a>
                                        <?php endif;?>
                                        <?php if(has_permission('delete')):?>
                                            <a href="<?=base_url('dashboard/program/delete/'.$program['id'])?>"><span class="badge badge-danger">Zmazať</span></a>
                                        <?php endif;?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var data_table = $('#data-table').dataTable({
        //ajax:getUrl(false),
        processing: true,
        bSortCellsTop:true,
        lengthMenu: [ [ 10, 25, 50, 100, -1 ], [ '10', '25', '50','100', 'Všetky výsledky' ] ],
        pageLength : 25,
        order:[[0,'asc']],
        language:{
            processing:'Spracovavám požiadavku',
            emptyTable : 'Momentálne sa tu nenachádzajú žiadne dáta. Ak sa nespracováva požiadavka, skúste nové hľadanie',
            loading:'Načítavam'
        }
    });
</script>