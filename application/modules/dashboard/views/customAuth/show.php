<div class="content-wrapper">
    <?php $this->load->view('partials/breadcrumb',array('noButton'=>'1'))?>
    <div class="content">
        <div class="card">
            <div class="card-header">
                <?php if($this->session->has_userdata('status')):?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-<?=$this->session->userdata('status')?> text-center">
                                <?=$this->session->userdata('message')?>
                            </div>
                        </div>
                    </div>
                <?php endif;?>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h5>Vytvorenie užívateľa pre zariadenie</h5>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <?php if(has_permission('create')):?>
                    <form action="<?=base_url('dashboard/customAuth/createProccess')?>" method="post">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Meno</label>
                                    <input type="text" class="form-control" name="first_name" placeholder="Meno">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Priezvisko</label>
                                    <input type="text" class="form-control" name="last_name" placeholder="Priezvisko">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Heslo</label>
                                    <input type="password" class="form-control" name="password" placeholder="Heslo">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Potvrdenie hesla</label>
                                    <input type="password" class="form-control" name="password_confirm" placeholder="Potvrdenie hesla">
                                </div>
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 text-left">
                                        <strong>Rola</strong>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php if(isset($roles) && !empty($roles)):?>
                                        <?php foreach($roles as $role):?>
                                            <div class="col-md-2">
                                                <label>
                                                    <input type="checkbox" value="<?=$role['id']?>" name="role[]" checked>
                                                    <?=$role['description']?>
                                                </label>
                                            </div>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </div>
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 text-left">
                                        <strong>Zariadenie</strong>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php if(isset($properties) && !empty($properties)):?>
                                        <?php foreach($properties as $property):?>
                                            <div class="col-md-2">
                                                <label>
                                                    <input type="checkbox" value="<?=$property['id']?>" name="property[]" checked>
                                                    <?=$property['name']?>
                                                </label>
                                            </div>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </div>
                            </div>
                            <legend></legend>
                            <button class="btn btn-success" style="width:100%;">Uložiť nového užívateľa</button>
                        </div>
                    </form>
                <?php endif;?>
                <legend></legend>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h5>Užívatelia</h5>
                    </div>
                </div>
                <legend></legend>
                <ul class="nav nav-tabs nav-tabs-solid nav-justified border-0">
                    <?php if(isset($users) && !empty($users)):?>
                        <?php $i = 0;foreach($users as $property_id => $property):?>
                            <li class="nav-item"><a href="#solid-justified-tab<?=$property_id?>" class="nav-link <?=$i == 0 ? 'active show' : ''?>" data-toggle="tab"><?=$property['property_name']?></a></li>
                            <?php $i++;endforeach;?>
                    <?php endif;?>
                </ul>
                <div class="tab-content">
                    <?php if(isset($users) && !empty($users)):?>
                        <?php $i = 0;foreach($users as $property_id => $property):?>
                            <div class="tab-pane fade <?=$i == 0 ? 'active show' : '' ?>" id="solid-justified-tab<?=$property_id?>">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Email</th>
                                            <th>Meno</th>
                                            <th>Prizviesko</th>
                                            <th>Role</th>
                                            <th>Akcia</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(isset($property['users']) && !empty($property['users'])):?>
                                            <?php foreach($property['users'] as $customer_id => $user):?>
                                                <?php if(isset($user['id']) && !empty($user['id'])):?>
                                                    <?php $showEdit = true;?>
                                                    <?php if(isset($user['group_order']) && !empty($user['group_order'])):?>
                                                        <?php $j=0;$count = count($user['group_order'])-1;foreach($user['group_order'] as $group_id):?>
                                                            <?php
                                                            if($j == $count){
                                                                if($group_id <= $max_role){
                                                                    $showEdit = true;
                                                                }
                                                                else{
                                                                    $showEdit = false;
                                                                }
                                                            }
                                                            ?>
                                                            <?php $j++;endforeach;?>
                                                    <?php else:?>
                                                        <?php $showEdit = true;?>
                                                    <?php endif;?>
                                                    <?php if($showEdit):?>
                                                        <tr>
                                                            <td><?=$user['email']?></td>
                                                            <td><?=$user['first_name']?></td>
                                                            <td><?=$user['last_name']?></td>
                                                            <td><?=implode(', ',$user['group_names'])?></td>
                                                            <td>
                                                                <?php if($showEdit && has_permission('edit')):?>
                                                                    <a href="<?=base_url('dashboard/customAuth/edit/'.$user['id'])?>"><span class="badge badge-success">Upraviť</span></a>
                                                                <?php else:?>
                                                                    <small>Nemáš právo meniť užívateľa!</small>
                                                                <?php endif;?>
                                                            </td>
                                                        </tr>
                                                    <?php endif;?>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php $i++;endforeach;?>
                    <?php endif;?>
                </div>

            </div>
        </div>
    </div>
</div>
