<div class="content">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h5>Editácia užívateľa - <?=$user['first_name'].' '.$user['last_name']?></h5>
                </div>
            </div>
        </div>
        <div class="card-body">
            <?php if(has_permission('edit')):?>
                <form action="<?=base_url('dashboard/customAuth/editProccess')?>" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" value="<?=$user['email']?>" disabled placeholder="Email">
                                <input type="hidden" name="email" value="<?=$user['email']?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Meno</label>
                                <input type="text" class="form-control" value="<?=$user['first_name']?>" name="first_name" placeholder="Meno">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Priezvisko</label>
                                <input type="text" class="form-control" name="last_name" value="<?=$user['last_name']?>" placeholder="Priezvisko">
                            </div>
                        </div>
                        <legend></legend>
                        <div class="col-md-12">
                            <div class="row">
                            <div class="col-md-12 text-left">  
                                <strong>Rola</strong>                  
                            </div>
                            </div>
                            <div class="row">
                                <?php if(isset($roles) && !empty($roles)):?>
                                    <?php foreach($roles as $role):?>
                                        <div class="col-md-2">
                                            <label>                                      
                                                <input type="checkbox" value="<?=$role['id']?>" name="role[]" <?=isset($user['group_ids']) && !empty($user['group_ids']) && count($user['group_ids'])>0 && in_array($role['id'],$user['group_ids']) ? 'checked' : ''?>>
                                                <?=$role['description']?>
                                            </label>
                                        </div>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </div>
                        </div>
                        <legend></legend>
                        <div class="col-md-12">
                            <div class="row">
                            <div class="col-md-12 text-left">  
                                <strong>Zariadenie</strong>                  
                            </div>
                            </div>
                            <div class="row">
                                <?php if(isset($properties) && !empty($properties)):?>
                                    <?php foreach($properties as $property):?>
                                        <input type="hidden" name="allProperties[<?=$property['id']?>]" value="<?=$property['id']?>">
                                        <div class="col-md-2">
                                            <label>
                                                <input type="checkbox" value="<?=$property['id']?>" name="property[]" <?=isset($user['properties']) && !empty($user['properties']) && in_array($property['id'],$user['properties']) ? 'checked' : ''?>>
                                                <?=$property['name']?>
                                            </label>
                                        </div>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </div>
                        </div>
                        <legend></legend>
                        <button class="btn btn-success" style="width:100%;">Editovať užívateľa <b><?=$user['first_name'].' '.$user['last_name']?></b></button>
                    </div>
                </form>
            <?php endif;?>
        </div>
    </div>
</div>