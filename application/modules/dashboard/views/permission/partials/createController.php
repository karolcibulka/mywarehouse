
    <div class="sidebar sidebar-light sidebar-secondary sidebar-expand-md">
        <div id="controllerProccesWrapper">
        <div id="dynamicCreateController">
            <?php  if(has_permission('create')):?>

        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-secondary-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            <span class="font-weight-semibold"></span>
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>

        <div class="sidebar-content">

            <form action="<?=base_url('dashboard/permission/createNewController')?>" method="post">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold"><?=lang('menu.addController')?></span>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label><?=lang('menu.controllerName')?>:</label>
                            <input type="text" name="name" class="form-control" placeholder="navigation" required>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label><?=lang('menu.controllerDescription')?>:</label>
                                <input type="text" name="description" class="form-control" placeholder="Navigácia" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn bg-teal btn-block" data-popup="tooltip" title="" data-original-title="<?=lang('menu.submit')?>"><?=lang('menu.submit')?></button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
        <!-- /sidebar content -->
            <?php endif;?>
        </div>
        </div>
    </div>


