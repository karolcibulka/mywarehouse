<?php  if(has_permission('edit')):?>

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-secondary-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        <span class="font-weight-semibold"></span>
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>

    <div class="sidebar-content">

        <form id="editControllerAjax" method="post">
            <div class="card">
                <div class="card-header bg-transparent header-elements-inline">
                    <span class="text-uppercase font-size-sm font-weight-semibold"><?=lang('menu.editController')?></span>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <span style="font-weight: bold;cursor:pointer;" class="returnToAdd"><i class="icon-arrow-left7"></i><?=lang('menu.backToAdd') ?></span>
                    </div>
                    <div class="form-group">
                        <label><?=lang('menu.controllerName')?>:</label>
                        <input type="text" name="name" class="form-control" value="<?=$controller['name']?>" required>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <label><?=lang('menu.controllerDescription')?>:</label>
                            <input type="text" name="description" class="form-control" value="<?=$controller['description']?>" required>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="id" value="<?=$controller['id']?>">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn bg-teal btn-block" data-popup="tooltip" title="" data-original-title="<?=lang('menu.submit')?>"><?=lang('menu.submit')?></button>
                    </div>
                </div>
            </div>
        </form>

    </div>
    <!-- /sidebar content -->

<?php endif;?>


<script>
   $("#editControllerAjax").submit(function(e) {
       var form = $(this);

       var url = '<?=base_url('dashboard/permission/handleSendEditController')?>';
       $.ajax({
           type: "POST",
           url: url,
           data: form.serializeArray(), // serializes the form's elements. serialize() || serializeArray() ->do pola
           success : function(data){
               var obj = jQuery.parseJSON(data);
               console.log(obj);
               if(obj.status == 1){
                   $('#dynamicCreateController').html(obj.view);
                   $('#dynamicSetPermission').html(obj.view2);
               }
           }



       });
       e.preventDefault();
   });
</script>

<script>
    $(".returnToAdd").on('click',function(){
        var newData = '1';

        $.ajax({
            type: 'POST',
            url: "<?=base_url('dashboard/permission/handleReturnToAdd') ?>",
            data: {one:newData},
            success : function(data){
                $('#controllerProccesWrapper').html(data);
            }
        });
    });
</script>