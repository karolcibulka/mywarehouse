<?=getNestable()?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-custom card-stretch">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Právomoci</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div id="accordion-child1">
                        <?php if(isset($groups) && !empty($groups)):?>
                            <?php foreach($groups as $name => $g):?>
                                <div class="card card-custom card-stretch">
                                    <div class="card-header bg-dark">
                                        <h6 class="card-title">
                                            <a data-toggle="collapse" class="text-white collapsed" href="#<?=$name?>" aria-expanded="false"><?=$g['description']?></a>
                                        </h6>
                                    </div>

                                    <div id="<?=$name?>" class="collapse" data-parent="#accordion-child1" style="">
                                        <div class="card-body">
                                            <table class="table " style="margin-bottom:30px;">
                                                <thead>
                                                <tr>
                                                    <th>Controller</th>
                                                    <th><?=lang('menu.showing') ?></th>
                                                    <th><?=lang('menu.creating') ?></th>
                                                    <th><?=lang('menu.editing') ?></th>
                                                    <th><?=lang('menu.deleting') ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach($g['controllers'] as $controller => $c):?>
                                                    <tr>
                                                        <td><?=$c['description']?>
                                                            <i style="cursor:pointer;font-size:10px" data-cid="<?=$c['id']?>" class="icon-pencil3 editController"></i>
                                                        </td>
                                                        <td>
                                                            <label class="checkbox">
                                                                <input  class="aj" data-show="1" type="checkbox" data-gid="<?=$g['id']?>" data-cid="<?=$c['id'] ?>" data-method="show" name="show" <?=(isset($c['permissions']['show']) && ($c['permissions']['show']==TRUE)) ? 'checked' : '' ?> value="1" class="inline checkbox">
                                                                <span></span>
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label class="checkbox">
                                                                <input  class="aj"  type="checkbox" data-gid="<?=$g['id']?>" data-cid="<?=$c['id'] ?>" data-method="create" name="create" <?=(isset($c['permissions']['create']) && $c['permissions']['create']==TRUE) ? 'checked' : '' ?> value="1" class="inline checkbox">
                                                                <span></span>
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label class="checkbox">
                                                                <input class="aj" type="checkbox" data-gid="<?=$g['id']?>" data-cid="<?=$c['id'] ?>" data-method="edit" name="edit" <?=(isset($c['permissions']['edit']) && $c['permissions']['edit']==TRUE) ? 'checked' : '' ?> value="1" class="inline checkbox">
                                                                <span></span>
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label class="checkbox">
                                                                <input class="aj" type="checkbox" data-gid="<?=$g['id']?>" data-cid="<?=$c['id'] ?>" data-method="delete" name="delete" <?=(isset($c['permissions']['delete']) && $c['permissions']['delete']==TRUE) ? 'checked' : '' ?> value="1" class="inline checkbox">
                                                                <span></span>
                                                            </label>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card card-custom card-stretch mt-2">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Usporiadanie právomocí</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="kanban-container">
                        <div class="dd kanban-board">
                            <ol class="dd-list kanban-drag">
                                <?php if(isset($groups) && !empty($groups)):?>
                                    <?php foreach($groups as $name => $g):?>
                                        <li class="dd-item kanban-item" data-id="<?=$g['id']?>">
                                            <div class="dd-handle">
                                                <span class="font-weight-bold"><?=$g['description']?></span>
                                            </div>
                                        </li>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $('.dd').nestable({
        maxDepth:1,
    });

    $(".btn-reorder").on('click',function (e) {
        var _this = $(this);
        _this.attr('disabled',true)
        _this.html('<i class="spinner icon-spinner2"></i>')

        var _json = $('.dd').nestable('serialize');

        $.ajax({
            type: 'POST',
            url: '<?=base_url('dashboard/permission/handleChangeGroupOrder')?>',
            data: { json:_json },
            dataType:'json',
            success:function(data){
                if( data.status === '1' ){
                    window.location.reload();
                }
                else{
                    _this.attr('disabled',false);
                    _this.html('Uložiť zoradenie');
                }
            }
        })
    });
    $(".aj").on("change",function(e) {

        var data = $(this).data();
        var checkbox = $(this).is(":checked");

        var url = '<?=base_url('dashboard/permission/handleEditPermission')?>';


        $.ajax({
            type: "POST",
            url: url,
            data: {data,checkbox},
            dataType:'json', // serializes the form's elements. serialize() || serializeArray() ->do pola
            success : function(data){
                if(data.status == 1){
                    //$('#dynamicNav').html(obj.view);
                }
            }
        });

        e.preventDefault();
    });
</script>

<script>
    $(".editController").on("click",function(e) {

        $.ajax({
            type: "POST",
            url: '<?=base_url('dashboard/permission/handleEditController')?>',
            data: $(this).data(),
            dataType:'json',
            success : function(data){
                if(data.status == 1){
                    $('#dynamicCreateController').html(data.view);
                }
            }
        });

        e.preventDefault();
    });
</script>

