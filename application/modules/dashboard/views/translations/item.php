<div class="container">
    <div class="col-md-12">
        <div class="card card-custom">
            <div class="card-header card-header-tabs-line">
                <div class="card-title">
                    <h3><?=$file?></h3>
                </div>
                <div class="card-toolbar">
                    <a href="<?=base_url('dashboard/translations')?>" class="btn btn-primary">Späť</a>
                    <button data-href="<?=base_url('dashboard/translations/createProcess')?>" class="ml-3 btn btn-success btn-store">Uložiť</button>
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <input type="text" class="form-control input-add-key" placeholder="key">
                            </div>
                            <div class="col-sm-4">
                                <button class="btn btn-info w-100 btn-add-key">Pridať kľúč</button>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">action</th>
                                <th scope="col">key</th>
                                <?php foreach($languages as $language):?>
                                    <th scope="col"><?=lang('lang.'.$language)?></th>
                                <?php endforeach;?>
                            </tr>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">
                                    <input type="text" placeholder="Filter key" class="form-control form-control-sm">
                                </th>
                                <?php foreach($languages as $language):?>
                                    <th scope="col">
                                        <input type="text" placeholder="Filter <?=lang('lang.'.$language)?>" class="form-control form-control-sm">
                                    </th>
                                <?php endforeach;?>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($translations as $key => $translation):?>
                                    <tr>
                                        <td>
                                            <button class="btn btn-danger btn-delete-row" type="button">X</button>
                                        </td>
                                        <td class="align-middle"><?=$key?></td>
                                        <?php foreach($languages as $language):?>
                                            <td>
                                                <input type="text" data-language="<?=$language?>" data-file="<?=$file?>" data-key="<?=$key?>" value="<?=isset($translation[$language]) ? $translation[$language] : ''?>" class="translation form-control">
                                            </td>
                                        <?php endforeach;?>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $('.btn-store').on('click',function(){
        var _items = $('.translation'),
            _response = [];

        _items.each(function(){
            var _self = $(this),
                _data = _self.data();

            _response.push({language:_data.language,file:_data.file,key:_data.key,value:_self.val()});

        })
        $.ajax({
            url:'<?=base_url('dashboard/translations/storeTranslations')?>',
            type:'post',
            data:{data:JSON.stringify(_response)},
            dataType:'json',
            success:function(){
                window.location.href = '<?=base_url('dashboard/translations')?>'
            }
        });

    });

    $('.btn-add-key').on('click',function(){
        var _self = $(this),
            _input = $('.input-add-key');

        if(_input.val() !== ''){
            var _html = generateTr(_input.val());
            if(_html){
                $(document).find('tbody').prepend(_html);
                _input.val('');
            }
        }
    });



    var generateTr = function(value){
        if(value){
            var str = '';
            str += '<tr>';
            str += '<td>';
            str += '<button class="btn btn-danger btn-delete-row">X</button>';
            str += '</td>';
            str += '<td class="align-middle">';
            str += value;
            str += '</td>';
            <?php foreach($languages as $language):?>
                str += '<td>';
                str += '<input class="form-control translation" data-file="<?=$file?>" data-key="'+value+'" data-language="<?=$language?>" type="text" placeholder="<?=lang('lang.'.$language)?>">';
                str += '</td>';
            <?php endforeach;?>
            str += '</tr>';

            return str;
        }

        return null;
    }

    console.log(generateTr('asd'));

    $(document).on('click','.btn-delete-row',function(){
        $(this).closest('tr').remove();
    });
</script>