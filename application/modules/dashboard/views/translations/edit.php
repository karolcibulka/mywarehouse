<div class="container">
    <div class="col-md-12">
        <div class="card card-custom card-stretch">
            <div class="card-header">
                <div class="card-title">
                    <h3>Úprava jazyku</h3>
                </div>
            </div>
            <div class="card-body">

                <form action="<?=base_url('dashboard/translations/editProcess/'.$language['name'])?>" id="store-file" method="post">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Názov jazyka (kód napr. slovak)</label>
                                <input type="text" name="name" placeholder="Názov jazyka (kód napr. slovak)" value="<?=$language['name']?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Skratka jazyka (kód napr. sk)</label>
                                <input type="text" name="code" placeholder="Skratka jazyka (kód napr. sk)" value="<?=$language['code']?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-primary w-100 font-weight-bold">Uložiť</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>