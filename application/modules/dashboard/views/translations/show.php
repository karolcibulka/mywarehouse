<?=getDataTable() ?>

<div class="container">
    <div class="col-md-12">
        <div class="card card-custom">
            <div class="card-header card-header-tabs-line">
                <div class="card-title">
                    <h3>Súbory</h3>
                </div>
                <div class="card-toolbar">
                    <a class="btn btn-primary btn-create-new-file">Pridať súbor</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered data-table">
                        <thead>
                            <tr>
                                <td>Názov</td>
                                <td>Akcia</td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($files as $file):?>
                            <tr>
                                <td><?=$file?></td>
                                <td>
                                    <?php if(has_permission('edit')):?>
                                        <a href="<?=base_url('dashboard/translations/show/'.$file)?>" class="badge badge-primary font-weight-bold">Zobraziť / upraviť</a>
                                    <?php endif;?>
                                    <?php if(has_permission('delete')):?>
                                        <a href="<?=base_url('dashboard/translations/delete/'.$file)?>" class="badge badge-danger btn-delete-file font-weight-bold">Zmazať</a>
                                    <?php endif;?>
                                </td>

                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="card card-custom mt-3">
            <div class="card-header card-header-tabs-line">
                <div class="card-title">
                    <h3>Jazyky</h3>
                </div>
                <div class="card-toolbar">
                    <a class="btn btn-primary btn-create-new-language">Pridať jazyk</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered data-table">
                        <thead>
                        <tr>
                            <td>Názov</td>
                            <td>Akcia</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($folders as $folder):?>
                            <tr>
                                <td><?=$folder?></td>
                                <td>
                                    <?php if(has_permission('edit')):?>
                                        <a href="<?=base_url('dashboard/translations/editLanguage/'.$folder)?>" class="badge badge-primary font-weight-bold">Upraviť</a>
                                    <?php endif;?>

                                    <?php if(has_permission('delete')):?>
                                        <a href="<?=base_url('dashboard/translations/deleteLanguage/'.$folder)?>" class="btn-delete-language badge badge-danger font-weight-bold">Zmazať</a>
                                    <?php endif;?>

                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="createLanguageItem" tabindex="-1" role="dialog" aria-labelledby="createMenuItem" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Pridať nový jazyk</h5>
            </div>
            <div class="modal-body">
                <form action="<?=base_url('dashboard/translations/storeLanguage')?>" id="store-translate" method="post">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Názov jazyka (kód napr. slovak)</label>
                                <input type="text" name="name" placeholder="Názov jazyka (kód napr. slovak)" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Skratka jazyka (kód napr. sk)</label>
                                <input type="text" name="code" placeholder="Skratka jazyka (kód napr. sk)" class="form-control">
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Zatvoriť</button>
                <button type="button" class="btn btn-primary font-weight-bold btn-store-language">Uložiť</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="createFile" tabindex="-1" role="dialog" aria-labelledby="createMenuItem" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Pridať nový jazyk</h5>
            </div>
            <div class="modal-body">
                <form action="<?=base_url('dashboard/translations/storeFile')?>" id="store-file" method="post">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Názov súboru (napr. navigation_items)</label>
                                <input type="text" name="name" placeholder="Názov súboru (napr. navigation_items)" class="form-control">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Zatvoriť</button>
                <button type="button" class="btn btn-primary font-weight-bold btn-store-file">Uložiť</button>
            </div>
        </div>
    </div>
</div>

<script>

    var data_table = $('.data-table').dataTable({
        //ajax:getUrl(false),
        processing: true,
        bSortCellsTop:true,
        lengthMenu: [ [ 10, 25, 50, 100, -1 ], [ '10', '25', '50','100', 'Všetky výsledky' ] ],
        pageLength : 25,
        order:[[0,'asc']],
        language:{
            processing:'Spracovavám požiadavku',
            emptyTable : 'Momentálne sa tu nenachádzajú žiadne dáta. Ak sa nespracováva požiadavka, skúste nové hľadanie',
            loading:'Načítavam'
        }
    });

    $('.btn-delete-file').on('click',function(e){
        e.preventDefault();
        e.stopPropagation();

        var _self = $(this),
            _href = _self.attr('href');

        __question(_href,true);
    })

    $('.btn-delete-language').on('click',function(e){
        e.preventDefault();
        e.stopPropagation();

        var _self = $(this),
            _href = _self.attr('href');

        __question(_href,true);
    })

    $('.btn-create-new-file').on('click',function(){
        $('#createFile').modal();
    });

    $('.btn-store-file').on('click',function(){
        var _form = $('#store-file'),
            _self = $(this);

        _form.submit();
    });

    $('.btn-create-new-language').on('click',function(){
        $('#createLanguageItem').modal();
    });

    $('.btn-store-language').on('click',function(){
        var _form = $('#store-translate'),
            _self = $(this);

        _form.submit();

    });

</script>