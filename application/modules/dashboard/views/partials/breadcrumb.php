<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold"><?=lang('segment.'.$this->uri->segment(2))?></span> - <?=lang('segment.'.$this->uri->segment(2).'.'.$this->uri->segment(2)) ?></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            <?php if(isset($noButton) && !empty($noButton)):?>
            <?php elseif(isset($tables) && !empty($tables)):?>
            <?php else:?>
            <a id="submitButton"  <?= (isset($buttonAddict) && !empty($buttonAddict)) ? 'data-toggle="modal" data-target="#myModal"' :''?>  href="#" class="btn  bg-primary" style="background-color: #26a59a !important;"><?=(isset($buttonAddict) && !empty($buttonAddict)) ? $buttonAddict : lang('menu.submit')?></a>
            <?php endif;?>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?=base_url($this->uri->segment(1))?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> <?=ucfirst(lang('segment.'.$this->uri->segment(1)))?></a>
                <span class="breadcrumb-item active"><?=lang('segment.'.$this->uri->segment(2))?></span>
            </div>

        </div>
    </div>
</div>