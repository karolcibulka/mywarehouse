<?=getDataTable()?>
<?=getDateRangePicker()?>

<div class="container">
    <div class="col-md-12">
        <div class="card card-custom card-stretch">
            <div class="card-header">
                <div class="card-title">
                    <h3>Logy</h3>
                </div>
                <div class="card-toolbar">
                    <div class="row">
                        <div class="col">
                            <select name="month_changer" class="form-control">
                                <?php for($i=0;$i<=12;$i++):?>
                                    <?php
                                        $month = $i < 10 && $i > 0 ? '0'.$i : $i;
                                    ?>
                                    <option <?=$get['month'] == $month ? 'selected' : ''?>  value="<?=$month?>"><?=$month == 0 ? lang('month.allMonths') : lang('month.'.$month)?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="col">
                            <select name="year_changer" class="form-control">
                                <option value="0"><?=lang('year.allYears')?></option>
                                <?php for($i=2019;$i<=date('Y');$i++):?>
                                    <option <?=$get['year'] == $i ? 'selected' : ''?> value="<?=$i?>"><?=$i?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="month" value="<?=$get['month']?>">
            <input type="hidden" name="year" value="<?=$get['year']?>">

            <div class="card-body">
                <div class="table-responsive">
                    <table id="data-table" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Užívateľ</th>
                            <th>Controller</th>
                            <th>Metóda</th>
                            <th>Prvok</th>
                            <th>Zariadenie</th>
                            <th>IP</th>
                            <th>Vykonané</th>
                        </tr>
                        <tr>
                            <th class="pr-5">
                                <input type="number" name="id" value="<?=isset($get['id']) ? $get['id'] : ''?>" id="" placeholder="ID" class="dt-filter form-control form-control-sm">
                            </th>
                            <th class="pr-5">
                                <input type="text"  value="<?=isset($get['user']) ? $get['user'] : ''?>" name="user" placeholder="Užívateľ" class="dt-filter form-control form-control-sm">
                            </th>
                            <th class="pr-5">
                                <input placeholder="Controller" value="<?=isset($get['controller']) ? $get['controller'] : ''?>" name="controller" type="text" class="dt-filter form-control form-control-sm">
                            </th>
                            <th class="pr-5">
                                <input placeholder="Metóda" value="<?=isset($get['method']) ? $get['method'] : ''?>" name="method" type="text" class="dt-filter form-control form-control-sm">
                            </th>
                            <th class="pr-5">
                                <input placeholder="Prvok" value="<?=isset($get['item']) ? $get['item'] : ''?>" name="item" type="text" class="dt-filter form-control form-control-sm">
                            </th>
                            <th class="pr-5">
                                <input placeholder="Zariadenie" value="<?=isset($get['property']) ? $get['property'] : ''?>" name="property" type="text" class="dt-filter form-control form-control-sm">
                            </th>
                            <th class="pr-5">
                                <input placeholder="IP" value="<?=isset($get['ip']) ? $get['ip'] : ''?>" name="ip" type="text" class="dt-filter form-control form-control-sm">
                            </th>
                            <th class="pr-5">
                                <input placeholder="Vykonané" autocomplete="off" value="<?=isset($get['created_at']) ? $get['created_at'] : ''?>" name="created_at" type="text" class="date-range-picker dt-filter form-control form-control-sm">
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var getUrl = function(remakeUrl){
        var _path = '<?=base_url('dashboard/log/getData')?>',
            _currentPath = '<?=base_url('dashboard/log')?>',
            _month = $('[name="month"]').val(),
            _year = $('[name="year"]').val(),
            _add = '',
            _inputs = $('.dt-filter');

        _add = '?month='+_month+'&year='+_year;

        _inputs.each(function(){
            var _self = $(this);
            if(_self.val() !== ''){
                _add += '&'+_self.attr('name')+'='+_self.val();
            }
        })

        if(remakeUrl){
            window.history.pushState(_add, 'Title', _currentPath+_add);
        }

        else{
            window.history.pushState(_add, 'Title', _currentPath+_add);
            return _path+_add;
        }
    }

    var _dateRangePicker = $('.date-range-picker');
    _dateRangePicker.daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoUpdateInput:false,
        locale: {
            format: 'DD.MM.YYYY',
            cancelLabel: 'Vyčisti',
            applyLabel: 'Potvrdiť'
        }
    });

    _dateRangePicker.on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD.MM.YYYY'));
        changeStringAndReloadTable();
    });

    _dateRangePicker.on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        changeStringAndReloadTable();
    });

    $('.dt-filter').on('input',function(){
        changeStringAndReloadTable();
    });

    var changeStringAndReloadTable = function(){
        getUrl(true);
        setTimeout(function(){
            data_table.api().clear();
            data_table.api().draw();
            data_table.api().ajax.url(getUrl(false)).load();
        },1000);
    }

    var data_table = $('#data-table').dataTable({
        ajax:getUrl(false),
        processing: true,
        bSortCellsTop:true,
        lengthMenu: [ [ 10, 25, 50, 100, -1 ], [ '10', '25', '50','100', 'Všetky výsledky' ] ],
        pageLength : 25,
        order:[[0,'desc']],
        language:{
            processing:'Spracovavám požiadavku',
            emptyTable : 'Momentálne sa tu nenachádzajú žiadne dáta. Ak sa nespracováva požiadavka, skúste nové hľadanie',
            loading:'Načítavam'
        }
    });


    $('[name="month_changer"]').on('change',function(){
        var _self = $(this),
            _item = $('[name="month"]');

        _item.val(_self.val());

        changeStringAndReloadTable();
    });

    $('[name="year_changer"]').on('change',function(){
        var _self = $(this),
            _item = $('[name="year"]');

        _item.val(_self.val());

        changeStringAndReloadTable();

    });

</script>
