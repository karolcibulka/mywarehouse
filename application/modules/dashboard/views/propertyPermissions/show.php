<style>
    .activeRow{
        background-color: #4ac6ff36;
    }
</style>
<div class="content-wrapper">
    <?php
        $this->load->view('partials/breadcrumb')
    ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card" >
                    <div class="card-body">
                        <fieldset class="mb-3 tab-pane fade active show" id="properties">
                            <div class="table-responsive" id="propertiesTableWrap">
                                <table class="table" id="propertiesTable">
                                    <thead>
                                    <tr>
                                        <th><?=lang('properties.propertyName')?></th>
                                        <th><?=lang('properties.email')?></th>
                                        <th><?=lang('properties.actions')?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(isset($properties) && !empty($properties)):?>
                                        <?php foreach ($properties as $p):?>
                                            <tr data-property_id="<?=$p['id']?>"  style="cursor: pointer">
                                                <td><?=$p['name']?></td>
                                                <td><?=$p['email']?></td>
                                                <td><span class="btn btn-primary"><?=lang('properties.showUsers')?></span></td>
                                            </tr>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                    </tbody>
                                </table>
                            </div>

                        </fieldset>
                    </div>
                </div>
                <div class="card" id="usersWrap" style="display: none">

                </div>
            </div>
        </div>
    </div>
</div>
<div id="preregistrtionDetailModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">

    </div>
</div>
<script>
    var property_id = 0;
    $('#propertiesTable').on('click','tbody tr',function () {
        $('#propertiesTable').find('tbody tr').removeClass('activeRow');
        $(this).addClass('activeRow');
        property_id = $(this).data('property_id');

        var url = '<?=base_url('dashboard/propertyPermissions/getPropertyUsersPermissions')?>/'+property_id;

        $.ajax({
            type: "POST",
            url: url,
            success : function(data){
                var obj = $.parseJSON(data);
                if(obj.status == 1){
                    $('#usersWrap').html(obj.view).show();
                }
            }
        });
    });

    $(document).on('click','#user_property',function () {
        var id = $(this).data('id');
        var value = 1;
        if($(this).is(':checked') == false){
            value = 0;
        }
        var user_id = $(this).parents('td').find('input[name*=user_id]').val();
        var url = '<?=base_url('dashboard/propertyPermissions/handlePropertyPermission')?>/'+property_id+'/'+user_id+'/'+value;

        $.ajax({
            type: "POST",
            url: url,
            success : function(data){
                var obj = $.parseJSON(data);
                if(obj.status == 1){
                    alert('check db');
                }
            }
        });
    })
</script>


