<div class="card-body">
    <h5 class="card-title">
        <?=lang('properties.permissionsForUser')?> <b><?=$property['name']?></b>
    </h5>
    <fieldset class="mb-3 tab-pane fade active show" id="users">
        <div class="table-responsive" id="propertiesTableWrap">
            <table class="table" id="usersTable">
                <thead>
                <tr>
                    <th><?=lang('users.email')?></th>
                    <th><?=lang('users.first_name')?></th>
                    <th><?=lang('users.last_name')?></th>
                    <th><?=lang('users.allow')?></th>
                </tr>
                </thead>
                <tbody>
                <?php if(isset($users) && !empty($users)):?>
                    <?php foreach ($users as $user):?>
                        <tr>
                            <td><?=$user['email']?></td>
                            <td><?=$user['first_name']?></td>
                            <td><?=$user['last_name']?></td>
                            <td>
                                <input type="checkbox" name="user_property" id="user_property" value="<?=isset($userProperties[$user['id']]) ? 1 : 0?>" <?=isset($userProperties[$user['id']]) ? 'checked' : ''?>>
                                <input type="hidden" name="user_id" value="<?=$user['id']?>">
                            </td>
                        </tr>
                    <?php endforeach;?>
                <?php endif;?>
                </tbody>
            </table>
        </div>

    </fieldset>
</div>