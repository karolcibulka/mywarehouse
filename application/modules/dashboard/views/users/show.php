<div class="content-wrapper">
    <div class="content">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-4">
                        <h5>Administrácia</h5>
                    </div>
                    <div class="col-md-4">
                        <a href="<?=base_url('dashboard/auth/create_user')?>" class="btn btn-primary w-100">Vytvoriť používateľa</a>
                    </div>
                    <div class="col-md-4">
                        <a href="<?=base_url('dashboard/auth/create_group')?>" class="btn btn-primary w-100">Vytvoriť skupinu</a>
                    </div>
                </div>
                <legend></legend>
            </div>
            <div class="card-body">
                <form action="<?=base_url('dashboard/auth/index/'.$start)?>" method="get">
                    <div class="row">
                        <div class="col-md-3">
                            <input type="number" value="<?=isset($get['id']) && !empty($get['id']) ? $get['id'] : '' ?>" min="0" step="1" name="id" placeholder="Index" class="form-control">
                        </div>
                        <div class="col-md-3">
                            <input type="text" value="<?=isset($get['email']) && !empty($get['email']) ? $get['email'] : '' ?>" name="email" placeholder="Email" class="form-control">
                        </div>
                        <div class="col-md-3">
                            <select name="role" class="form-control">
                                <option value="">Žiadna rola</option>
                                <?php if(isset($roles) && !empty($roles)):?>
                                    <?php foreach($roles as $roleID => $role):?>
                                        <option <?=isset($get['role']) && !empty($get['role']) && $get['role'] == $roleID ? 'selected' : ''?> value="<?=$roleID?>"><?=$role?></option>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-primary w-100">Hľadať</button>
                        </div>
                    </div>
                </form>
                <legend></legend>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Email</th>
                            <th>Rola</th>
                            <th>Akcia</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($users) && !empty($users)):?>
                                <?php foreach($users as $user):?>
                                    <tr>
                                        <td><?=$user['id']?></td>
                                        <td><?=$user['email']?></td>
                                        <td>
                                            <?php $groups = '';?>
                                            <?php if(isset($user['roles']) && !empty($user['roles'])):?>
                                                <?php foreach($user['roles'] as $roleID => $role):?>
                                                    <?php $groups .= '<a href="'.base_url('dashboard/auth/edit_group/'.$roleID).'">'.$role.'</a> | ';?>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                            <?=rtrim($groups,' | ');?>
                                        </td>
                                        <td>
                                            <?php if(has_permission('edit')):?>
                                                <a href="<?=base_url('dashboard/auth/edit_user/'.$user['id'])?>"><span class="badge badge-primary">Upraviť</span></a>
                                            <?php endif;?>
                                            <?php if(has_permission('delete')):?>
                                                <a href="<?=base_url('dashboard/auth/delete_user/'.$user['id'])?>"><span class="badge badge-danger delete">Zmazať</span></a>
                                            <?php endif;?>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
                <?=$links?>
            </div>
        </div>
    </div>
</div>

<script>
    $('.delete').on('click',function(e){
        e.preventDefault();
        e.stopPropagation();

        swalAlertDelete($(this).closest('a').attr('href'));
    })
</script>