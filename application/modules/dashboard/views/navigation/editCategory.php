<div class="container">
    <div class="col-md-12">
        <div class="card card-custom card-stretch">
            <div class="card-header">
                <div class="card-title">
                    <h3>Úprava kategórie </h3>
                </div>
            </div>
            <div class="card-body">
                <form action="<?=base_url('dashboard/navigation/editNavigationCategoryProcess/'.$category['id'])?>" method="post">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Názov kategórie</label>
                                <input type="text" name="name"   value="<?=$category['name']?>" class="form-control" placeholder="Názov kategórie">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            <button class="btn btn-primary w-100">Upraviť</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>