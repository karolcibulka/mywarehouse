<div class="container">
    <div class="col-md-12">
        <div class="card card-custom card-stretch">
            <div class="card-header">
                <div class="card-title">
                    <h3>Úprava navigácie</h3>
                </div>
            </div>
            <div class="card-body">
                <form action="<?=base_url('dashboard/navigation/editNavigationProcess/'.$navigation['id'])?>" id="storeNavigation" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Názov navigácie (kód)</label>
                                <input type="text" name="name" id="" autocomplete="__unique" value="<?=$navigation['name']?>" placeholder="Názov navigácie (kód)" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Kategória</label>
                                <select name="navigation_category" id="" class="form-control">
                                    <?php if(isset($categories) && !empty($categories)):?>
                                        <?php foreach($categories as $category):?>
                                            <option <?=isset($navigation['navigation_category']) && $navigation['navigation_category'] === $category['id'] ? 'selected' : ''?> value="<?=$category['id']?>"><?=$category['name']?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Controller</label>
                                <select name="controller" id="" class="form-control">
                                    <option value="">Nie som naviazaný na controller</option>
                                    <?php if(isset($controllers) && !empty($controllers)):?>
                                        <?php foreach($controllers as $controller):?>
                                            <option <?=isset($navigation['controller']) && $navigation['controller'] === $controller['id'] ? 'selected' : ''?> value="<?=$controller['id']?>"><?=$controller['description']?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                            <hr>
                        </div>
                        <input type="hidden" name="type" value="3">
                        <input type="hidden" name="order" value="0">
                        <div class="col-md-12">
                            <div class="row">
                                <?php if(isset($icons) && !empty($icons)):?>
                                    <?php foreach($icons as $key => $icon):?>
                                        <div class="col-sm-1">
                                            <label class="radio">
                                                <input type="radio" <?=(isset($navigation['icon']) && $navigation['icon'] === $icon['id']) || $key === 0 ? 'checked' : ''?> name="icon" value="<?=$icon['id']?>">
                                                <span></span>
                                                &nbsp; <i class="<?=$icon['name']?> text-black"></i>
                                            </label>
                                        </div>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mt-10">
                        <hr>
                        <button class="btn btn-primary w-100">Upraviť</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>