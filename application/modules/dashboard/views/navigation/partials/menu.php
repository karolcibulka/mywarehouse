<style>
.elemButton {
    height: 30px;
    margin: 5px 0;
    padding: 5px 10px;
    color: #333;
    text-decoration: none;
    font-weight: bold;
    border: 1px solid #ccc;
    background: #fafafa;
    border-radius: 3px;
    box-sizing: border-box;
}

.dd-item > button{
    display:none;
}
</style>

<div class="content">

    <!-- Basic card -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><?=lang('menu.menu')?></h5>
        </div>
        <div class="card-body">
            <div class="dd">
                <ol class="dd-list">
                    <?php foreach ($navs as $nav):?>
                        <?php if($nav['deleted'] == 0):?>
                        <?php if (!isset($nav['children'])) :?>
                            <li class="dd-item col-md-12" style="display:inline-block" data-parent="<?=$nav['parent']?>" data-id="<?=$nav['id']?>">
                                <div class="dd-handle <?=(has_permission('edit') || has_permission('delete')) ? 'col-md-10' : 'col-md-12' ?>" style="display:inline-block">
                                    <i class="icon-<?=$nav['icon']?>"></i>
                                    <?=$nav['name']?>
                                </div>
                                <?php if(has_permission('edit') || has_permission('delete')):?>
                                <div class="elemButton" style="display:inline-block">
                                    <?php if(has_permission('edit')):?>
                                    <div style="display:inline-block">
                                        <form action="<?=base_url('dashboard/navigation/editCMSOption') ?>" method="post">
                                            <input type="hidden" name="id" value="<?=$nav['id']?>">
                                            <button type="submit" style="cursor:pointer;" class="icon-pencil6" data-popup="tooltip" title="" data-original-title="<?=lang('menu.edit')?>"></button>
                                        </form>
                                    </div>
                                    <?php endif; ?>
                                    <?php if(has_permission('edit') && has_permission('delete')):?>
                                        <div style="display:inline-block; margin:0 10px">
                                            |
                                        </div>
                                    <?php endif;?>
                                    <?php if(has_permission('delete')):?>
                                    <div style="display:inline-block">
                                        <form action="<?=base_url('dashboard/navigation/deleteCMSOption')?>" method="post">
                                            <input type="hidden" name="id" value="<?=$nav['id']?>">
                                            <button type="submit" style="cursor:pointer;" class="icon-eraser" data-popup="tooltip" title="" data-original-title="<?=lang('menu.delete')?>"></button>
                                        </form>
                                    </div>
                                    <?php endif;?>
                                </div>
                                <?php endif ;?>
                            </li>
                        <?php elseif(isset($nav['children']) && !empty($nav['children'])) :?>
                            <li class="dd-item col-md-12" style="display:inline-block;" data-parent="<?=$nav['parent']?>" data-id="<?=$nav['id']?>">
                                <div class="dd-handle <?=(has_permission('edit') || has_permission('delete')) ? 'col-md-10' : 'col-md-12' ?>" style="display:inline-block">
                                     <span style="padding-left: 10px;">
                                         <i class="icon-<?=$nav['icon']?>"></i>
                                         <?=$nav['name']?>
                                     </span>
                                </div>
                                <?php if(has_permission('edit') || has_permission('delete')) :?>
                                <div class="elemButton" style="display:inline-block">
                                    <?php if(has_permission('edit')):?>
                                    <div style="display:inline-block">
                                        <form action="<?=base_url('dashboard/navigation/editCMSOption') ?>" method="post">
                                            <input type="hidden" name="id" value="<?=$nav['id']?>">
                                            <button type="submit" style="cursor:pointer;" class="icon-pencil6" data-popup="tooltip" title="" data-original-title="<?=lang('menu.edit')?>"></button>
                                        </form>
                                    </div>
                                    <?php endif;?>
                                    <?php if(has_permission('edit') && has_permission('delete')):?>
                                    <div style="display:inline-block; margin:0 10px">
                                        |
                                    </div>
                                    <?php endif;?>
                                    <?php if(has_permission('delete')) :?>
                                    <div style="display:inline-block">
                                        <form action="<?=base_url('dashboard/navigation/deleteCMSOption')?>" method="post">
                                            <input type="hidden" name="id" value="<?=$nav['id']?>">
                                            <button type="submit" style="cursor:pointer;" class="icon-eraser" data-popup="tooltip" title="" data-original-title="<?=lang('menu.delete')?>"></button>
                                        </form>
                                    </div>
                                    <?php endif;?>
                                </div>
                                <?php endif;?>
                                <ol class="dd-list">
                                    <?php foreach ($nav['children'] as $child) :?>
                                        <?php if ($child['deleted'] == 0) :?>
                                        <li class="dd-item col-md-12" style="display:inline-block" data-parent="<?=$nav['id']?>" data-id="<?=$child['id']?>">
                                            <div class="dd-handle <?=(has_permission('edit') || has_permission('delete')) ?'col-md-10' : 'col-md-12' ?>" style="display:inline-block">
                                                <i class="icon-<?=$child['icon']?>"></i>
                                                <?=$child['name']?>
                                            </div>
                                            <?php if(has_permission('edit') || has_permission('delete')) :?>
                                            <div class="elemButton" style="display:inline-block">
                                                <?php if(has_permission('edit')):?>
                                                <div style="display:inline-block">
                                                    <form action="<?=base_url('dashboard/navigation/editCMSOption') ?>" method="post">
                                                        <input type="hidden" name="id" value="<?=$child['id']?>">
                                                        <button type="submit" style="cursor:pointer;" class="icon-pencil6" data-popup="tooltip" title="" data-original-title="<?=lang('menu.edit')?>"></button>
                                                    </form>
                                                </div>
                                                <?php endif;?>
                                                <?php if(has_permission('edit') && has_permission('delete')):?>
                                                <div style="display:inline-block; margin:0 10px">
                                                    |
                                                </div>
                                                <?php endif;?>
                                                <?php if(has_permission('delete')):?>
                                                <div style="display:inline-block">
                                                    <form action="<?=base_url('dashboard/navigation/deleteCMSOption')?>" method="post">
                                                        <input type="hidden" name="id" value="<?=$child['id']?>">
                                                        <button type="submit" style="cursor:pointer;" class="icon-eraser" data-popup="tooltip" title="" data-original-title="<?=lang('menu.delete')?>"></button>
                                                    </form>
                                                </div>
                                                <?php endif;?>
                                            </div>
                                            <?php endif;?>
                                        </li>
                                    <?php endif;?>
                                    <?php endforeach;?>
                                </ol>
                            </li>
                        <?php endif;?>
                        <?php endif;?>
                    <?php endforeach;?>
                </ol>
            </div>
        </div>


    </div>
    <!-- /basic card -->


</div>

<script>
    $('.dd').nestable({
        maxDepth:2,
    });

    $(".dd").on('change',function (e) {
        var json = $('.dd').nestable('serialize');
        var ids = $(json).map(function () {
            return this.id;
        }).get();


        $.ajax({
            type: 'POST',
            url: '<?=base_url('dashboard/navigation/handleChangeMenu')?>',
            data: { json:json }
        })
    });
</script>

