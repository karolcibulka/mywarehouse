<?=getNestable()?>

<div class=" container ">
    <!--begin::Dashboard-->
    <!--begin::Row-->
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-custom card-stretch">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Navigácia</h3>
                    </div>
                    <div class="card-toolbar">
                        <div class="dropdown dropdown-inline show" data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick actions">
                            <a href="#" class="btn btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i class="ki ki-bold-more-hor "></i>
                            </a>
                            <div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right" x-placement="top-end" style="position: absolute; transform: translate3d(-217px, -367px, 0px); top: 0px; left: 0px; will-change: transform;">
                                <!--begin::Navigation-->
                                <ul class="navi navi-hover">
                                    <li class="navi-header font-weight-bold py-4">
                                        <span class="font-size-lg">Zvoľ akciu:</span>
                                    </li>
                                    <li class="navi-separator mb-3 opacity-70"></li>
                                    <li class="navi-item">
                                        <a href="<?=base_url('dashboard/navigation/categories')?>" class="navi-link">
                                            <span class="navi-text">
                                                <span class="label label-xl label-inline w-100">
                                                    Kategórie
                                                </span>
                                            </span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="<?=base_url('dashboard/navigation/controllers')?>" class="navi-link">
                                            <span class="navi-text">
                                                <span class="label label-xl label-inline w-100">
                                                    Controllers
                                                </span>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                                <!--end::Navigation-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <?php if(has_permission('create')):?>
                        <div class="col-md-12 mb-3 text-right">
                            <button type="button" class="btn btn-success btn-create-nav-item">
                                Vytvoriť prvok navigácie
                            </button>
                        </div>
                    <?php endif;?>
                    <?php if(isset($navs) && !empty($navs)):?>
                        <?php foreach($navs as $category_id => $nav):?>
                            <div class="kanban-container">
                                <div class="dd kanban-board" id="dd-<?=$category_id?>">
                                    <header class="kanban-board-header">
                                        <div class="kanban-title-board mb-5">
                                            <?=$nav['name']?>
                                        </div>
                                    </header>
                                    <ol class="dd-list kanban-drag dd-nodrag">
                                        <?php foreach($nav['items'] as $navItem):?>
                                            <li class="dd-item kanban-item" data-id="<?=$navItem['id']?>">
                                                <div class="dd-handle">
                                                    <span class="font-weight-bold col-md-8">
                                                        <i class="<?=$navItem['icon']?> fa-sm"></i>  <?=$navItem['name']?>
                                                        <?php if(has_permission('delete')):?>
                                                            <a href="<?=base_url('dashboard/navigation/handleDeleteNavigation/'.$navItem['id'])?>" class=" delete badge badge-danger float-right ml-2">
                                                                Zmazať
                                                            </a>
                                                        <?php endif;?>
                                                        <?php if(has_permission('edit')):?>
                                                            <a href="<?=base_url('dashboard/navigation/editNavigation/'.$navItem['id'])?>" class="badge badge-primary float-right">
                                                                Upraviť
                                                            </a>
                                                        <?php endif;?>
                                                    </span>
                                                </div>
                                                <?php if(isset($navItem['children']) && !empty($navItem['children'])):?>
                                                <ol class="dd-list">
                                                    <?php foreach($navItem['children'] as $children):?>
                                                        <li class="dd-item kanban-item" data-id="<?=$children['id']?>">
                                                            <div class="dd-handle">
                                                                <span class="font-weight-bold">
                                                                     <i class="<?=$children['icon']?> fa-sm"></i> <?=$children['name']?>
                                                                    <?php if(has_permission('delete')):?>
                                                                        <a href="<?=base_url('dashboard/navigation/handleDeleteNavigation/'.$children['id'])?>" class=" delete badge badge-danger float-right ml-2">
                                                                            Zmazať
                                                                        </a>
                                                                    <?php endif;?>
                                                                    <?php if(has_permission('edit')):?>
                                                                        <a href="<?=base_url('dashboard/navigation/editNavigation/'.$navItem['id'])?>" class="badge badge-primary float-right">
                                                                            Upraviť
                                                                        </a>
                                                                    <?php endif;?>
                                                                </span>
                                                            </div>
                                                            <?php if(isset($children['children']) && !empty($children['children'])):?>
                                                                <ol class="dd-list">
                                                                    <?php foreach($children['children'] as $grand_children):?>
                                                                        <li class="dd-item kanban-item" data-id="<?=$grand_children['id']?>">
                                                                            <div class="dd-handle">
                                                                                <span class="font-weight-bold">
                                                                                     <i class="<?=$grand_children['icon']?> fa-sm"></i> <?=$grand_children['name']?>
                                                                                    <?php if(has_permission('delete')):?>
                                                                                        <a href="<?=base_url('dashboard/navigation/handleDeleteNavigation/'.$grand_children['id'])?>" class=" detebadge badge-danger float-right ml-2">
                                                                                            Zmazať
                                                                                        </a>
                                                                                    <?php endif;?>
                                                                                    <?php if(has_permission('edit')):?>
                                                                                        <a href="<?=base_url('dashboard/navigation/editNavigation/'.$navItem['id'])?>" class="badge badge-primary float-right">
                                                                                            Upraviť
                                                                                        </a>
                                                                                    <?php endif;?>
                                                                                </span>
                                                                            </div>
                                                                        </li>
                                                                    <?php endforeach;?>
                                                                </ol>
                                                            <?php endif;?>
                                                        </li>
                                                    <?php endforeach;?>
                                                </ol>
                                                <?php endif;?>
                                            </li>
                                        <?php endforeach;?>
                                    </ol>
                                </div>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="createMenuItem" tabindex="-1" role="dialog" aria-labelledby="createMenuItem" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Vytvoriť prvok navigácie</h5>
            </div>
            <div class="modal-body">
                <form action="<?=base_url('dashboard/navigation/storeNavigation')?>" id="storeNavigation" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Názov navigácie (kód)</label>
                                <input type="text" name="name" id="" autocomplete="__unique" placeholder="Názov navigácie (kód)" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Kategória</label>
                                <select name="navigation_category" id="" class="form-control">
                                    <?php if(isset($categories) && !empty($categories)):?>
                                        <?php foreach($categories as $category):?>
                                            <option value="<?=$category['id']?>"><?=$category['name']?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Controller</label>
                                <select name="controller" id="" class="form-control">
                                    <option value="">Nie som naviazaný na controller</option>
                                    <?php if(isset($controllers) && !empty($controllers)):?>
                                        <?php foreach($controllers as $controller):?>
                                            <option value="<?=$controller['id']?>"><?=$controller['description']?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                            <hr>
                        </div>
                        <input type="hidden" name="type" value="3">
                        <input type="hidden" name="order" value="0">
                        <div class="col-md-12">
                            <div class="row">
                                <?php if(isset($icons) && !empty($icons)):?>
                                    <?php foreach($icons as $key => $icon):?>
                                        <div class="col-sm-1">
                                            <label class="radio">
                                                <input type="radio" <?=$key == 0 ? 'checked' : ''?> name="icon" value="<?=$icon['id']?>">
                                                <span></span>
                                                &nbsp; <i class="<?=$icon['name']?> text-black"></i>
                                            </label>
                                        </div>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Zatvoriť</button>
                <button type="button" class="btn btn-primary font-weight-bold save-navigation-item">Uložiť</button>
            </div>
        </div>
    </div>
</div>

<script>

    $('.save-navigation-item').on('click',function(e){

        e.preventDefault();
        e.stopPropagation();

        $('#storeNavigation').submit();
    });

    $('.btn-create-nav-item').on('click',function(){
        $('#createMenuItem').modal();
    });

    $('.delete').on('click',function(e){

        e.preventDefault();
        e.stopPropagation();

        var _self = $(this);

        Swal.fire({
            title: 'Are you sure?',
            text: 'You want to delete it?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'Cancel',
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    type:'get',
                    url:_self.attr('href'),
                    dataType:'json',
                    success:function(data){
                        if(data.status === 1){
                            _self.closest('.dd-item').remove();
                            __successMessage();
                        }
                    }
                })
            }
        });
    });

    $('.dd').each(function(){
        var _self = $(this);

        _self.nestable({
            maxDepth:3,
            group:_self.prop('id')
        });

        _self.on('change',function(){
            var _data = _self.nestable('serialize');

            $.ajax({
                type:'post',
                url:'<?=base_url('dashboard/navigation/reorder')?>',
                data:{json:_data},
                dataType:'json',
                success:function(){

                }
            })
        });
    })
</script>

