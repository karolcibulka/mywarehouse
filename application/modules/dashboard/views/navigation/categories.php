<?=getNestable()?>
<div class=" container ">
    <!--begin::Dashboard-->
    <!--begin::Row-->
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-custom card-stretch">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Kategórie</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="<?=base_url('dashboard/navigation')?>" class="btn btn-primary"><i class="fas fa-arrow-left"></i>Späť na navigáciu</a>
                    </div>
                </div>
                <div class="card-body">
                    <?php if(isset($navs) && !empty($navs)):?>
                        <div class="kanban-container">
                            <div class="dd kanban-board" id="dd-0">
                                <header class="kanban-board-header">
                                    <div class="kanban-title-board mb-5">
                                        Kategórie
                                        <?php if(has_permission('create')):?>
                                            <a href="<?=base_url('dashboard')?>" class="badge badge-success float-right">
                                                Vytvoriť kategóriu
                                            </a>
                                        <?php endif;?>
                                    </div>
                                </header>
                                <ol class="dd-list kanban-drag dd-nodrag">
                                    <?php foreach($navs as $category_id => $navItem):?>
                                        <li class="dd-item kanban-item" data-id="<?=$navItem['id']?>">
                                            <div class="dd-handle">
                                                <span class="font-weight-bold">
                                                    <?=$navItem['name']?>
                                                    <?php if(has_permission('delete')):?>
                                                        <a href="<?=base_url('dashboard/navigation/deleteNavigationCategory/'.$navItem['id'])?>" class="badge delete badge-danger float-right ml-2">
                                                            Zmazať
                                                        </a>
                                                    <?php endif;?>
                                                    <?php if(has_permission('edit')):?>
                                                        <a href="<?=base_url('dashboard/navigation/editNavigationCategory/'.$navItem['id'])?>" class="badge badge-primary float-right">
                                                            Upraviť
                                                        </a>
                                                    <?php endif;?>
                                                </span>
                                            </div>
                                        </li>
                                    <?php endforeach;?>
                                </ol>
                            </div>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $('.delete').on('click',function(e){
        e.preventDefault();
        e.stopPropagation();

        __question($(this).attr('href'),true);
    });

    $('.dd').each(function(){
        var _self = $(this);

        _self.nestable({
            maxDepth:1,
            group:_self.prop('id')
        });

        _self.on('change',function(){
            var _data = _self.nestable('serialize');

            $.ajax({
                type:'post',
                url:'<?=base_url('dashboard/navigation/reorderCategories')?>',
                data:{json:_data},
                dataType:'json',
                success:function(){

                }
            })
        });
    })
</script>

