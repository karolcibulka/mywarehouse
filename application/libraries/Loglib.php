<?php


class Loglib
{
    public $ci;
    public $controller;
    public $data;
    public $user_id;
    public $property_id;
    protected $cache_store_length = 3600*24*31;

    public function __construct(){
        $this->ci = & get_instance();
        $this->ci->load->model('Log_model','log_model');

        $this->ci->load->driver('cache',array('driver'=>'file','backup'=>'file'));

    }

    public function storeLog($controller,$method,$item_id=null){
        return true;
    }

    public function storeLogToCache($controller=false,$method = false,$item = false){

        $this->checkFolder();

        $insertLogData = array(
            'property_id' => $this->ci->session->userdata('active_property'),
            'user_id' => $this->ci->session->userdata('user_id'),
            'controller' => !$controller ? 'dashboard' : $controller,
            'method' => !$method ? 'index' : $method,
            'item' => $item,
            'ip' => $this->getIP(),
            'created_at' => date('Y-m-d H:i:s')
        );


        if(!$logs = $this->ci->cache->file->get('logs/'.date('Ymd'))){
            $logs = array();
        }

        $logs[] = $insertLogData;

        $this->ci->cache->file->delete( 'logs/'.date('Ymd') );
        $this->ci->cache->file->save('logs/'.date('Ymd') , $logs , $this->cache_store_length);
    }

    public function saveLogsCacheDataToDatabase(){
        $logs = $this->ci->cache->file->get('logs/'.date('Ymd'));
        $this->ci->log_model->storeLogsBatch($logs);
        $this->ci->cache->file->delete('logs/'.date('Ymd'));

    }

    public function autoStoreYesterdaysLogs(){
        $yesterday = date('Ymd',strtotime('- 1 day'));

        if($logs = $this->ci->cache->file->get('logs/'.$yesterday)){
            $this->ci->log_model->storeLogsBatch($logs);
            $this->ci->cache->file->delete('logs/'.$yesterday);
        }
    }

    private function getIP(){
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    private function checkFolder(){
        if(!file_exists(APPPATH.'cache/logs')){
            mkdir(APPPATH.'cache/logs',0777,true);
        }
    }
}