<?php

Class Uploader {

    public $ci;

    public function __construct()
    {
        $this->ci = & get_instance();
    }

    public function resizeImage($upload_dir,$name,$extension,$sizes){

        $this->ci->load->library('resize');
        if(!empty($sizes)){
            foreach($sizes as $prefix => $size){
                $newName = $name.'-'.$prefix.'.'.$extension;
                $resizeObjThumb = new resize($upload_dir.$name.'.'.$extension);
                $resizeObjThumb->resizeImage($size, 0, 'landscape');
                $resizeObjThumb->saveImage($upload_dir . '' . $newName, 70);
            }
        }

    }
}
