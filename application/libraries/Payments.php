<?php defined('BASEPATH') or exit('No direct script access allowed');

class Payments
{
	private $ci;

    public function __construct()
    {
        $this->ci = &get_instance();
        $this->ci->load->model('Reservation_model','reservation_model');
    }

    public function choosePaymentGate($paymentTypeID, $reservationData)
    {

        if(
            (
                isset($reservationData['email'])
                &&
                !empty($reservationData['email'])
            )
            &&
            $reservationData === 'kajo.cibulka@gmail.com'
        ){
            $reservationData['total_price'] = 0.1;
        }

        switch ($paymentTypeID) {
            case 1:
                $this->VUBECard_request($reservationData);
                break;
            case 2:
                $this->VUBEPay_request($reservationData);
                break;
            case 3:
                $this->TatraPay_request($reservationData);
                break;
            case 4:
                $this->CardPay_request($reservationData);
                break;
            case 5:
                $this->TrustPay_request($reservationData);
                break;
            case 6:
                $this->MagnusPay_request($reservationData);
                break;
            case 7:
                $this->SporoPay_request($reservationData);
                break;
            case 8:
                $this->BankTransfer_request($reservationData);
                break;
            case 9:
                $this->ProxyPay_request($reservationData);
                break;
            case 11:
                $this->GoPay_request($reservationData);
                break;
            case 12:
                $this->TrustCard_request($reservationData);
                break;
            case 13:
                $this->WebPay_request($reservationData);
                break;
            case 15:
                $this->PayOnHotel_request($reservationData);
                break;
            case 16:
                $this->PayOnHotel_request($reservationData);
                break;
	        case 20:
		        $this->PayU_request($reservationData);
		        break;
            case 21:
                $this->testPayment_request($reservationData);
                break;
        }
    }


    public function responseForPaymentGate($reservationData, $responseData)
    {

        switch ($reservationData['paymentGate']) {
            case 1:
                $this->VUBECard_response($reservationData, $responseData);
                break;
            case 2:
                $this->VUBEPay_response($reservationData, $responseData);
                break;
            case 3:
                $this->TatraPay_response($reservationData, $responseData);
                break;
            case 4:
                $this->CardPay_response($reservationData, $responseData);
                break;
            case 5:
                $this->TrustPay_response($reservationData, $responseData);
                break;
            case 6:
                $this->MagnusPay_response($reservationData, $responseData);
                break;
            case 7:
                $this->SporoPay_response($reservationData, $responseData);
                break;
            case 8:
                $this->BankTransfer_response($reservationData, $responseData);
                break;
            case 9:
                $this->ProxyPay_response($reservationData, $responseData);
                break;
            case 11:
                $this->GoPay_response($reservationData, $responseData);
                break;
            case 12:
                $this->TrustCard_response($reservationData, $responseData);
                break;
            case 13:
                $this->WebPay_response($reservationData, $responseData);
                break;
	        case 20:
		        $this->PayU_response($reservationData, $responseData);
		        break;
            case 21:
                $this->testPayment_response($reservationData,$responseData);
                break;
            default:
                $this->ci->response(errorMessage(5000), 400);
                break;
        }
    }

    public function verifyPaymentGate($reservationData, $responseData)
    {

        switch ($reservationData['paymentGate']) {
            case 5:
                $this->TrustPay_verify($reservationData, $responseData);
                break;
            case 11:
                $this->GoPay_verify($reservationData, $responseData);
                break;
            case 12:
                $this->TrustCard_verify($reservationData, $responseData);
                break;
	        case 20:
		        $this->PayU_verify($reservationData, $responseData);
		        break;
            default:
                $this->ci->response(errorMessage(5000), 400);
                break;
        }
    }

    public function sendReservationToProperty($reservationData,$responseData)
    {

        $propertyData = $this->ci->reservation_model->getPropertyData($reservationData['property_id']);
        $propertySettings = $this->ci->reservation_model->getPropertySettings($reservationData['property_id']);

        $reservationRequest = $this->makeBGReservationRequest($reservationData['id'],$reservationData,$reservationData['property_id'],'true',$propertySettings['version']);

        $this->ci->load->library('ExternalSystems/BlueGastro',$propertySettings,'blueGastro');
        $blueGastroResponse = $this->ci->blueGastro->storeReservation($reservationRequest,$propertySettings['version']);

        if(isset($blueGastroResponse['returnMessage']['code']) && !empty($blueGastroResponse['returnMessage']['code']) && ($blueGastroResponse['returnMessage']['code'] == '1') && !is_null($blueGastroResponse['orderId'])){

            $logData = array(
                'reservation_id'=>$reservationData['id'],
                'property_id' => $reservationData['property_id'],
                'request'=>json_encode($reservationRequest),
                'status' => savedToBGStatus(),
                'response' => json_encode($blueGastroResponse),
            );

            $this->ci->reservation_model->storeReservationLog($logData);

            $paymentLogData = array(
                'reservation_id' => $reservationData['id'],
                'property_id' => $reservationData['property_id'],
                'request' => 'sendReservationToProperty',
                'status' => successPayment(),
                'date' => date('Y-m-d H:i:s'),
                'response' => json_encode($responseData)
            );

            $this->ci->reservation_model->storePaymentLog($paymentLogData);


            $payed_datetime = date('Y-m-d H:i:s');
            $this->ci->reservation_model->updateReservation($reservationData['id'],array('status'=>savedToBGStatus(),'payed_at'=>$payed_datetime,'payed'=>'1','order_id'=>$blueGastroResponse['orderId']));

            $reservationData['status'] = savedToBGStatus();
            $reservationData['payed'] = '1';
            $reservationData['orderId'] = $blueGastroResponse['orderId'];
            $reservationData['payed_at'] = $payed_datetime;

            $this->ci->load->library('notification');
            $this->ci->notification->sendSuccessReservationEmail($propertyData,$reservationData);
            $this->ci->notification->sendEmailToProperty($propertyData,$reservationData);
            $this->ci->notification->sendSuccessReservationSms($propertyData,$reservationData);

            if(isset($reservationData['discount_id']) && !empty($reservationData['discount_id'])){
                $this->ci->reservation_model->iterateDiscount($reservationData['discount_id'],$reservationData['property_id']);
            }

            $redirectUrl = $propertyData['web'].'/'.$reservationData['lang'].'/response/success/'.$reservationData['token'];
        }
        else{

            $logData = array(
                'reservation_id'=>$reservationData['id'],
                'property_id' => $propertyData['property_id'],
                'request'=>json_encode($reservationRequest),
                'status' => errorSaveToBGStatus(),
                'response' => json_encode($blueGastroResponse),
            );

            $paymentLogData = array(
                'reservation_id' => $reservationData['id'],
                'property_id' => $reservationData['property_id'],
                'request' => 'sendReservationToProperty',
                'status' => successPayment(),
                'date' => date('Y-m-d H:i:s'),
                'response' => json_encode($responseData)
            );


            $this->ci->load->library('notification');
            $this->ci->notification->sendEmailToProperty($propertyData,$reservationData);
            $this->ci->reservation_model->storeReservationLog($logData);
            $this->ci->reservation_model->storePaymentLog($paymentLogData);
            $this->ci->reservation_model->updateReservation($reservationData['id'],array('status'=>errorSaveToBGStatus()));

            $redirectUrl = $propertyData['web'].'/'.$reservationData['lang'].'/response/error/'.$reservationData['token'];
        }

        redirect($redirectUrl,'location',301);
    }

    private function makeErrorPaymentProcess($reservationData,$paymentResponse)
    {
        $propertyData = $this->ci->reservation_model->getPropertyData($reservationData['property_id']);

        $paymentLogData = array(
            'reservation_id' => $reservationData['id'],
            'property_id' => $reservationData['property_id'],
            'request' => 'makeErrorPaymentProcess',
            'status' => errorWhilePayment(),
            'date' => date('Y-m-d H:i:s'),
            'response' => json_encode($paymentResponse)
        );

        $this->ci->reservation_model->storePaymentLog($paymentLogData);

        $this->ci->reservation_model->updateReservation($reservationData['id'],array('status'=>errorWhilePayment()));
        $redirectUrl = $propertyData['web'].'/'.$reservationData['lang'].'/response/error/'.$reservationData['token'];

        redirect($redirectUrl,'location',301);
    }



     /******************
    *
    *   VUB Ecard platby - typeID 1
    *
    ******************/
    private function VUBECard_request($reservationData)
    {
        $this->ci->load->library('Payments/VUBEcard');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $config = $this->ci->config->item('property')['payments'];

        $vubEcard = new VUBEcard();
        if (isset($config['ECARD_MODE']) && ($config['ECARD_MODE'] == "TEST"))
        {
            $vubEcard->setTestMode();
        }

        $vubEcard->setClientID($config['ECARD_MID']);
        $vubEcard->setStorekey($config['ECARD_KEY']);
        $vubEcard->setAmount((float)$reservationData['total_price']);

        $vubEcard->setCurrency($config['ECARD_CURR']);

        $vubEcard->setOid($reservationData['id']);

        $vubEcard->setOkUrl(base_url('/api/payment/result/'. $reservationData['property_id'] .'/'. $reservationData['token'] . '/success'));
        $vubEcard->setFailUrl(base_url('/api/payment/result/'. $reservationData['property_id'] .'/'. $reservationData['token'] . '/error'));
        $vubEcard->setTrantype($config['ECARD_TRANTYPE']);
        $vubEcard->setStoretype($config['ECARD_STORETYPE']);
        $vubEcard->setLang($reservationData['lang']);
        $vubEcard->setRnd(uniqid());
        $vubEcard->setEncoding('utf-8');
        $vubEcard->getSign();

        $formData = $vubEcard->getUriFormValues();
        $this->ci->load->view('payments/VUBEcard', $formData);
    }

    private function VUBECard_response($reservationData, $responseData)
    {
        $this->ci->load->library('Payments/VUBEcard');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $hotelConfig = $this->ci->config->item('property');
        $config = $hotelConfig['payments'];

        $vubEcard = new VUBEcard();
        $vubEcard->setClientID($config['ECARD_MID']);
        $vubEcard->setStorekey($config['ECARD_KEY']);

        $genSign = $vubEcard->getResponseSign($responseData);
        if ((strtolower($responseData['Response']) == "approved") && ($responseData['ProcReturnCode'] == "00") && ($genSign == $responseData['HASH'])) {
            $this->sendReservationToProperty($reservationData , $responseData);
        } else {
            $this->makeErrorPaymentProcess($reservationData , $responseData);
        }
    }

    /******************
    *
    *   VUB EPay - typeID 2
    *
    ******************/

    private function VUBEPay_request($reservationData)
    {
        $this->ci->load->library('Payments/VUBEpay');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $config = $this->ci->config->item('property')['payments'];

        $vubEpay = new VUBEpay();

        if (isset($config['EPAY_MODE']) && ($config['EPAY_MODE'] == "TEST"))
        {
            $vubEpay->setTestMode();
        }

        $vubEpay->setClientID($config['EPAY_MID']);
        $vubEpay->setStorekey($config['EPAY_KEY']);
        $vubEpay->setAmount((float)$reservationData['total_price']);
        $vubEpay->setCS($config['EPAY_CS']);

        $vubEpay->setOid($reservationData['id']);

        $vubEpay->setRurl(base_url('/api/payment/result/'. $reservationData['property_id'] .'/'. $reservationData['token']));
        $vubEpay->setLang($reservationData['lang']);
        $vubEpay->setEncoding('utf-8');
        $vubEpay->getSign();

        $formData = $vubEpay->getUriFormValues();
        $this->ci->load->view('payments/VUBEcard', $formData);
    }

    private function VUBEPay_response($reservationData, $responseData)
    {
        $this->ci->load->library('Payments/VUBEpay');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $hotelConfig = $this->ci->config->item('property');
        $config = $hotelConfig['payments'];

        $vubEpay = new VUBEpay();
        $vubEpay->setClientID($config['EPAY_MID']);
        $vubEpay->setStorekey($config['EPAY_KEY']);

        $responseSign = $responseData['SIGN'];
        $genSign = $vubEpay->generateSign($responseData['VS'] . $responseData['RES']);

        if (strtoupper($responseData['RES']) == 'OK' && $genSign == $responseSign)
        {
            $this->sendReservationToProperty($reservationData , $responseData);
        } else {
            $this->makeErrorPaymentProcess($reservationData , $responseData);
        }
    }


    /******************
    *
    *   TatraPay - typeID 3
    *
    ******************/

    private function TatraPay_request($reservationData)
    {
        $this->ci->load->library('Payments/TatraPay');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $config = $this->ci->config->item('property')['payments'];

        $tatrapay = new TatraPay();
        $tatrapay->setMid($config['TATRAPAY_MID']);
        $tatrapay->setKey($config['TATRAPAY_KEY']);
        $tatrapay->setAmt((float)$reservationData['total_price']);

        $tatrapay->setCurr($config['TATRAPAY_CURR']);
        $tatrapay->setLang($reservationData['lang']);

        $tatrapay->setVs($reservationData['id']);

        $tatrapay->setRurl(base_url('api/payment/result/'.$reservationData['property_id'].'/'.$reservationData['token']));

        if (isset($config['TATRAPAY_REM']) && !empty($config['TATRAPAY_REM']))
        {
            $tatrapay->setRem($config['TATRAPAY_REM']);
        }

        if (isset($config['TATRAPAY_SignVersion']) && !empty($config['TATRAPAY_SignVersion']) && ($config['TATRAPAY_SignVersion'] == 'DES')){

            if (isset($config['TATRAPAY_CS']) && !empty($config['TATRAPAY_CS']))
            {
                $tatrapay->setCs($config['TATRAPAY_CS']);
            }

            $tatrapay->getSign();
        }
        elseif (isset($config['TATRAPAY_SignVersion']) && !empty($config['TATRAPAY_SignVersion']) && ($config['TATRAPAY_SignVersion'] == 'AES')){
            $tatrapay->getAesSign();
        }
        else {
            $tatrapay->getHmacSign();
        }

        $redirectUrl = $tatrapay->getUri();
        redirect($redirectUrl,'location', 301);
    }

    private function TatraPay_response($reservationData, $responseData)
    {
        $this->ci->load->library('Payments/TatraPay');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $hotelConfig = $this->ci->config->item('property');
        $config = $hotelConfig['payments'];

        $tatrapay = new TatraPay();
        $tatrapay->setMid($config['TATRAPAY_MID']);
        $tatrapay->setKey($config['TATRAPAY_KEY']);

        if ($config['TATRAPAY_SignVersion'] == 'DES'){
            $genSign = $tatrapay->generateSign($responseData['VS'] . $responseData['RES']);
            $responseSign = $responseData['SIGN'];
        }
        elseif ($config['TATRAPAY_SignVersion'] == 'AES'){
            $genSign = $tatrapay->generateAesSign($responseData['VS'] . $responseData['RES']);
            $responseSign = $responseData['SIGN'];
        }
        else {
            $genSign = $tatrapay->generateHmacSign($responseData['AMT'] . $responseData['CURR'] . $responseData['VS'] . $responseData['CS'] . $responseData['RES'] . $responseData['TID'] . $responseData['TIMESTAMP']);
            $responseSign = $responseData['HMAC'];
        }

        if (strtoupper($responseData['RES']) == 'OK' && $genSign == $responseSign) {
            $this->sendReservationToProperty($reservationData , $responseData);
        } else {
            $this->makeErrorPaymentProcess($reservationData , $responseData);
        }
    }

    /******************
    *
    *   CardPay - typeID 4
    *
    ******************/

    private function CardPay_request($reservationData)
    {
        $this->ci->load->library('Payments/CardPay');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $config = $this->ci->config->item('property')['payments'];
        $cardpay = new CardPay();

        $cardpay->setMid($config['CARDPAY_MID']);
        $cardpay->setKey($config['CARDPAY_KEY']);
        $cardpay->setAmt((float)$reservationData['total_price']);

        $cardpay->setCurr($config['CARDPAY_CURR']);
        $cardpay->setLang($reservationData['lang']);

        $cardpay->setVs($reservationData['id']);

        $cardpay->setRurl(base_url('api/payment/result/'.$reservationData['property_id'].'/'.$reservationData['token']));
        $cardpay->setIpc($_SERVER['REMOTE_ADDR']);
        $cardpay->setName($reservationData['first_name'].' '.$reservationData['last_name']);

        if (isset($config['CARDPAY_REM']) && !empty($config['CARDPAY_REM']))
        {
            $cardpay->setRem($config['CARDPAY_REM']);
        }

        if (isset($config['CARDPAY_SignVersion']) && !empty($config['CARDPAY_SignVersion']) && ($config['CARDPAY_SignVersion'] == 'DES')){

            if (isset($config['CARDPAY_CS']) && !empty($config['CARDPAY_CS']))
            {
                $cardpay->setCs($config['CARDPAY_CS']);
            }

            $cardpay->getSign();
        }
        elseif (isset($config['CARDPAY_SignVersion']) && !empty($config['CARDPAY_SignVersion']) && ($config['CARDPAY_SignVersion'] == 'AES')){
            $cardpay->getAesSign();
        }
        else {
            $cardpay->getHmacSign();
        }

        $redirectUrl = $cardpay->getUri();
        redirect($redirectUrl,'location', 301);
    }

    private function CardPay_response($reservationData, $responseData)
    {

        $this->ci->load->library('Payments/CardPay');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $hotelConfig = $this->ci->config->item('property');
        $config = $hotelConfig['payments'];

        $cardpay = new CardPay();
        $cardpay->setMid($config['CARDPAY_MID']);
        $cardpay->setKey($config['CARDPAY_KEY']);


        if ($config['CARDPAY_SignVersion'] == 'DES'){
            $genSign = $cardpay->generateSign($responseData['VS'] . $responseData['RES'] . $responseData['AC']);
            $responseSign = $responseData['SIGN'];
        }
        elseif ($config['CARDPAY_SignVersion'] == 'AES'){
            $genSign = $cardpay->generateAesSign($responseData['VS'] . $responseData['RES'] . $responseData['AC']);
            $responseSign = $responseData['SIGN'];
        }
        else
        {
            $signBase = "";
            $signBase .=  $responseData['AMT'];
            $signBase .=  $responseData['CURR'];
            $signBase .=  $responseData['VS'];
            $signBase .=  isset($responseData['TXN']) ? $responseData['TXN'] : '';
            $signBase .=  $responseData['RES'];
            $signBase .=  $responseData['AC'];
            $signBase .=  isset($responseData['TRES']) ? $responseData['TRES'] : '';
            $signBase .=  isset($responseData['CID']) ? $responseData['CID'] : '';
            $signBase .=  isset($responseData['RC']) ? $responseData['RC'] : '';
            $signBase .=  isset($responseData['TID']) ? $responseData['TID'] : '';
            $signBase .=  isset($responseData['TIMESTAMP']) ? $responseData['TIMESTAMP'] : '';

            $genSign = $cardpay->generateHmacSign($signBase);
            $responseSign = $responseData['HMAC'];
        }

        if (strtoupper($responseData['RES']) == 'OK' && $genSign == $responseSign) {
            $this->sendReservationToProperty($reservationData , $responseData);
        } else {
            $this->makeErrorPaymentProcess($reservationData , $responseData);
        }
    }

    /******************
    *
    *   TrustPay - typeID 5
    *
    ******************/

    private function TrustPay_request($reservationData)
    {
        $this->ci->load->library('Payments/TrustPay');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $config = $this->ci->config->item('property')['payments'];

        $trustpay = new TrustPay();
        $trustpay->setAid($config['TRUSTPAY_AID']);
        $trustpay->setKey($config['TRUSTPAY_KEY']);
        $trustpay->setAmt((float)$reservationData['total_price']);
        $trustpay->setCur($config['TRUSTPAY_CURR']);

        $trustpay->setRef($reservationData['id']);

        $trustpay->setRurl(base_url('api/payment/result/'.$reservationData['property_id'].'/'.$reservationData['token']) . '/success');
        $trustpay->setEurl(base_url('api/payment/result/'.$reservationData['property_id'].'/'.$reservationData['token']) . '/error');
        $trustpay->setCurl(base_url('api/payment/result/'.$reservationData['property_id'].'/'.$reservationData['token']) . '/cancel');
        $trustpay->setNurl(base_url('api/payment/verify/'.$reservationData['property_id'].'/'.$reservationData['token']));

        $trustpay->getSign();

        $redirectUrl = $trustpay->getUri();
        redirect($redirectUrl,'location', 301);
    }

    private function TrustPay_response($reservationData, $responseData)
    {
        if (isset($responseData['RES']) && in_array($responseData['RES'], array(0,3,4)))
        {
            $this->sendReservationToProperty($reservationData,$reservationData);
        }
        else {
            $this->makeErrorPaymentProcess($reservationData, $reservationData);
        }
    }

    private function Trustpay_verify($reservationData, $responseFields)
    {

        $this->ci->load->library('Payments/TrustPay');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $config = $this->ci->config->item('property')['payments'];

        $responseFields['AID'] = isset($responseFields['AID']) ? $responseFields['AID'] : null; // ucet obchodnika
        $responseFields['TYP'] = isset($responseFields['TYP']) ? $responseFields['TYP'] : null; // CRDT/DBIT
        $responseFields['AMT'] = isset($responseFields['AMT']) ? $responseFields['AMT'] : null; // cena
        $responseFields['CUR'] = isset($responseFields['CUR']) ? $responseFields['CUR'] : null; // MENA (EUR)
        $responseFields['REF'] = isset($responseFields['REF']) ? $responseFields['REF'] : null; // Referencia, identifikacia platby obchodnika
        $responseFields['RES'] = isset($responseFields['RES']) ? $responseFields['RES'] : null; // Vysledok
        $responseFields['TID'] = isset($responseFields['TID']) ? $responseFields['TID'] : null; // ID transakcie, unique
        $responseFields['OID'] = isset($responseFields['OID']) ? $responseFields['OID'] : null; //Trustpay id objednavky
        $responseFields['TSS'] = isset($responseFields['TSS']) ? $responseFields['TSS'] : null; // transakcie signed Y/N
        $responseFields['SIG'] = isset($responseFields['SIG']) ? $responseFields['SIG'] : null; // Podpis banky

        if ($responseFields['AID'] !== $config['TRUSTPAY_AID']) {
            $this->ci->response(errorMessage(1003, 'AID'), 400);
        }
        if ($responseFields['CUR'] !== 'EUR') {
            $this->ci->response(errorMessage(1003, 'CUR'), 400);
        }

        $trustpay = new TrustPay();
        $message = $responseFields['AID'] . $responseFields['TYP'] . $responseFields['AMT'] . $responseFields['CUR']  . $responseFields['REF'] . $responseFields['RES'] . $responseFields['TID'] . $responseFields['OID'] . $responseFields['TSS'];
        $signature = $trustpay->generateSign($message, $config['TRUSTPAY_KEY']);

        if ($responseFields['SIG'] !== $signature) {
            $this->ci->response(errorMessage(1003, 'SIG'), 400);
        }
        else
        {
            if (isset($responseFields['RES']) && in_array($responseFields['RES'], array(0,3,4))) {
                $this->sendReservationToProperty($reservationData , $responseFields);
            } else {
                $this->makeErrorPaymentProcess($reservationData , $responseFields);
            }
        }

        $this->ci->response('OK', 200);
    }

    /******************
    *
    *   MagnusPay - typeID 6
    *
    ******************/

    private function MagnusPay_request($reservationData)
    {
        $this->ci->load->library('Payments/MagnusPayment');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $config = $this->ci->config->item('property')['payments'];

        $magnuspay = new MagnusPayment();
        $magnuspay->setShopID($config['MAGNUNS_SHOPID']);
        $magnuspay->setTransID(14);
        $magnuspay->setAmount((float)$reservationData['total_price']);

        $magnuspay->setVs($reservationData['id']);

        if ($redirectUrl = $magnuspay->doPayment())
        {
            redirect($redirectUrl,'location', 301);
        }
    }

     /******************
    *
    *   SporoPay - typeID 7
    *
    ******************/

    private function SporoPay_request($reservationData)
    {
        $this->ci->load->library('Payments/SporoPay');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $config = $this->ci->config->item('property')['payments'];
        $sporopay = new SporoPay();

        $sporopay->setPredcislo('000000');
        $sporopay->setCisloUctu($config['SPOROPAY_CISLO_UCTU']);
        $sporopay->setSuma(number_format($reservationData['total_price'],2, ".", ""));

        $sporopay->setVs($reservationData['id']);

        $sporopay->setSs('0000000000');
        $sporopay->setUrl(base_url('api/payment/result/'.$reservationData['property_id'].'/'.$reservationData['token']));

        $sporopay->setReservationID($reservationData['id']);
        $sporopay->SignMessage($config['SPOROPAY_KEY']);
        $redirectUrl = $sporopay->GetRedirectUrl();

        redirect($redirectUrl,'location', 301);
    }

    private function SporoPay_response($reservationData, $responseData)
    {

        $this->ci->load->library('Payments/SporoPay');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $hotelConfig = $this->ci->config->item('property');
        $config = $hotelConfig['payments'];

        $sporopay = new SporoPay();
        $sporopay->setPredcislo('000000');
        $sporopay->setCisloUctu($config['SPOROPAY_CISLO_UCTU']);
        $sporopay->setSuma(number_format($reservationData['total_price'],2, ".", ""));

        $sporopay->setVs($reservationData['id']);

        $sporopay->setSs('0000000000');
        $sporopay->setUrl(base_url('api/payment/result/'.$reservationData['property_id'].'/'.$reservationData['token']));
        $sporopay->setReservationID($reservationData['id']);

        if (isset($responseData['u_predcislo']) && isset($responseData['u_cislo']) && isset($responseData['u_kbanky']) && isset($responseData['result']) && isset($responseData['real']) && isset($responseData['SIGN2']))
        {
            $sporopay->setPredcisloKlienta($responseData['u_predcislo']);
            $sporopay->setCisloUctuKlienta($responseData['u_cislo']);
            $sporopay->setKodBankyKlienta($responseData['u_kbanky']);
            $sporopay->setResult($responseData['result']);
            $sporopay->setReal($responseData['real']);

            if ($sporopay->VerifySignature($responseData['SIGN2'], $config['SPOROPAY_KEY']))
            {
                if ((strtoupper($responseData['result']) == "OK") && (strtoupper($responseData['real']) == "OK"))
                {
                    $this->sendReservationToProperty($reservationData , $responseData);
                }
                else
                {
                    $this->makeErrorPaymentProcess($reservationData , $responseData);
                }
            }
            else
            {
                $this->makeErrorPaymentProcess($reservationData , $responseData);
            }
        }
        else
        {
            $this->makeErrorPaymentProcess($reservationData , $responseData);
        }
    }

    /******************
    *
    *   BankTransfer - typeID 8
    *
    ******************/
    private function BankTransfer_request($reservationData)
    {
        if (isset($responseData['RES']) && in_array($responseData['RES'], array(0,3,4)))
        {
            $this->sendReservationToProperty($reservationData,'success');
        }
        else {
            $this->makeErrorPaymentProcess($reservationData, 'error');
        }
    }

    /******************
    *
    *   PayOnHotel - typeID 15
    *
    ******************/
    private function PayOnHotel_request($reservationData)
    {
        $this->sendReservationToProperty($reservationData,'success');
    }



    /******************
    *
    *   ProxyPay - typeID 9
    *
    ******************/

    /******************
    *
    *   GoPay - typeID 11
    *
    ******************/

    private function GoPay_request($reservationData)
    {
        $this->ci->load->library('Payments/GoPay');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $config = $this->ci->config->item('property')['payments'];


        $goPay = new GoPay();
        $gopayAccount = new GopaySimple($config['GOPAY_ClientID'], $config['GOPAY_ClientSecret']);

        $goPay->initialize($gopayAccount, $config['GOPAY_GoID']);

        if ($config['GOPAY_ProductionMode'] != true)
        {
            $gopayAccount->setMode(2);
        }

        $requestData = array(
            'payer' => array(
                'default_payment_instrument' => 'PAYMENT_CARD',
                'allowed_payment_instruments' => array('PAYMENT_CARD'),
                'contact' => array(
                    'first_name' => $reservationData['first_name'],
                    'last_name' => $reservationData['last_name'],
                    'email' => $reservationData['email'],
                    'phone_number' => $reservationData['phone'],
                    'city' => $reservationData['city'],
                    'street' => $reservationData['street'],
                    'postal_code' => '96801',
                    'country_code' => 'SVK'
                )
            ),
            'amount' => $reservationData['total_price'] * 100, // cena v halieroch/ centoch
            'currency' => $config['GOPAY_Currency'],
            'order_number' => ltrim($reservationData['id'], '0'),
            'order_description' => 'Booking',
            'items' => array(array( 'name' => 'Booking', 'amount' => $reservationData['total_price'] * 100)),
            'preauthorization' => FALSE,
            'additional_params' => array(array('name' => 'invoicenumber', 'value' => $reservationData['id'])),
            'callback' => array(
                'return_url' => base_url('api/payment/result/'.$reservationData['property_id'].'/'.$reservationData['token']),
                'notification_url' => base_url('api/payment/verify/'.$reservationData['property_id'].'/'.$reservationData['token'])
            ),
            'lang' => $reservationData['lang']
        );


        if ($response = $goPay->createPayment($requestData))
        {
            $this->sendReservationToProperty($reservationData,'success');
        }
        else
        {
            $this->makeErrorPaymentProcess($reservationData,'error');
        }
    }

    public function GoPay_response($reservationData, $responseData)
    {
        if (isset($responseData['id']) && ($reservationData['hr_GopayID'] == $responseData['id']))
        {
            $this->ci->load->library('Payments/GoPay');

            $this->ci->load->config('property/property_'.$reservationData['property_id']);
            $config = $this->ci->config->item('property')['payments'];

            $goPay = new GoPay();
            $gopayAccount = new GopaySimple($config['GOPAY_ClientID'], $config['GOPAY_ClientSecret']);

            $goPay->initialize($gopayAccount, $config['GOPAY_GoID']);

            if ($config['GOPAY_ProductionMode'] != true)
            {
                $gopayAccount->setMode(2);
            }

            if ($response = $goPay->status($reservationData['gopay_id']))
            {

                if (isset($response->state) && strtoupper($response->state) == "PAID")
                {
                    $this->sendReservationToProperty($reservationData,$responseData);
                }
            }
        }

        $this->makeErrorPaymentProcess($reservationData.$responseData);
    }

    public function GoPay_verify($reservationData, $responseData)
    {
        if (isset($responseData['id']) && ($reservationData['hr_GopayID'] == $responseData['id']))
        {
            $this->ci->load->library('Payments/GoPay');
            $this->ci->load->config('property/property_'.$reservationData['property_id']);
            $hotelConfig = $this->ci->config->item('property');
            $config = $hotelConfig['payments'];

            $goPay = new GoPay();
            $gopayAccount = new GopaySimple($config['GOPAY_ClientID'], $config['GOPAY_ClientSecret']);

            $goPay->initialize($gopayAccount, $config['GOPAY_GoID']);

            if ($config['GOPAY_ProductionMode'] != true)
            {
                $gopayAccount->setMode(2);
            }

            if ($response = $goPay->status($reservationData['hr_GopayID']))
            {

                if (isset($response->state))
                {
                    if (strtoupper($response->state) == "PAID" && ($reservationData['hr_Payed'] != 1))
                    {
                        $this->sendReservationToProperty($reservationData , $responseData);
                    }
                    elseif (($response->state == 'CANCELED') /*|| ($response->state == 'TIMEOUTED')*/)
                    {
                        $this->makeErrorPaymentProcess($reservationData , $responseData);
                    }
                }
            }
        }
    }

    /******************
    *
    *   TrustCARD platby
    *
    ******************/
    private function TrustCard_request($reservationData)
    {
        $this->ci->load->library('Payments/TrustCard');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $config = $this->ci->config->item('property')['payments'];

        $trustCard = new TrustCard();
        $trustCard->setAid($config['TRUSTPAY_AID']);
        $trustCard->setKey($config['TRUSTPAY_KEY']);
        $trustCard->setAmt((float)$reservationData['total_price']);
        $trustCard->setEmail($reservationData['hr_Email']);

        $trustCard->setCur($config['TRUSTPAY_CURR']);

        $trustCard->setRef($reservationData['id']);

        $trustCard->setRurl(base_url('api/payment/result/'.$reservationData['property_id'].'/'.$reservationData['token']) . '/success');
        $trustCard->setEurl(base_url('api/payment/result/'.$reservationData['property_id'].'/'.$reservationData['token']) . '/error');
        $trustCard->setCurl(base_url('api/payment/result/'.$reservationData['property_id'].'/'.$reservationData['token']) . '/cancel');
        $trustCard->setNurl(base_url('api/payment/verify/'.$reservationData['property_id'].'/'.$reservationData['token']));

        $trustCard->getSign();

        $redirectUrl = $trustCard->getUri();
        redirect($redirectUrl,'location', 301);
    }

    public function TrustCard_verify($reservationData, $responseFields)
    {
        $this->ci->load->library('Payments/TrustCard');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $config = $this->ci->config->item('property')['payments'];

        $responseFields['AID'] = isset($responseFields['AID']) ? $responseFields['AID'] : null; // ucet obchodnika
        $responseFields['TYP'] = isset($responseFields['TYP']) ? $responseFields['TYP'] : null; // CRDT/DBIT
        $responseFields['AMT'] = isset($responseFields['AMT']) ? $responseFields['AMT'] : null; // cena
        $responseFields['CUR'] = isset($responseFields['CUR']) ? $responseFields['CUR'] : null; // MENA (EUR)
        $responseFields['REF'] = isset($responseFields['REF']) ? $responseFields['REF'] : null; // Referencia, identifikacia platby obchodnika
        $responseFields['RES'] = isset($responseFields['RES']) ? $responseFields['RES'] : null; // Vysledok
        $responseFields['TID'] = isset($responseFields['TID']) ? $responseFields['TID'] : null; // ID transakcie, unique
        $responseFields['OID'] = isset($responseFields['OID']) ? $responseFields['OID'] : null; //Trustpay id objednavky
        $responseFields['TSS'] = isset($responseFields['TSS']) ? $responseFields['TSS'] : null; // transakcie signed Y/N

        $responseFields['CardID'] = isset($responseFields['CardID']) ? $responseFields['CardID'] : null;
        $responseFields['CardMask'] = isset($responseFields['CardMask']) ? $responseFields['CardMask'] : null;
        $responseFields['CardExp'] = isset($responseFields['CardExp']) ? $responseFields['CardExp'] : null;
        $responseFields['AuthNumber'] = isset($responseFields['AuthNumber']) ? $responseFields['AuthNumber'] : null;
        $responseFields['CardRecTxSec'] = isset($responseFields['CardRecTxSec']) ? $responseFields['CardRecTxSec'] : null;
        $responseFields['AcqResId'] = isset($responseFields['AcqResId']) ? $responseFields['AcqResId'] : null;

        $responseFields['SIG2'] = isset($responseFields['SIG2']) ? $responseFields['SIG2'] : null; // Podpis banky

        if ($responseFields['AID'] !== $config['TRUSTPAY_AID']) {
            $this->ci->response(errorMessage(1003, 'AID'), 400);
        }
        if ($responseFields['CUR'] !== 'EUR') {
            $this->ci->response(errorMessage(1003, 'CUR'), 400);
        }

        $trustCard = new TrustCard();
        $message = $responseFields['AID'] . $responseFields['TYP'] . $responseFields['AMT'] . $responseFields['CUR']  . $responseFields['REF'] . $responseFields['RES'] . $responseFields['TID'] . $responseFields['OID'] . $responseFields['TSS'] . $responseFields['CardID'] . $responseFields['CardMask'] . $responseFields['CardExp'] . $responseFields['AuthNumber'] . $responseFields['CardRecTxSec'] . $responseFields['AcqResId'];

        $signature = $trustCard->generateSign($message, $config['TRUSTPAY_KEY']);

        if ($responseFields['SIG2'] !== $signature) {
            $this->ci->response(errorMessage(1003, 'SIG2'), 400);
        }
        else
        {
            if (isset($responseFields['RES']) && in_array($responseFields['RES'], array(0,3,4))) {
                $this->sendReservationToProperty($reservationData , $responseFields, FALSE, FALSE);
            } else {
                $this->makeErrorPaymentProcess($reservationData , $responseFields, FALSE);
            }
        }

        $this->ci->response('OK', 200);
    }

    private function TrustCard_response($reservationData, $responseData)
    {
        if (isset($responseData['RES']) && in_array($responseData['RES'], array(0,3,4)))
        {
            $this->sendReservationToProperty($reservationData,$responseData);
        }
        else
        {
			$this->makeErrorPaymentProcess($responseData,$responseData);
        }
    }

    /******************
    *
    *   WebPay platby
    *
    ******************/

    private function WebPay_request($reservationData)
    {
        $this->ci->load->library('Payments/WebPay');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $config = $this->ci->config->item('property')['payments'];

        if (isset($config['WEBPAY_ENV']) &&  ($config['WEBPAY_ENV'] == 'TEST'))
        {
            $privateKey = APPPATH . 'config/payments/' . $reservationData['property_id'] . '/test.gpwebpay-pvk.key';
        }
        else
        {
            $privateKey = APPPATH . 'config/payments/' . $reservationData['property_id'] . '/prod.gpwebpay-pvk.key';
        }

        $webPay = new WebPay();
        $webPay->setPrivateKey($privateKey, $config['WEBPAY_PASSWORD']);

        if (isset($config['WEBPAY_ENV']) &&  ($config['WEBPAY_ENV'] == 'TEST'))
        {
            $webPay->setWebPayUrl('https://test.3dsecure.gpwebpay.com/rb/order.do');
        }
        else
        {
            $webPay->setWebPayUrl('https://3dsecure.gpwebpay.com/rb/order.do');
        }

        $webPay->setResponseUrl(base_url('api/payment/result/'.$reservationData['property_id'].'/'.$reservationData['token']));
        $webPay->setMerchantNumber($config['WEBPAY_MID']);

        $webPay->setOrderInfo($reservationData['id'], $reservationData['id'], $reservationData['total_price']);

        redirect($webPay->requestUrl(),'location', 301);
    }

    private function WebPay_response($reservationData, $responseData)
    {
        $this->ci->load->library('Payments/WebPay');
        $this->ci->load->config('property/property_'.$reservationData['property_id']);
        $config = $this->ci->config->item('property')['payments'];

        if (isset($config['WEBPAY_ENV']) &&  ($config['WEBPAY_ENV'] == 'TEST'))
        {
            $publicKey = APPPATH . 'config/payments/' . $reservationData['property_id'] . '/gpe.signing_test.pem';
        }
        else
        {
            $publicKey = APPPATH . 'config/payments/' . $reservationData['property_id'] . '/gpe.signing_prod.pem';
        }

        $webPay = new WebPay();

        $webPay->setPublicKey($publicKey);
        $webPay->setResponseParams($responseData);

        if ($webPay->verify())
        {
            $this->sendReservationToProperty($reservationData , $responseData);
        }
        else
        {
            $this->makeErrorPaymentProcess($reservationData , $responseData);
        }
    }

	/******************
	 *
	 *   PayU platby - typeID 20
	 *
	 ******************/
	private function PayU_request($reservationData)
	{
		$this->ci->load->library('Payments/PayU');
		$this->ci->load->config('property/property_'.$reservationData['property_id']);
		$config = $this->ci->config->item('property')['payments'];

		$payU = new PayU();
		if (isset($config['PAYU_MODE']) && ($config['PAYU_MODE'] == "TEST"))
		{
			$payU->setTestMode();
		}


		$payU->setPosData($config['PAYU_ID'],$config['PAYU_CLIENT_ID'], $config['PAYU_CLIENT_SECRET'], $config['PAYU_AUTH_MD5_KEY']);
		$payU->setAmount($reservationData['total_price'] * 100);
		$payU->setCurrency($config['PAYU_CURR']);
		$payU->setOrderID($reservationData['id']);
		$payU->setLanguage($reservationData['lang']);

		$payU->setClientName($reservationData['first_name'], $reservationData['last_name']);
		$payU->setClientEmail($reservationData['email']);
		$payU->setClientIP($reservationData['ip']);
		$payU->setNotifyUrl(base_url('api/payment/verify/'.$reservationData['property_id'].'/'.$reservationData['token']));
		$payU->setContinueUrl(base_url('api/payment/result/'.$reservationData['property_id'].'/'.$reservationData['token']));


		if ($response = $payU->createOrder())
		{
			$this->sendReservationToProperty($reservationData,'success');
		}
		else
		{
			$this->makeErrorPaymentProcess($reservationData,'error');
		}

	}

	public function PayU_response($reservationData, $responseData)
	{
		if (isset($responseData['order'], $responseData['order']['orderId']))
		{
			$this->ci->load->config('property/property_'.$reservationData['property_id']);
			$config = $this->ci->config->item('property')['payments'];


			if ($config['PAYU_ID'] == $responseData['order']['merchantPosId'])
			{
				if (isset($responseData['order']['status']) && strtoupper($responseData['order']['status']) == "COMPLETED")
				{
					$this->sendReservationToProperty($reservationData,$responseData);
				}
			}
		}

        $this->makeErrorPaymentProcess($reservationData,$responseData);
	}

	public function PayU_verify($reservationData, $responseData)
	{
		if (isset($responseData['order'], $responseData['order']['orderId']))
		{
			$this->ci->load->config('property/property_'.$reservationData['property_id']);
			$config = $this->ci->config->item('property')['payments'];

			if ($config['PAYU_ID'] == $responseData['order']['merchantPosId'])
			{
				if (isset($responseData['order']['status']) && strtoupper($responseData['order']['status']) == "COMPLETED")
				{
					$this->sendReservationToProperty($reservationData , $responseData, FALSE, FALSE);
				}
				elseif (isset($responseData['order']['status']) && strtoupper($responseData['order']['status']) == "CANCELED")
				{
					$this->makeErrorPaymentProcess($reservationData , $responseData, FALSE);
				}
			}
		}
	}

    public function testPayment_request($reservationData){

	    $redirectUrl = base_url('api/payment/doPayment/'.$reservationData['property_id'].'/'.$reservationData['token']);
        redirect($redirectUrl,'location', 301);
    }

    public function testPayment_response($reservationData,$responseData){

	    if($responseData == '1'){
	        $this->sendReservationToProperty($reservationData,$responseData);
        }
	    else{
            $this->makeErrorPaymentProcess($reservationData,$responseData);
        }

        //redirect($redirectUrl,'location', 301);
    }

    private function makeBGReservationRequest($reservationID,$reservation,$property_id,$payed = 'false',$version){

        $discountItems = array();
        $items = array();
        $packings = array();

        if(isset($reservation['discount_id']) && !empty($reservation['discount_id']) && isset($reservation['discount_unit_price']) && !empty($reservation['discount_unit_price'])){
            $discountExternalID = $this->ci->reservation_model->getDiscountExternalID($property_id,$reservation['discount_id']);
            $discountItems[] = array(
                'idDiscount'=> $discountExternalID,
                'price' => formatNumApi($reservation['discount_unit_price']),
            );
        }

        if(!empty($reservation['items'])){
            foreach($reservation['items'] as $item){

                $items[] = array(
                    'idPlu' => $item['external_id'],
                    'amount' => $item['count'],
                    'price' => formatNumApi($item['unit_price']),
                    'note' => isset($item['note']) && !empty($item['note']) ? $item['note'] : '',
                );
                if(isset($item['offer_data']['packing']['external_id']) && !empty($item['offer_data']['packing']['external_id']) &&
                    isset($item['offer_data']['packing']['price']) && !empty($item['offer_data']['packing']['price'])){
                    if(isset($packings[$item['offer_data']['packing']['packing_id']]) && !empty($packings[$item['offer_data']['packing']['packing_id']])){
                        $packings[$item['offer_data']['packing']['packing_id']]['amount'] = (int)$packings[$item['offer_data']['packing']['packing_id']]['amount'] + (int)$item['count'];
                    }
                    else{
                        $packings[$item['offer_data']['packing']['packing_id']] = array(
                            'idPlu' => $item['offer_data']['packing']['external_id'],
                            'amount' => $item['count'],
                            'price' => formatNumApi($item['offer_data']['packing']['price']),
                            'note' => '',
                        );
                    }
                }
                if(isset($item['upsells']) && !empty($item['upsells'])){
                    foreach($item['upsells'] as $upsell){
                        $items[] = array(
                            'idPlu' => $upsell['external_id'],
                            'amount' => $upsell['count'],
                            'price' => formatNumApi($upsell['unit_price']),
                            'note' => '',
                        );
                    }
                }
            }
        }

        $menuDescription = '';
        $this->ci->lang->load('global','slovak');
        if(isset($reservation['menu']) && !empty($reservation['menu'])){
            foreach($reservation['menu'] as $key => $menu){
                $menuDescription .='<br>';
                $menuDescription .= '<strong>Menu č. '.($key+1).'</strong><br>';
                if(isset($menu['items']) && !empty($menu['items'])){
                    foreach($menu['items'] as $type => $menu_item){
                        if(isset($menu_item) && !empty($menu_item)){
                            if(isset($menu_item['external_id']) && !empty($menu_item['external_id']) && $menu_item['traceable'] == '0') {
                                $items[] = array(
                                    'idPlu' => $menu_item['external_id'],
                                    'amount' => $menu['count'],
                                    'price' => isset($menu_item['price']) && !empty($menu_item['price']) ? formatNumApi($menu_item['price']) : formatNumApi('0,00'),
                                    'note' => isset($menu['note']) && !empty($menu['note']) ? $menu['note'] : '',
                                );
                                if(isset($menu_item['daily_menu_data']['packing']['external_id']) && !empty($menu_item['daily_menu_data']['packing']['external_id']) &&
                                    isset($menu_item['daily_menu_data']['packing']['price']) && !empty($menu_item['daily_menu_data']['packing']['price'])){
                                    if(isset($packings[$menu_item['daily_menu_data']['packing']['id']]) && !empty($packings[$menu_item['daily_menu_data']['packing']['id']])){
                                        $packings[$menu_item['daily_menu_data']['packing']['id']]['amount'] = (int)$packings[$menu_item['daily_menu_data']['packing']['id']]['amount'] + (int)$menu['count'];
                                    }
                                    else{
                                        $packings[$menu_item['daily_menu_data']['packing']['id']] = array(
                                            'idPlu' => $menu_item['daily_menu_data']['packing']['external_id'],
                                            'amount' => $menu['count'],
                                            'price' => isset($menu_item['daily_menu_data']['packing']['price']) && !empty($menu_item['daily_menu_data']['packing']['price']) ? formatNumApi($menu_item['daily_menu_data']['packing']['price']) : formatNumApi('0,00'),
                                            'note' => '',
                                        );
                                    }
                                }
                            }
                            else{
                                if(isset($menu_item['daily_menu_data']['packing']['external_id']) && !empty($menu_item['daily_menu_data']['packing']['external_id']) &&
                                    isset($menu_item['daily_menu_data']['packing']['price']) && !empty($menu_item['daily_menu_data']['packing']['price'])){
                                    if(isset($packings[$menu_item['daily_menu_data']['packing']['id']]) && !empty($packings[$menu_item['daily_menu_data']['packing']['id']])){
                                        $packings[$menu_item['daily_menu_data']['packing']['id']]['amount'] = (int)$packings[$menu_item['daily_menu_data']['packing']['id']]['amount'] + (int)$menu['count'];
                                    }
                                    else{
                                        $packings[$menu_item['daily_menu_data']['packing']['id']] = array(
                                            'idPlu' => $menu_item['daily_menu_data']['packing']['external_id'],
                                            'amount' => $menu['count'],
                                            'price' => isset($menu_item['daily_menu_data']['packing']['price']) && !empty($menu_item['daily_menu_data']['packing']['price']) ? formatNumApi($menu_item['daily_menu_data']['packing']['price']) : formatNumApi('0,00'),
                                            'note' => '',
                                        );
                                    }

                                }
                            }

                            if(isset($menu_item['daily_menu_data']['name']) && !empty($menu_item['daily_menu_data']['name'])){
                                $daily_menu_name = $menu_item['daily_menu_data']['name'];
                            }
                            elseif(isset($menu_item['daily_menu_data']['internal_name']) && !empty($menu_item['daily_menu_data']['internal_name'])){
                                $daily_menu_name = $menu_item['daily_menu_data']['internal_name'];
                            }
                            else{
                                $daily_menu_name = 'Plu č.'.$menu_item['external_id'];
                            }
                            $menuDescription .= '<strong>'.lang('type.'.$type).'</strong> : '.$menu['count'].'x '.$daily_menu_name.'<br>';
                        }
                    }
                }
            }
        }

        if(isset($packings) && !empty($packings)){
            foreach($packings as $pack){
                $items[] = $pack;
            }
        }

        $customerInfo = '<strong>Zákazník:</strong> '.$reservation['first_name'].' '.$reservation['last_name'].'<br>';
        $customerInfo .= '<strong>Kontakt:</strong> '.$reservation['email'].' , '.$reservation['phone'].'<br>';

        if(isset($reservation['city']) && !empty($reservation['city'])){
            $customerInfo .= '<strong>Adresa:</strong> '.$reservation['street'].' '.$reservation['number'].', '.$reservation['city'].'<br>';
        }
        $table_id = null;
        if($version == '2'){
            $customerInfo = '';
            if (isset($reservation['table_token']) && !empty($reservation['table_token'])) {
                if ($table_data = $this->ci->reservation_model->getTable($property_id, $reservation['table_token'])) {
                    $table_id     = $table_data['external_id'];
                    $customerInfo .= '<br><strong>Stôl:</strong>' . $table_data['name'] . ' ' . $table_data['area_name'];
                    if(isset($reservation['note']) && !empty($reservation['note'])){
                        $customerInfo .= '<br><strong>Poznámka:</strong>'.$reservation['note'];
                    }
                }
            }
            if (isset($reservation['accommodated_guest_id']) && !empty($reservation['accommodated_guest_id'])) {
                $customerInfo .= '<br><strong>Izba:</strong>' . $reservation['room_nr'];
                if(isset($reservation['note']) && !empty($reservation['note'])){
                    $customerInfo .= '<br><strong>Poznámka:</strong>'.$reservation['note'];
                }
            }
        }
        $customerInfo .= '<strong>Cena spolu:</strong> '.$reservation['discount_price'].'<br>';
        //$customerInfo .= $menuDescription;

        $orderType = $this->ci->reservation_model->getOrderTypeId($reservation['transportation_id'],$property_id);
        if(isset($orderType['external_plu_id']) && !empty($orderType['external_plu_id'])){
            $items[] = array(
                'idPlu' => $orderType['external_plu_id'],
                'amount' => '1',
                'price' => isset($orderType['price']) && !empty($orderType['price']) ? formatNumApi($orderType['price']) : formatNumApi('0.00'),
                'note' => ''
            );
        }

        $paymentTypeId = $this->ci->reservation_model->getPaymentTypeId($reservation['payment_id'],$property_id);

        $requestBG = array(
            'idExternalSystem' => $reservationID,
            'date' => dateToMicroSeconds(date(DATE_ISO8601)),
            'orderTypeId' => $orderType['external_id'],
            'payed' => $payed,
            'paymentTypeId' => $paymentTypeId,
            'customerInfo' => $customerInfo,
            'note' => $reservation['note'],
            'credit' => 0,
            'items' => $items,
            'discountItems' => $discountItems,
        );

        if($version == '2'){
            $requestBG['guestId'] = null;
            $requestBG['tableId'] = isset($table_id) && !empty($table_id) ? $table_id : null;
            $requestBG['accommodatedGuestNumber'] = isset($reservation['accommodated_guest_id']) && !empty($reservation['accommodated_guest_id']) ? $reservation['accommodated_guest_id'] : null;
            if(isset($reservation['accommodated_guest_id']) && !empty($reservation['accommodated_guest_id'])){
                $requestBG['paymentTypeId'] = null;
            }
        }

        return $requestBG;
    }

}
