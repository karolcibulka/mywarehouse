<?php


class FrontCache
{

    protected $ci;
    protected $type;
    protected $property_id;

    public function __construct($data)
    {
        $this->ci = & get_instance();
        $this->type = $data['type'];
        $this->property_id = $data['property_id'];
        $this->ci->load->model('Front_model','front_model');
        $this->ci->load->library('curl');
    }

    public function deleteCache(){
        if($frontWebSite = $this->ci->front_model->getPropertySettings($this->property_id)){
            $this->ci->curl->create($frontWebSite.'/cache/delete/'.$this->property_id.'/'.$this->type);
            $response = json_decode($this->ci->curl->execute(),TRUE);
            return true;
        }



    }

}