<?php


class Reservation
{

    public $ci;

    public function __construct()
    {
        $this->ci = & get_instance();
        $this->ci->load->model('Reservation_model','reservation_model');
    }

    public function storeReservation($token,$reservationData){

        $paymentType = $this->ci->reservation_model->getPaymentGate($reservationData['payment_id'],$reservationData['property_id']);
        if($paymentType['type'] == 'online'){
            $this->ci->load->library('payments');
            $this->ci->payments->choosePaymentGate($paymentType['id'],$reservationData);
        }
        else{
            $this->finalProcessReservation($token,$reservationData['property_id']);
        }

    }

    public function finalProcessReservation($token,$propertyID){

        $reservationData = $this->ci->reservation_model->getPaymentsDataForReservationToken($token);
        $propertyData = $this->ci->reservation_model->getPropertyData($propertyID);

        $propertySettings = $this->ci->reservation_model->getPropertySettings($propertyID);
        $reservationRequest = $this->makeBGReservationRequest($reservationData['id'],$reservationData,$propertySettings['id'],'false',$propertySettings['version']);

        $this->ci->load->library('ExternalSystems/BlueGastro',$propertySettings,'blueGastro');
        $blueGastroResponse = $this->ci->blueGastro->storeReservation($reservationRequest,$propertySettings['version']);

        if(isset($blueGastroResponse['returnMessage']['code']) && !empty($blueGastroResponse['returnMessage']['code']) && ($blueGastroResponse['returnMessage']['code'] == '1') && !is_null($blueGastroResponse['orderId'])){

            $logData = array(
                'reservation_id'=>$reservationData['id'],
                'property_id' => $propertyID,
                'request'=>json_encode($reservationRequest),
                'status' => savedToBGStatus(),
                'response' => json_encode($blueGastroResponse),
            );

            $this->ci->reservation_model->storeReservationLog($logData);
            $this->ci->reservation_model->updateReservation($reservationData['id'],array('status'=>savedToBGStatus(),'order_id'=>$blueGastroResponse['orderId']));

            $this->ci->load->library('notification');
            $this->ci->notification->sendSuccessReservationEmail($propertyData,$reservationData);
            $this->ci->notification->sendEmailToProperty($propertyData,$reservationData);
            $this->ci->notification->sendSuccessReservationSms($propertyData,$reservationData);

            if(isset($reservationData['discount_id']) && !empty($reservationData['discount_id'])){
                $this->ci->reservation_model->iterateDiscount($reservationData['discount_id'],$propertyID);
            }

            $redirectUrl = $propertyData['web'].'/'.$reservationData['lang'].'/response/success/'.$token;
        }
        else{

            $logData = array(
                'reservation_id'=>$reservationData['id'],
                'property_id' => $propertyData['property_id'],
                'request'=>json_encode($reservationRequest),
                'status' => errorSaveToBGStatus(),
                'response' => json_encode($blueGastroResponse),
            );

            $this->ci->load->library('notification');

            $this->ci->notification->sendEmailToProperty($propertyData,$reservationData);
            $this->ci->reservation_model->storeReservationLog($logData);
            $this->ci->reservation_model->updateReservation($reservationData['id'],array('status'=>errorSaveToBGStatus()));


            $redirectUrl = $propertyData['web'].'/'.$reservationData['lang'].'/response/error/'.$token;
        }

        redirect($redirectUrl,'location', 301);
    }

    private function makeBGReservationRequest($reservationID,$reservation,$property_id,$payed = 'false',$version){

            $discountItems = array();
            $items = array();
            $packings = array();

            if(isset($reservation['discount_id']) && !empty($reservation['discount_id']) && isset($reservation['discount_unit_price']) && !empty($reservation['discount_unit_price'])){
                $discountExternalID = $this->ci->reservation_model->getDiscountExternalID($property_id,$reservation['discount_id']);
                $discountItems[] = array(
                    'idDiscount'=> $discountExternalID,
                    'price' => formatNumApi($reservation['discount_unit_price']),
                );
            }

            if(!empty($reservation['items'])){
                foreach($reservation['items'] as $item){

                    $items[] = array(
                        'idPlu' => $item['external_id'],
                        'amount' => $item['count'],
                        'price' => formatNumApi($item['unit_price']),
                        'note' => isset($item['note']) && !empty($item['note']) ? $item['note'] : '',
                    );
                    if(isset($item['offer_data']['packing']['external_id']) && !empty($item['offer_data']['packing']['external_id']) &&
                        isset($item['offer_data']['packing']['price']) && !empty($item['offer_data']['packing']['price'])){
                            if(isset($packings[$item['offer_data']['packing']['packing_id']]) && !empty($packings[$item['offer_data']['packing']['packing_id']])){
                                $packings[$item['offer_data']['packing']['packing_id']]['amount'] = (int)$packings[$item['offer_data']['packing']['packing_id']]['amount'] + (int)$item['count'];
                            }
                            else{
                                $packings[$item['offer_data']['packing']['packing_id']] = array(
                                    'idPlu' => $item['offer_data']['packing']['external_id'],
                                    'amount' => $item['count'],
                                    'price' => formatNumApi($item['offer_data']['packing']['price']),
                                    'note' => '',
                                );
                            }
                    }
                    if(isset($item['upsells']) && !empty($item['upsells'])){
                        foreach($item['upsells'] as $upsell){
                            $items[] = array(
                                'idPlu' => $upsell['external_id'],
                                'amount' => $upsell['count'],
                                'price' => formatNumApi($upsell['unit_price']),
                                'note' => '',
                            );
                        }
                    }
                }
            }

        $menuDescription = '';
        $this->ci->lang->load('global','slovak');
        if(isset($reservation['menu']) && !empty($reservation['menu'])){
            foreach($reservation['menu'] as $key => $menu){
                $menuDescription .='<br>';
                $menuDescription .= '<strong>Menu č. '.($key+1).'</strong><br>';
                if(isset($menu['items']) && !empty($menu['items'])){
                    foreach($menu['items'] as $type => $menu_item){
                        if(isset($menu_item) && !empty($menu_item)){
                            if(isset($menu_item['external_id']) && !empty($menu_item['external_id']) && $menu_item['traceable'] == '0') {
                                $items[] = array(
                                    'idPlu' => $menu_item['external_id'],
                                    'amount' => $menu['count'],
                                    'price' => isset($menu_item['price']) && !empty($menu_item['price']) ? formatNumApi($menu_item['price']) : formatNumApi('0,00'),
                                    'note' => isset($menu['note']) && !empty($menu['note']) ? $menu['note'] : '',
                                );
                                if(isset($menu_item['daily_menu_data']['packing']['external_id']) && !empty($menu_item['daily_menu_data']['packing']['external_id']) &&
                                    isset($menu_item['daily_menu_data']['packing']['price']) && !empty($menu_item['daily_menu_data']['packing']['price'])){
                                        if(isset($packings[$menu_item['daily_menu_data']['packing']['id']]) && !empty($packings[$menu_item['daily_menu_data']['packing']['id']])){
                                            $packings[$menu_item['daily_menu_data']['packing']['id']]['amount'] = (int)$packings[$menu_item['daily_menu_data']['packing']['id']]['amount'] + (int)$menu['count'];
                                        }
                                        else{
                                            $packings[$menu_item['daily_menu_data']['packing']['id']] = array(
                                                'idPlu' => $menu_item['daily_menu_data']['packing']['external_id'],
                                                'amount' => $menu['count'],
                                                'price' => isset($menu_item['daily_menu_data']['packing']['price']) && !empty($menu_item['daily_menu_data']['packing']['price']) ? formatNumApi($menu_item['daily_menu_data']['packing']['price']) : formatNumApi('0,00'),
                                                'note' => '',
                                            );
                                        }
                                }
                            }
                            else{
                                if(isset($menu_item['daily_menu_data']['packing']['external_id']) && !empty($menu_item['daily_menu_data']['packing']['external_id']) &&
                                    isset($menu_item['daily_menu_data']['packing']['price']) && !empty($menu_item['daily_menu_data']['packing']['price'])){
                                        if(isset($packings[$menu_item['daily_menu_data']['packing']['id']]) && !empty($packings[$menu_item['daily_menu_data']['packing']['id']])){
                                            $packings[$menu_item['daily_menu_data']['packing']['id']]['amount'] = (int)$packings[$menu_item['daily_menu_data']['packing']['id']]['amount'] + (int)$menu['count'];
                                        }
                                        else{
                                            $packings[$menu_item['daily_menu_data']['packing']['id']] = array(
                                                'idPlu' => $menu_item['daily_menu_data']['packing']['external_id'],
                                                'amount' => $menu['count'],
                                                'price' => isset($menu_item['daily_menu_data']['packing']['price']) && !empty($menu_item['daily_menu_data']['packing']['price']) ? formatNumApi($menu_item['daily_menu_data']['packing']['price']) : formatNumApi('0,00'),
                                                'note' => '',
                                            );
                                        }

                                }
                            }

                            if(isset($menu_item['daily_menu_data']['name']) && !empty($menu_item['daily_menu_data']['name'])){
                                $daily_menu_name = $menu_item['daily_menu_data']['name'];
                            }
                            elseif(isset($menu_item['daily_menu_data']['internal_name']) && !empty($menu_item['daily_menu_data']['internal_name'])){
                                $daily_menu_name = $menu_item['daily_menu_data']['internal_name'];
                            }
                            else{
                                $daily_menu_name = 'Plu č.'.$menu_item['external_id'];
                            }
                            $menuDescription .= '<strong>'.lang('type.'.$type).'</strong> : '.$menu['count'].'x '.$daily_menu_name.'<br>';
                        }
                    }
                }
            }
        }

        if(isset($packings) && !empty($packings)){
            foreach($packings as $pack){
                $items[] = $pack;
            }
        }

        $customerInfo = '<strong>Zákazník:</strong> '.$reservation['first_name'].' '.$reservation['last_name'].'<br>';
        $customerInfo .= '<strong>Kontakt:</strong> '.$reservation['email'].' , '.$reservation['phone'].'<br>';


        if(isset($reservation['city']) && !empty($reservation['city'])){
            $customerInfo .= '<strong>Adresa:</strong> '.$reservation['street'].' '.$reservation['number'].', '.$reservation['city'].'<br>';
        }
        $table_id = null;
        if($version == '2') {
            $customerInfo = '';
            if (isset($reservation['table_token']) && !empty($reservation['table_token'])) {
                if ($table_data = $this->ci->reservation_model->getTable($property_id, $reservation['table_token'])) {
                    $table_id     = $table_data['external_id'];
                    $customerInfo .= '<br><strong>Stôl:</strong>' . $table_data['name'] . ' ' . $table_data['area_name'];

                }
            }
            if (isset($reservation['accommodated_guest_id']) && !empty($reservation['accommodated_guest_id'])) {
                $customerInfo .= '<br><strong>Izba:</strong>' . $reservation['room_nr'];
            }
            if(isset($reservation['note']) && !empty($reservation['note'])){
                $customerInfo .= '<br><strong>Poznámka:</strong>'.$reservation['note'];
            }
        }
        $customerInfo .= '<br><strong>Cena spolu:</strong> '.$reservation['discount_price'].'<br>';
        //$customerInfo .= $menuDescription;

        $orderType = $this->ci->reservation_model->getOrderTypeId($reservation['transportation_id'],$property_id);
        if(isset($orderType['external_plu_id']) && !empty($orderType['external_plu_id'])){
            $items[] = array(
                'idPlu' => $orderType['external_plu_id'],
                'amount' => '1',
                'price' => isset($orderType['price']) && !empty($orderType['price']) ? formatNumApi($orderType['price']) : formatNumApi('0.00'),
                'note' => ''
            );
        }

        $paymentTypeId = $this->ci->reservation_model->getPaymentTypeId($reservation['payment_id'],$property_id);


        $requestBG = array(
            'idExternalSystem' => $reservationID,
            'date' => dateToMicroSeconds(date(DATE_ISO8601)),
            'orderTypeId' => $orderType['external_id'],
            'payed' => $payed,
            'paymentTypeId' => $paymentTypeId,
            'customerInfo' => $customerInfo,
            'note' => $reservation['note'],
            'credit' => 0,
            'items' => $items,
            'discountItems' => $discountItems,
        );

        if($version == '2'){
            $requestBG['guestId'] = null;
            $requestBG['tableId'] = isset($table_id) && !empty($table_id) ? $table_id : null;
            $requestBG['accommodatedGuestNumber'] = isset($reservation['accommodated_guest_id']) && !empty($reservation['accommodated_guest_id']) ? $reservation['accommodated_guest_id'] : null;
            if(isset($reservation['accommodated_guest_id']) && !empty($reservation['accommodated_guest_id'])){
                $requestBG['paymentTypeId'] = null;
            }
        }

        return $requestBG;
    }
}