<?php


class Notification
{
    public $ci;
    protected $walkin_email = 'walkin_email@developer.com';
    protected $walkin_phone = '000000';

    public function __construct()
    {
        $this->ci = & get_instance();
        $this->ci->load->library('email');
        $this->ci->load->model('Notifications_model','notifications_model');
    }

    public function sendSuccessReservationEmail($property,$data){
        if($data['email'] !== $this->walkin_email){
            $this->ci->email->clear(TRUE);
            $this->ci->email->initialize($this->getConfig());

            $lang = $this->ci->notifications_model->getLang($data['id']);
            $this->ci->lang->load('global',getLangFromCode($lang));

            $this->ci->email->from('no-reply@softsolutions.sk', $property['name']);
            $this->ci->email->to($data['email']);

            $this->ci->email->subject(lang('email.reservationNumber').$data['id']);

            if(isset($data['table_token']) && !empty($data['table_token'])){
                $allData['table'] = $this->ci->notifications_model->getTable($data['property_id'],$data['table_token']);
            }

            $allData['property'] = $property;
            $allData['reservation'] = $data;

            $template = $this->ci->load->view('api/email_templates/successReservation2',$allData,true);

            $this->ci->email->message($template);

            $this->ci->email->send();
        }
    }

    public function sendSuccessReservationSms($property,$data){
        if($data['phone']!==$this->walkin_phone){
            if($smsSettings = $this->ci->notifications_model->getPropertySettingsSms($property['property_id'])){

                if(isset($smsSettings['text']) && !empty($smsSettings['text'])){
                    $smsSettings['text'] = $this->translateSms($smsSettings['text'],$data);
                }
                else{
                    $smsSettings['text'] = lang('sms.order').$property['name'];
                }

                $lang = $this->ci->notifications_model->getLang($data['id']);
                $this->ci->lang->load('global',getLangFromCode($lang));

                $message = $smsSettings['text'];
                $recipient = $data['phone'];
                $this->sendSms($message,$recipient,$smsSettings);
                return true;
            }
            return false;
        }
    }

    public function sendSubscribeEmail($lang,$propertyID,$token,$email){
        $this->ci->lang->load('global',getLangFromCode($lang));

        $data['activatePath'] = base_url('api/newsletter/activateSubscribe/'.$lang.'/'.$propertyID.'/'.$token);
        $data['deactivatePath'] = base_url('api/newsletter/deactivateSubscribe/'.$lang.'/'.$propertyID.'/'.$token);
        $data['property'] = $property = $this->ci->notifications_model->getProperty($propertyID);
        $message = $this->ci->load->view('email_templates/subscribeEmail',$data,true);

        $this->ci->email->clear(TRUE);
        $this->ci->email->initialize($this->getConfig());
        $this->ci->email->from('no-reply@softsolutions.sk', $property['name']);
        $this->ci->email->to($email);
        $this->ci->email->subject(lang('email.actSubscribeH'));
        $this->ci->email->message($message);
        $this->ci->email->send();
    }

    private function translateSms($text,$data){
        $newText = str_replace('{first_name}',$data['first_name'],$text);
        $newText = str_replace('{last_name}',$data['last_name'],$newText);
        $newText = str_replace('{total_price}',$data['total_price'],$newText);
        $newText = str_replace('{reservation_number}',$data['id'],$newText);
        return $newText;
    }

    public function sendEmailToProperty($property,$data){
        if($notificationEmail = $this->ci->notifications_model->getPropertySettingsNotificationEmail($property['property_id'])){

            $lang = $this->ci->notifications_model->getLang($data['id']);
            $this->ci->lang->load('global',getLangFromCode($lang));

            $this->ci->email->clear(TRUE);
            $this->ci->email->initialize($this->getConfig());

            $this->ci->email->from('no-reply@softsolutions.sk', 'GASTRO SYSTEM - '.$property['name']);
            $this->ci->email->to($notificationEmail);

            $this->ci->email->subject('GASTRO SYSTEM - Rezervácia č.'.$data['id']);

            if(isset($data['table_token']) && !empty($data['table_token'])){
                $allData['table'] = $this->ci->notifications_model->getTable($data['property_id'],$data['table_token']);
            }

            $allData['property'] = $property;
            $allData['reservation'] = $data;

            $template = $this->ci->load->view('api/email_templates/successReservation2',$allData,true);
            $this->ci->email->message($template);

            $this->ci->email->send();
        }
    }

    private function getConfig(){
        return Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.websupport.sk',
            'smtp_user' => 'no-reply@softsolutions.sk',
            'smtp_pass' => 'Bo6Bxr+=+@',
            'smtp_port' => '465',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'wordwrap' => TRUE,
            'crlf' => '\r\n',
            'newline' => '\r\n'
        );
    }

    private function sendSms($message,$recipient,$smsSettings){
        try {
            $signature = substr(md5($smsSettings['integration_id'].$recipient), 10, 11);
            $sender = $smsSettings['name'];

            $values = array(
                "integration_id" => $smsSettings['code'],
                "sender" => $sender,
                "recipient" => $recipient,
                "message" => $message,
                "signature" => $signature
            );

            $client = new SoapClient("http://app.smsfarm.sk/api/?wsdl");

            $request_id = $client->SendMessage($values);

            return true;

        } catch (Exception $e) {
            return false;
        }
    }
}