<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['property']['payments']['TATRAPAY_MID'] = '999';
$config['property']['payments']['TATRAPAY_KEY'] = '9999'; //'k8JMXjpGY9s43o3N2JVX3qIemPv2SiGjvSnSFeee8J42/oCSwOOke5xfqphTrf97';
$config['property']['payments']['TATRAPAY_CURR'] = 978;
$config['property']['payments']['TATRAPAY_CS'] = '0308';
$config['property']['payments']['TATRAPAY_RSMS'] = '';
$config['property']['payments']['TATRAPAY_REM'] = '';
$config['property']['payments']['TATRAPAY_SignVersion'] = 'HMAC';  // DES, AES, HMAC

$config['property']['payments']['GOPAY_GoID'] = '';
$config['property']['payments']['GOPAY_ClientID'] = '';
$config['property']['payments']['GOPAY_ClientSecret'] = '';
$config['property']['payments']['GOPAY_ProductionMode'] = true;
$config['property']['payments']['GOPAY_Currency'] = 'CZK';

// VUB Ecard
$config['property']['payments']['ECARD_MID'] = '10006001';
$config['property']['payments']['ECARD_KEY'] = 'Kosicehotel55';
$config['property']['payments']['ECARD_CURR'] = 978;
$config['property']['payments']['ECARD_MODE'] = 'TEST';
$config['property']['payments']['ECARD_TRANTYPE'] = 'Auth';
$config['property']['payments']['ECARD_STORETYPE'] = '3d_pay_hosting';

?>