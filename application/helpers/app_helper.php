<?php


function pre_r($expression, $return = FALSE)
{
    list($callee) = debug_backtrace();

    if ($return === true) {
        if (is_string($expression)) return '<pre>' . print_r(str_replace(array('<', '>'), array('&lt;', '&gt;'), $expression), TRUE) . '</pre>';
        return '<pre>' . print_r($expression, TRUE) . '</pre>';
    } elseif ($return == 'noDebug') {
        echo '<pre>';
        if (is_string($expression)) print_r(str_replace(array('<', '>'), array('&lt;', '&gt;'), $expression), FALSE);
        else print_r($expression, FALSE);
        echo '</pre>';
    } else {
        echo '<strong>' . $callee['file'] . '</strong>  <span style="color:tomato">@line ' . $callee['line'] . '</span>';
        echo '<pre>';
        if (is_string($expression)) print_r(str_replace(array('<', '>'), array('&lt;', '&gt;'), $expression), FALSE);
        else print_r($expression, FALSE);
        echo '</pre>';
    }
}

function getCurrentControllerName()
{
    $ci = &get_instance();
    $str = 'dashboard';

    $lang = $ci->session->userdata('language');

    if($ci->uri->segment(2)){
        $str =  $ci->uri->segment(2);
    }
    elseif($ci->uri->segment(1)){
        $str = $ci->uri->segment(1);
    }

    return $str;
}

function getCurrentControllerBreadcrumb()
{

    $str = '';
    $ci = &get_instance();
    $lang = $ci->session->userdata('language');

    $response = array();

    for($i = 0;$i<10;$i++){
        if($ci->uri->segment($i)){
           $response[$i] = $ci->uri->segment($i);
        }
    }


    return $response;
}

function getNestable()
{
    $str = '<script src="'.asset_url('plugin/nestable/jquery.nestable.min.js').'"></script>';
    $str .= '<link rel="stylesheet" href="'.asset_url('plugin/nestable/jquery.nestable.min.css').'">';
    $str .= '<link href="' . asset_url('css/style_b.css') . '" rel="stylesheet" type="text/css"/>';

    return $str;

}

function appDevelopers()
{
    return array(
        '0' => 'kajo.cibulka@gmail.com',
        '1' => 'admin@admin.com',
    );
}

function setSuccessState($message){
    $ci = & get_instance();
    $ci->session->set_flashdata('success_state',$message);
}

function hasActiveChildren($items){
    $class = 'menu-item-active menu-item-open';
    $response = false;
    $ci = & get_instance();
    $segment = $ci->uri->segment(2);


    if(!empty($items)){
        foreach($items as $item){
            if(isset($item['children']) && !empty($item['children'])){
                foreach($item['children'] as $children){
                    if($children['path'] === $segment){
                        $response = true;
                        break 1;
                        break 2;
                    }
                }
            }
            else{
                if($item['path'] === $segment){
                    $response = true;
                    break;
                }
            }
        }
    }

    return !$response ?: $class;
}

function isActiveItem($controller){
    $class =  'menu-item-active';

    $ci = & get_instance();
    $segment = $ci->uri->segment(2);

    if(!$segment){
        return $controller !== 'dashboard' ?: 'menu-item-active';
    }

    return $segment !== $controller ?: $class;
}

function getDataTable(){
    $str = '<script src="'.asset_url('plugin/datatable/jquery.dataTables.min.js"').'"></script>';
    $str .= '<script src="'.asset_url('plugin/datatable/dataTables.bootstrap4.min.js').'"></script>';
    $str .= '<link rel="stylesheet" href="'.asset_url('plugin/datatable/dataTables.bootstrap4.min.css').'">';

    return $str;
}

function getDateRangePicker(){
    $str = '<script type="text/javascript" src="'.asset_url('plugin/daterangepicker/moment.min.js').'"></script>';
    $str .= '<script type="text/javascript" src="'.asset_url('plugin/daterangepicker/daterangepicker.min.js').'"></script>';
    $str .= '<link rel="stylesheet" type="text/css" href="'.asset_url('plugin/daterangepicker/daterangepicker.css').'" />';

    return $str;
}

function getLangFromCode($lang_code)
{
    switch ($lang_code) {
        case "sk":
            return "slovak";
            break;
        case "en":
            return "english";
            break;
        case "cz":
            return "czech";
            break;
        case "ru":
            return "russian";
            break;
        case "hu":
            return "hungary";
            break;
        case "de":
            return "german";
            break;
        case "pl":
            return "polish";
            break;
    }
}

function doSlug($string)
{
    $slug = url_title(iconv('UTF-8', 'ASCII//TRANSLIT', $string), 'dash', true);
    if (strlen($slug) > 30) {
        return substr($slug, 0, 30) . '...';
    } else {
        return substr($slug, 0, 30);
    }
}

function faviconSizes()
{
    return array(
        '32' => 32,
        '57' => 57,
        '76' => 76,
        '96' => 96,
        '120' => 120,
        '144' => 144,
        '195' => 195,
        '228' => 228,
        '128' => 128,
        '152' => 152,
        '167' => 167,
        '180' => 180,
        '192' => 192,
        '196' => 196
    );
}

function developerIDs()
{
    return array('1','8');
}

function isDeveloper()
{
    $CI = &get_instance();
    if ($CI->session->has_userdata('user_id')) {
        if (in_array($CI->session->userdata('user_id'), developerIDs())) {
            return true;
        }
    }
    return false;
}


function generateToken($length = 10)
{
    $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString     = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function applicationLangs()
{
    $langs = array(
        '0' => 'sk',
        '1' => 'en',
        '2' => 'pl',
        '3' => 'de',
        '4' => 'hu',
        '5' => 'ru',
        '6' => 'cz'
    );

    return $langs;
}

function savedToDBStatus()
{
    return 'savedToDB';
}

function savedToBGStatus()
{
    return 'savedToBG';
}

function errorItemsStatus()
{
    return 'errorItems';
}

function errorSaveToBGStatus()
{
    return 'errorSaveToBG';
}

function successPayment()
{
    return 'successPayed';
}

function errorWhilePayment()
{
    return 'errorPayment';
}

function changedPriceStatus()
{
    return 'changedPrice';
}

function random_color_part()
{
    return str_pad(rand(0, 255), 3, '0', STR_PAD_LEFT);
}

function random_color()
{
    return random_color_part() . ',' . random_color_part() . ',' . random_color_part();
}

function dateToMicroSeconds($date)
{
    $explodedDate = explode('+', $date);
    return $explodedDate[0] . '.000+0000';
}

function formatNumApi($num)
{
    return number_format(str_replace(',', '.', $num), '2', '.', '');
}