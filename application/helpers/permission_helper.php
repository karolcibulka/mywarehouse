<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function has_permission($method = false,$custom_controller = false,$checkController = false) {
    $ci = & get_instance();
    if(isset($method) && !empty($method)){
        if($ci->session->has_userdata('permissions')) {
            $userData = $ci->session->userdata('permissions');

            if($custom_controller || $checkController){
                if (isset($userData[$custom_controller][$method])) {
                    return (bool)$userData[$custom_controller][$method];
                }
            }
            else{
                if (isset($ci->uri->segments[2]) && !empty($ci->uri->segments[2])) {
                    $controller = $ci->uri->segments[2];
                    if (isset($userData[$controller][$method])) {
                        return (bool)$userData[$controller][$method];
                    }
                }
            }
        }
    }

    return false;
}
