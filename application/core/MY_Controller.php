<?php
/**
 * Created by PhpStorm.
 * User: karol
 * Date: 2019-03-05
 * Time: 11:35
 */


require APPPATH . '/libraries/REST_Controller.php';
class MY_Controller extends CI_Controller
{

    private $defaultLanguage = NULL;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'language','form','typography','app', 'permission'));

        if(!isset($userLanguage)){

            $this->lang->load(array('global', 'auth'),$this->config->item('language'));
        }
        $this->load->database();


    }
}

class DASH_Controller extends MY_Controller
{

    protected $user_id;
    protected $property_id;
    protected $permissions;
    protected $user_email;
    protected $language;
    protected $languages;
    protected $language_files;
    protected $cache_store_length = 3600*24*31;
    protected $navigation = array();

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ion_auth', 'form_validation','curl','loglib'));

        if (!file_exists(APPPATH.'cache')) {
            mkdir(APPPATH.'cache', 0775, true);
        }




        $this->load->driver('cache',array('driver'=>'file','backup'=>'file'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->load->model('Permission_model','permission_model');
        $this->load->model('Ion_auth_model','ion_auth_model');
        $this->load->model('Global_model','global_model');

        $this->load->driver('cache',
            array('adapter' => 'file', 'backup' => 'file')
        );



        if(!$this->languages = $this->cache->file->get('languages')){
            $this->languages = $this->global_model->getLanguages();
            $this->cache->file->save('languages',$this->languages,$this->cache_store_length);
        }

        if(!$this->language_files = $this->cache->file->get('language_files')){
            $this->language_files = $this->getLanguageFiles();
            $this->cache->file->save('language_files',$this->language_files,$this->cache_store_length);
        }


        if(!$this->ion_auth->logged_in()){
            redirect('dashboard/auth/login');
        }

        $this->loglib->autoStoreYesterdaysLogs();
        $this->loglib->storeLogToCache($this->uri->segment(2),$this->uri->segment(3),$this->uri->segment(4));

        if(isset($_GET['deleteCache'])){
            $this->cache->file->clean();
        }

        $user_session = $this->session->userdata();

        $this->user_id = isset($user_session['user_id']) && !empty($user_session['user_id']) ? $user_session['user_id'] : null;
        $this->property_id = isset($user_session['active_property']) && !empty($user_session['active_property']) ? $user_session['active_property'] : null;
        $this->user_email = isset($user_session['email']) && !empty($user_session['email']) ? $user_session['email'] : null;

        if($this->user_id){
            $this->language = $this->ion_auth_model->getUserLang($this->user_id);
            $this->language = $this->language['name'];

            $userProperties = $this->ion_auth_model->getUserProperties($this->user_id);

            if(!empty($userProperties)) {
                $this->template->set('properties', $userProperties);
                $properties = array_values($userProperties);
                if(!$this->property_id) {
                    $this->property_id = $properties[0]['id'];
                    $this->session->set_userdata('active_property', $this->property_id);
                }
                else{
                    if(!isset($userProperties[$this->property_id]) || empty($userProperties[$this->property_id])){
                        $properties = array_values($userProperties);
                        $this->property_id = $properties[0]['id'];
                        $this->session->set_userdata('active_property', $this->property_id);
                    }
                }
            }

            $this->lang->load($this->language_files,$this->language);
        }

        if(!$this->language){
            $this->lang->load($this->language_files,$this->config->item('language'));
        }

        if($this->user_id && $this->property_id){
            $this->permissions = $this->permission_model->getUserPermissions($this->user_id,false,$this->property_id);
        }


        if(!$this->navigation = $this->cache->file->get('navigation')){
            $this->load->model('Navigation_model','navigation_model');
            $this->navigation = $this->navigation_model->getNavItems();
            $this->cache->file->save('navigation',$this->navigation,$this->cache_store_length);
        }

        $this->template->set('navigationItems',$this->getNavigation());

    }

    private function getLanguageFiles()
    {
        $response = array();

        $path = APPPATH . '/language/slovak';
        if (file_exists($path)) {
            $files = scandir($path);
            foreach ($files as $file) {
                if ($file !== '.' && $file !== '..') {
                    $name       = str_replace('_lang.php', '', $file);
                    $response[] = $name;
                }
            }
        }

        return $response;
    }

    private function getNavigation(){

        $navigation = $this->navigation;

        if(!empty($navigation)){
            foreach($navigation as $category_id => $category){
                if(isset($category['items']) && !empty($category['items'])){
                    foreach($category['items'] as $item_id => $item){
                        if(isset($item['children']) && !empty($item['children'])){
                            foreach($item['children'] as $children_id => $children){
                                if(isset($children['path']) && !empty($children['path']) && has_permission('show',$children['path'],true)){
                                    continue;
                                }
                                unset($navigation[$category_id]['items'][$item_id]['children'][$children_id]);
                            }
                        }
                        else{
                            if(isset($item['path']) && !empty($item['path']) && has_permission('show',$item['path'],true)){
                                continue;
                            }
                            unset($navigation[$category_id]['items'][$item_id]);
                        }
                    }
                }
            }
        }

        if(!empty($navigation)){
            foreach($navigation as $category_id => $category){
                if(isset($category['items']) && !empty($category['items'])){
                    foreach($category['items'] as $item_id => $item){
                        if(!isset($item['children'])){
                            continue;
                        }

                        if(isset($item['children']) && empty($item['children'])){
                            unset($navigation[$category_id]['items'][$item_id]);
                        }

                    }
                }
                else{
                    unset($navigation[$category_id]);
                }
            }
        }

        return $navigation;

    }

    public function wrongState(){
        $data = array('status' => 'warning',
            'message' => lang('message.warning'),
        );

        $this->session->set_flashdata($data);
        redirect(base_url());
    }

    public function icons(){
        $icons = '["icon-xl fas fa-ad","icon-xl fas fa-address-book","icon-xl fas fa-address-card","icon-xl fas fa-adjust","icon-xl fas fa-air-freshener","icon-xl fas fa-align-center","icon-xl fas fa-align-justify","icon-xl fas fa-align-left","icon-xl fas fa-align-right","icon-xl fas fa-allergies","icon-xl fas fa-ambulance","icon-xl fas fa-american-sign-language-interpreting","icon-xl fas fa-anchor","icon-xl fas fa-angle-double-down","icon-xl fas fa-angle-double-left","icon-xl fas fa-angle-double-right","icon-xl fas fa-angle-double-up","icon-xl fas fa-angle-down","icon-xl fas fa-angle-left","icon-xl fas fa-angle-right","icon-xl fas fa-angle-up","icon-xl fas fa-angry","icon-xl fas fa-ankh","icon-xl fas fa-apple-alt","icon-xl fas fa-archive","icon-xl fas fa-archway","icon-xl fas fa-arrow-alt-circle-down","icon-xl fas fa-arrow-alt-circle-left","icon-xl fas fa-arrow-alt-circle-right","icon-xl fas fa-arrow-alt-circle-up","icon-xl fas fa-arrow-circle-down","icon-xl fas fa-arrow-circle-left","icon-xl fas fa-arrow-circle-right","icon-xl fas fa-arrow-circle-up","icon-xl fas fa-arrow-down","icon-xl fas fa-arrow-left","icon-xl fas fa-arrow-right","icon-xl fas fa-arrow-up","icon-xl fas fa-arrows-alt","icon-xl fas fa-arrows-alt-h","icon-xl fas fa-arrows-alt-v","icon-xl fas fa-assistive-listening-systems","icon-xl fas fa-asterisk","icon-xl fas fa-at","icon-xl fas fa-atlas","icon-xl fas fa-atom","icon-xl fas fa-audio-description","icon-xl fas fa-award","icon-xl fas fa-baby","icon-xl fas fa-baby-carriage","icon-xl fas fa-backspace","icon-xl fas fa-backward","icon-xl fas fa-bacon","icon-xl fas fa-bacteria","icon-xl fas fa-bacterium","icon-xl fas fa-bahai","icon-xl fas fa-balance-scale","icon-xl fas fa-balance-scale-left","icon-xl fas fa-balance-scale-right","icon-xl fas fa-ban","icon-xl fas fa-band-aid","icon-xl fas fa-barcode","icon-xl fas fa-bars","icon-xl fas fa-baseball-ball","icon-xl fas fa-basketball-ball","icon-xl fas fa-bath","icon-xl fas fa-battery-empty","icon-xl fas fa-battery-full","icon-xl fas fa-battery-half","icon-xl fas fa-battery-quarter","icon-xl fas fa-battery-three-quarters","icon-xl fas fa-bed","icon-xl fas fa-beer","icon-xl fas fa-bell","icon-xl fas fa-bell-slash","icon-xl fas fa-bezier-curve","icon-xl fas fa-bible","icon-xl fas fa-bicycle","icon-xl fas fa-biking","icon-xl fas fa-binoculars","icon-xl fas fa-biohazard","icon-xl fas fa-birthday-cake","icon-xl fas fa-blender","icon-xl fas fa-blender-phone","icon-xl fas fa-blind","icon-xl fas fa-blog","icon-xl fas fa-bold","icon-xl fas fa-bolt","icon-xl fas fa-bomb","icon-xl fas fa-bone","icon-xl fas fa-bong","icon-xl fas fa-book","icon-xl fas fa-book-dead","icon-xl fas fa-book-medical","icon-xl fas fa-book-open","icon-xl fas fa-book-reader","icon-xl fas fa-bookmark","icon-xl fas fa-border-all","icon-xl fas fa-border-none","icon-xl fas fa-border-style","icon-xl fas fa-bowling-ball","icon-xl fas fa-box","icon-xl fas fa-box-open","icon-xl fas fa-box-tissue","icon-xl fas fa-boxes","icon-xl fas fa-braille","icon-xl fas fa-brain","icon-xl fas fa-bread-slice","icon-xl fas fa-briefcase","icon-xl fas fa-briefcase-medical","icon-xl fas fa-broadcast-tower","icon-xl fas fa-broom","icon-xl fas fa-brush","icon-xl fas fa-bug","icon-xl fas fa-building","icon-xl fas fa-bullhorn","icon-xl fas fa-bullseye","icon-xl fas fa-burn","icon-xl fas fa-bus","icon-xl fas fa-bus-alt","icon-xl fas fa-business-time","icon-xl fas fa-calculator","icon-xl fas fa-calendar","icon-xl fas fa-calendar-alt","icon-xl fas fa-calendar-check","icon-xl fas fa-calendar-day","icon-xl fas fa-calendar-minus","icon-xl fas fa-calendar-plus","icon-xl fas fa-calendar-times","icon-xl fas fa-calendar-week","icon-xl fas fa-camera","icon-xl fas fa-camera-retro","icon-xl fas fa-campground","icon-xl fas fa-candy-cane","icon-xl fas fa-cannabis","icon-xl fas fa-capsules","icon-xl fas fa-car","icon-xl fas fa-car-alt","icon-xl fas fa-car-battery","icon-xl fas fa-car-crash","icon-xl fas fa-car-side","icon-xl fas fa-caravan","icon-xl fas fa-caret-down","icon-xl fas fa-caret-left","icon-xl fas fa-caret-right","icon-xl fas fa-caret-square-down","icon-xl fas fa-caret-square-left","icon-xl fas fa-caret-square-right","icon-xl fas fa-caret-square-up","icon-xl fas fa-caret-up","icon-xl fas fa-carrot","icon-xl fas fa-cart-arrow-down","icon-xl fas fa-cart-plus","icon-xl fas fa-cash-register","icon-xl fas fa-cat","icon-xl fas fa-certificate","icon-xl fas fa-chair","icon-xl fas fa-chalkboard","icon-xl fas fa-chalkboard-teacher","icon-xl fas fa-charging-station","icon-xl fas fa-chart-area","icon-xl fas fa-chart-bar","icon-xl fas fa-chart-line","icon-xl fas fa-chart-pie","icon-xl fas fa-check","icon-xl fas fa-check-circle","icon-xl fas fa-check-double","icon-xl fas fa-check-square","icon-xl fas fa-cheese","icon-xl fas fa-chess","icon-xl fas fa-chess-bishop","icon-xl fas fa-chess-board","icon-xl fas fa-chess-king","icon-xl fas fa-chess-knight","icon-xl fas fa-chess-pawn","icon-xl fas fa-chess-queen","icon-xl fas fa-chess-rook","icon-xl fas fa-chevron-circle-down","icon-xl fas fa-chevron-circle-left","icon-xl fas fa-chevron-circle-right","icon-xl fas fa-chevron-circle-up","icon-xl fas fa-chevron-down","icon-xl fas fa-chevron-left","icon-xl fas fa-chevron-right","icon-xl fas fa-chevron-up","icon-xl fas fa-child","icon-xl fas fa-church","icon-xl fas fa-circle","icon-xl fas fa-circle-notch","icon-xl fas fa-city","icon-xl fas fa-clinic-medical","icon-xl fas fa-clipboard","icon-xl fas fa-clipboard-check","icon-xl fas fa-clipboard-list","icon-xl fas fa-clock","icon-xl fas fa-clone","icon-xl fas fa-closed-captioning","icon-xl fas fa-cloud","icon-xl fas fa-cloud-download-alt","icon-xl fas fa-cloud-meatball","icon-xl fas fa-cloud-moon","icon-xl fas fa-cloud-moon-rain","icon-xl fas fa-cloud-rain","icon-xl fas fa-cloud-showers-heavy","icon-xl fas fa-cloud-sun","icon-xl fas fa-cloud-sun-rain","icon-xl fas fa-cloud-upload-alt","icon-xl fas fa-cocktail","icon-xl fas fa-code","icon-xl fas fa-code-branch","icon-xl fas fa-coffee","icon-xl fas fa-cog","icon-xl fas fa-cogs","icon-xl fas fa-coins","icon-xl fas fa-columns","icon-xl fas fa-comment","icon-xl fas fa-comment-alt","icon-xl fas fa-comment-dollar","icon-xl fas fa-comment-dots","icon-xl fas fa-comment-medical","icon-xl fas fa-comment-slash","icon-xl fas fa-comments","icon-xl fas fa-comments-dollar","icon-xl fas fa-compact-disc","icon-xl fas fa-compass","icon-xl fas fa-compress","icon-xl fas fa-compress-alt","icon-xl fas fa-compress-arrows-alt","icon-xl fas fa-concierge-bell","icon-xl fas fa-cookie","icon-xl fas fa-cookie-bite","icon-xl fas fa-copy","icon-xl fas fa-copyright","icon-xl fas fa-couch","icon-xl fas fa-credit-card","icon-xl fas fa-crop","icon-xl fas fa-crop-alt","icon-xl fas fa-cross","icon-xl fas fa-crosshairs","icon-xl fas fa-crow","icon-xl fas fa-crown","icon-xl fas fa-crutch","icon-xl fas fa-cube","icon-xl fas fa-cubes","icon-xl fas fa-cut","icon-xl fas fa-database","icon-xl fas fa-deaf","icon-xl fas fa-democrat","icon-xl fas fa-desktop","icon-xl fas fa-dharmachakra","icon-xl fas fa-diagnoses","icon-xl fas fa-dice","icon-xl fas fa-dice-d20","icon-xl fas fa-dice-d6","icon-xl fas fa-dice-five","icon-xl fas fa-dice-four","icon-xl fas fa-dice-one","icon-xl fas fa-dice-six","icon-xl fas fa-dice-three","icon-xl fas fa-dice-two","icon-xl fas fa-digital-tachograph","icon-xl fas fa-directions","icon-xl fas fa-disease","icon-xl fas fa-divide","icon-xl fas fa-dizzy","icon-xl fas fa-dna","icon-xl fas fa-dog","icon-xl fas fa-dollar-sign","icon-xl fas fa-dolly","icon-xl fas fa-dolly-flatbed","icon-xl fas fa-donate","icon-xl fas fa-door-closed","icon-xl fas fa-door-open","icon-xl fas fa-dot-circle","icon-xl fas fa-dove","icon-xl fas fa-download","icon-xl fas fa-drafting-compass","icon-xl fas fa-dragon","icon-xl fas fa-draw-polygon","icon-xl fas fa-drum","icon-xl fas fa-drum-steelpan","icon-xl fas fa-drumstick-bite","icon-xl fas fa-dumbbell","icon-xl fas fa-dumpster","icon-xl fas fa-dumpster-fire","icon-xl fas fa-dungeon","icon-xl fas fa-edit","icon-xl fas fa-egg","icon-xl fas fa-eject","icon-xl fas fa-ellipsis-h","icon-xl fas fa-ellipsis-v","icon-xl fas fa-envelope","icon-xl fas fa-envelope-open","icon-xl fas fa-envelope-open-text","icon-xl fas fa-envelope-square","icon-xl fas fa-equals","icon-xl fas fa-eraser","icon-xl fas fa-ethernet","icon-xl fas fa-euro-sign","icon-xl fas fa-exchange-alt","icon-xl fas fa-exclamation","icon-xl fas fa-exclamation-circle","icon-xl fas fa-exclamation-triangle","icon-xl fas fa-expand","icon-xl fas fa-expand-alt","icon-xl fas fa-expand-arrows-alt","icon-xl fas fa-external-link-alt","icon-xl fas fa-external-link-square-alt","icon-xl fas fa-eye","icon-xl fas fa-eye-dropper","icon-xl fas fa-eye-slash","icon-xl fas fa-fan","icon-xl fas fa-fast-backward","icon-xl fas fa-fast-forward","icon-xl fas fa-faucet","icon-xl fas fa-fax","icon-xl fas fa-feather","icon-xl fas fa-feather-alt","icon-xl fas fa-female","icon-xl fas fa-fighter-jet","icon-xl fas fa-file","icon-xl fas fa-file-alt","icon-xl fas fa-file-archive","icon-xl fas fa-file-audio","icon-xl fas fa-file-code","icon-xl fas fa-file-contract","icon-xl fas fa-file-csv","icon-xl fas fa-file-download","icon-xl fas fa-file-excel","icon-xl fas fa-file-export","icon-xl fas fa-file-image","icon-xl fas fa-file-import","icon-xl fas fa-file-invoice","icon-xl fas fa-file-invoice-dollar","icon-xl fas fa-file-medical","icon-xl fas fa-file-medical-alt","icon-xl fas fa-file-pdf","icon-xl fas fa-file-powerpoint","icon-xl fas fa-file-prescription","icon-xl fas fa-file-signature","icon-xl fas fa-file-upload","icon-xl fas fa-file-video","icon-xl fas fa-file-word","icon-xl fas fa-fill","icon-xl fas fa-fill-drip","icon-xl fas fa-film","icon-xl fas fa-filter","icon-xl fas fa-fingerprint","icon-xl fas fa-fire","icon-xl fas fa-fire-alt","icon-xl fas fa-fire-extinguisher","icon-xl fas fa-first-aid","icon-xl fas fa-fish","icon-xl fas fa-fist-raised","icon-xl fas fa-flag","icon-xl fas fa-flag-checkered","icon-xl fas fa-flag-usa","icon-xl fas fa-flask","icon-xl fas fa-flushed","icon-xl fas fa-folder","icon-xl fas fa-folder-minus","icon-xl fas fa-folder-open","icon-xl fas fa-folder-plus","icon-xl fas fa-font","icon-xl fas fa-font-awesome-logo-full","icon-xl fas fa-football-ball","icon-xl fas fa-forward","icon-xl fas fa-frog","icon-xl fas fa-frown","icon-xl fas fa-frown-open","icon-xl fas fa-funnel-dollar","icon-xl fas fa-futbol","icon-xl fas fa-gamepad","icon-xl fas fa-gas-pump","icon-xl fas fa-gavel","icon-xl fas fa-gem","icon-xl fas fa-genderless","icon-xl fas fa-ghost","icon-xl fas fa-gift","icon-xl fas fa-gifts","icon-xl fas fa-glass-cheers","icon-xl fas fa-glass-martini","icon-xl fas fa-glass-martini-alt","icon-xl fas fa-glass-whiskey","icon-xl fas fa-glasses","icon-xl fas fa-globe","icon-xl fas fa-globe-africa","icon-xl fas fa-globe-americas","icon-xl fas fa-globe-asia","icon-xl fas fa-globe-europe","icon-xl fas fa-golf-ball","icon-xl fas fa-gopuram","icon-xl fas fa-graduation-cap","icon-xl fas fa-greater-than","icon-xl fas fa-greater-than-equal","icon-xl fas fa-grimace","icon-xl fas fa-grin","icon-xl fas fa-grin-alt","icon-xl fas fa-grin-beam","icon-xl fas fa-grin-beam-sweat","icon-xl fas fa-grin-hearts","icon-xl fas fa-grin-squint","icon-xl fas fa-grin-squint-tears","icon-xl fas fa-grin-stars","icon-xl fas fa-grin-tears","icon-xl fas fa-grin-tongue","icon-xl fas fa-grin-tongue-squint","icon-xl fas fa-grin-tongue-wink","icon-xl fas fa-grin-wink","icon-xl fas fa-grip-horizontal","icon-xl fas fa-grip-lines","icon-xl fas fa-grip-lines-vertical","icon-xl fas fa-grip-vertical","icon-xl fas fa-guitar","icon-xl fas fa-h-square","icon-xl fas fa-hamburger","icon-xl fas fa-hammer","icon-xl fas fa-hamsa","icon-xl fas fa-hand-holding","icon-xl fas fa-hand-holding-heart","icon-xl fas fa-hand-holding-medical","icon-xl fas fa-hand-holding-usd","icon-xl fas fa-hand-holding-water","icon-xl fas fa-hand-lizard","icon-xl fas fa-hand-middle-finger","icon-xl fas fa-hand-paper","icon-xl fas fa-hand-peace","icon-xl fas fa-hand-point-down","icon-xl fas fa-hand-point-left","icon-xl fas fa-hand-point-right","icon-xl fas fa-hand-point-up","icon-xl fas fa-hand-pointer","icon-xl fas fa-hand-rock","icon-xl fas fa-hand-scissors","icon-xl fas fa-hand-sparkles","icon-xl fas fa-hand-spock","icon-xl fas fa-hands","icon-xl fas fa-hands-helping","icon-xl fas fa-hands-wash","icon-xl fas fa-handshake","icon-xl fas fa-handshake-alt-slash","icon-xl fas fa-handshake-slash","icon-xl fas fa-hanukiah","icon-xl fas fa-hard-hat","icon-xl fas fa-hashtag","icon-xl fas fa-hat-cowboy","icon-xl fas fa-hat-cowboy-side","icon-xl fas fa-hat-wizard","icon-xl fas fa-hdd","icon-xl fas fa-head-side-cough","icon-xl fas fa-head-side-cough-slash","icon-xl fas fa-head-side-mask","icon-xl fas fa-head-side-virus","icon-xl fas fa-heading","icon-xl fas fa-headphones","icon-xl fas fa-headphones-alt","icon-xl fas fa-headset","icon-xl fas fa-heart","icon-xl fas fa-heart-broken","icon-xl fas fa-heartbeat","icon-xl fas fa-helicopter","icon-xl fas fa-highlighter","icon-xl fas fa-hiking","icon-xl fas fa-hippo","icon-xl fas fa-history","icon-xl fas fa-hockey-puck","icon-xl fas fa-holly-berry","icon-xl fas fa-home","icon-xl fas fa-horse","icon-xl fas fa-horse-head","icon-xl fas fa-hospital","icon-xl fas fa-hospital-alt","icon-xl fas fa-hospital-symbol","icon-xl fas fa-hospital-user","icon-xl fas fa-hot-tub","icon-xl fas fa-hotdog","icon-xl fas fa-hotel","icon-xl fas fa-hourglass","icon-xl fas fa-hourglass-end","icon-xl fas fa-hourglass-half","icon-xl fas fa-hourglass-start","icon-xl fas fa-house-damage","icon-xl fas fa-house-user","icon-xl fas fa-hryvnia","icon-xl fas fa-i-cursor","icon-xl fas fa-ice-cream","icon-xl fas fa-icicles","icon-xl fas fa-icons","icon-xl fas fa-id-badge","icon-xl fas fa-id-card","icon-xl fas fa-id-card-alt","icon-xl fas fa-igloo","icon-xl fas fa-image","icon-xl fas fa-images","icon-xl fas fa-inbox","icon-xl fas fa-indent","icon-xl fas fa-industry","icon-xl fas fa-infinity","icon-xl fas fa-info","icon-xl fas fa-info-circle","icon-xl fas fa-italic","icon-xl fas fa-jedi","icon-xl fas fa-joint","icon-xl fas fa-journal-whills","icon-xl fas fa-kaaba","icon-xl fas fa-key","icon-xl fas fa-keyboard","icon-xl fas fa-khanda","icon-xl fas fa-kiss","icon-xl fas fa-kiss-beam","icon-xl fas fa-kiss-wink-heart","icon-xl fas fa-kiwi-bird","icon-xl fas fa-landmark","icon-xl fas fa-language","icon-xl fas fa-laptop","icon-xl fas fa-laptop-code","icon-xl fas fa-laptop-house","icon-xl fas fa-laptop-medical","icon-xl fas fa-laugh","icon-xl fas fa-laugh-beam","icon-xl fas fa-laugh-squint","icon-xl fas fa-laugh-wink","icon-xl fas fa-layer-group","icon-xl fas fa-leaf","icon-xl fas fa-lemon","icon-xl fas fa-less-than","icon-xl fas fa-less-than-equal","icon-xl fas fa-level-down-alt","icon-xl fas fa-level-up-alt","icon-xl fas fa-life-ring","icon-xl fas fa-lightbulb","icon-xl fas fa-link","icon-xl fas fa-lira-sign","icon-xl fas fa-list","icon-xl fas fa-list-alt","icon-xl fas fa-list-ol","icon-xl fas fa-list-ul","icon-xl fas fa-location-arrow","icon-xl fas fa-lock","icon-xl fas fa-lock-open","icon-xl fas fa-long-arrow-alt-down","icon-xl fas fa-long-arrow-alt-left","icon-xl fas fa-long-arrow-alt-right","icon-xl fas fa-long-arrow-alt-up","icon-xl fas fa-low-vision","icon-xl fas fa-luggage-cart","icon-xl fas fa-lungs","icon-xl fas fa-lungs-virus","icon-xl fas fa-magic","icon-xl fas fa-magnet","icon-xl fas fa-mail-bulk","icon-xl fas fa-male","icon-xl fas fa-map","icon-xl fas fa-map-marked","icon-xl fas fa-map-marked-alt","icon-xl fas fa-map-marker","icon-xl fas fa-map-marker-alt","icon-xl fas fa-map-pin","icon-xl fas fa-map-signs","icon-xl fas fa-marker","icon-xl fas fa-mars","icon-xl fas fa-mars-double","icon-xl fas fa-mars-stroke","icon-xl fas fa-mars-stroke-h","icon-xl fas fa-mars-stroke-v","icon-xl fas fa-mask","icon-xl fas fa-medal","icon-xl fas fa-medkit","icon-xl fas fa-meh","icon-xl fas fa-meh-blank","icon-xl fas fa-meh-rolling-eyes","icon-xl fas fa-memory","icon-xl fas fa-menorah","icon-xl fas fa-mercury","icon-xl fas fa-meteor","icon-xl fas fa-microchip","icon-xl fas fa-microphone","icon-xl fas fa-microphone-alt","icon-xl fas fa-microphone-alt-slash","icon-xl fas fa-microphone-slash","icon-xl fas fa-microscope","icon-xl fas fa-minus","icon-xl fas fa-minus-circle","icon-xl fas fa-minus-square","icon-xl fas fa-mitten","icon-xl fas fa-mobile","icon-xl fas fa-mobile-alt","icon-xl fas fa-money-bill","icon-xl fas fa-money-bill-alt","icon-xl fas fa-money-bill-wave","icon-xl fas fa-money-bill-wave-alt","icon-xl fas fa-money-check","icon-xl fas fa-money-check-alt","icon-xl fas fa-monument","icon-xl fas fa-moon","icon-xl fas fa-mortar-pestle","icon-xl fas fa-mosque","icon-xl fas fa-motorcycle","icon-xl fas fa-mountain","icon-xl fas fa-mouse","icon-xl fas fa-mouse-pointer","icon-xl fas fa-mug-hot","icon-xl fas fa-music","icon-xl fas fa-network-wired","icon-xl fas fa-neuter","icon-xl fas fa-newspaper","icon-xl fas fa-not-equal","icon-xl fas fa-notes-medical","icon-xl fas fa-object-group","icon-xl fas fa-object-ungroup","icon-xl fas fa-oil-can","icon-xl fas fa-om","icon-xl fas fa-otter","icon-xl fas fa-outdent","icon-xl fas fa-pager","icon-xl fas fa-paint-brush","icon-xl fas fa-paint-roller","icon-xl fas fa-palette","icon-xl fas fa-pallet","icon-xl fas fa-paper-plane","icon-xl fas fa-paperclip","icon-xl fas fa-parachute-box","icon-xl fas fa-paragraph","icon-xl fas fa-parking","icon-xl fas fa-passport","icon-xl fas fa-pastafarianism","icon-xl fas fa-paste","icon-xl fas fa-pause","icon-xl fas fa-pause-circle","icon-xl fas fa-paw","icon-xl fas fa-peace","icon-xl fas fa-pen","icon-xl fas fa-pen-alt","icon-xl fas fa-pen-fancy","icon-xl fas fa-pen-nib","icon-xl fas fa-pen-square","icon-xl fas fa-pencil-alt","icon-xl fas fa-pencil-ruler","icon-xl fas fa-people-arrows","icon-xl fas fa-people-carry","icon-xl fas fa-pepper-hot","icon-xl fas fa-percent","icon-xl fas fa-percentage","icon-xl fas fa-person-booth","icon-xl fas fa-phone","icon-xl fas fa-phone-alt","icon-xl fas fa-phone-slash","icon-xl fas fa-phone-square","icon-xl fas fa-phone-square-alt","icon-xl fas fa-phone-volume","icon-xl fas fa-photo-video","icon-xl fas fa-piggy-bank","icon-xl fas fa-pills","icon-xl fas fa-pizza-slice","icon-xl fas fa-place-of-worship","icon-xl fas fa-plane","icon-xl fas fa-plane-arrival","icon-xl fas fa-plane-departure","icon-xl fas fa-plane-slash","icon-xl fas fa-play","icon-xl fas fa-play-circle","icon-xl fas fa-plug","icon-xl fas fa-plus","icon-xl fas fa-plus-circle","icon-xl fas fa-plus-square","icon-xl fas fa-podcast","icon-xl fas fa-poll","icon-xl fas fa-poll-h","icon-xl fas fa-poo","icon-xl fas fa-poo-storm","icon-xl fas fa-poop","icon-xl fas fa-portrait","icon-xl fas fa-pound-sign","icon-xl fas fa-power-off","icon-xl fas fa-pray","icon-xl fas fa-praying-hands","icon-xl fas fa-prescription","icon-xl fas fa-prescription-bottle","icon-xl fas fa-prescription-bottle-alt","icon-xl fas fa-print","icon-xl fas fa-procedures","icon-xl fas fa-project-diagram","icon-xl fas fa-pump-medical","icon-xl fas fa-pump-soap","icon-xl fas fa-puzzle-piece","icon-xl fas fa-qrcode","icon-xl fas fa-question","icon-xl fas fa-question-circle","icon-xl fas fa-quidditch","icon-xl fas fa-quote-left","icon-xl fas fa-quote-right","icon-xl fas fa-quran","icon-xl fas fa-radiation","icon-xl fas fa-radiation-alt","icon-xl fas fa-rainbow","icon-xl fas fa-random","icon-xl fas fa-receipt","icon-xl fas fa-record-vinyl","icon-xl fas fa-recycle","icon-xl fas fa-redo","icon-xl fas fa-redo-alt","icon-xl fas fa-registered","icon-xl fas fa-remove-format","icon-xl fas fa-reply","icon-xl fas fa-reply-all","icon-xl fas fa-republican","icon-xl fas fa-restroom","icon-xl fas fa-retweet","icon-xl fas fa-ribbon","icon-xl fas fa-ring","icon-xl fas fa-road","icon-xl fas fa-robot","icon-xl fas fa-rocket","icon-xl fas fa-route","icon-xl fas fa-rss","icon-xl fas fa-rss-square","icon-xl fas fa-ruble-sign","icon-xl fas fa-ruler","icon-xl fas fa-ruler-combined","icon-xl fas fa-ruler-horizontal","icon-xl fas fa-ruler-vertical","icon-xl fas fa-running","icon-xl fas fa-rupee-sign","icon-xl fas fa-sad-cry","icon-xl fas fa-sad-tear","icon-xl fas fa-satellite","icon-xl fas fa-satellite-dish","icon-xl fas fa-save","icon-xl fas fa-school","icon-xl fas fa-screwdriver","icon-xl fas fa-scroll","icon-xl fas fa-sd-card","icon-xl fas fa-search","icon-xl fas fa-search-dollar","icon-xl fas fa-search-location","icon-xl fas fa-search-minus","icon-xl fas fa-search-plus","icon-xl fas fa-seedling","icon-xl fas fa-server","icon-xl fas fa-shapes","icon-xl fas fa-share","icon-xl fas fa-share-alt","icon-xl fas fa-share-alt-square","icon-xl fas fa-share-square","icon-xl fas fa-shekel-sign","icon-xl fas fa-shield-alt","icon-xl fas fa-shield-virus","icon-xl fas fa-ship","icon-xl fas fa-shipping-fast","icon-xl fas fa-shoe-prints","icon-xl fas fa-shopping-bag","icon-xl fas fa-shopping-basket","icon-xl fas fa-shopping-cart","icon-xl fas fa-shower","icon-xl fas fa-shuttle-van","icon-xl fas fa-sign","icon-xl fas fa-sign-in-alt","icon-xl fas fa-sign-language","icon-xl fas fa-sign-out-alt","icon-xl fas fa-signal","icon-xl fas fa-signature","icon-xl fas fa-sim-card","icon-xl fas fa-sink","icon-xl fas fa-sitemap","icon-xl fas fa-skating","icon-xl fas fa-skiing","icon-xl fas fa-skiing-nordic","icon-xl fas fa-skull","icon-xl fas fa-skull-crossbones","icon-xl fas fa-slash","icon-xl fas fa-sleigh","icon-xl fas fa-sliders-h","icon-xl fas fa-smile","icon-xl fas fa-smile-beam","icon-xl fas fa-smile-wink","icon-xl fas fa-smog","icon-xl fas fa-smoking","icon-xl fas fa-smoking-ban","icon-xl fas fa-sms","icon-xl fas fa-snowboarding","icon-xl fas fa-snowflake","icon-xl fas fa-snowman","icon-xl fas fa-snowplow","icon-xl fas fa-soap","icon-xl fas fa-socks","icon-xl fas fa-solar-panel","icon-xl fas fa-sort","icon-xl fas fa-sort-alpha-down","icon-xl fas fa-sort-alpha-down-alt","icon-xl fas fa-sort-alpha-up","icon-xl fas fa-sort-alpha-up-alt","icon-xl fas fa-sort-amount-down","icon-xl fas fa-sort-amount-down-alt","icon-xl fas fa-sort-amount-up","icon-xl fas fa-sort-amount-up-alt","icon-xl fas fa-sort-down","icon-xl fas fa-sort-numeric-down","icon-xl fas fa-sort-numeric-down-alt","icon-xl fas fa-sort-numeric-up","icon-xl fas fa-sort-numeric-up-alt","icon-xl fas fa-sort-up","icon-xl fas fa-spa","icon-xl fas fa-space-shuttle","icon-xl fas fa-spell-check","icon-xl fas fa-spider","icon-xl fas fa-spinner","icon-xl fas fa-splotch","icon-xl fas fa-spray-can","icon-xl fas fa-square","icon-xl fas fa-square-full","icon-xl fas fa-square-root-alt","icon-xl fas fa-stamp","icon-xl fas fa-star","icon-xl fas fa-star-and-crescent","icon-xl fas fa-star-half","icon-xl fas fa-star-half-alt","icon-xl fas fa-star-of-david","icon-xl fas fa-star-of-life","icon-xl fas fa-step-backward","icon-xl fas fa-step-forward","icon-xl fas fa-stethoscope","icon-xl fas fa-sticky-note","icon-xl fas fa-stop","icon-xl fas fa-stop-circle","icon-xl fas fa-stopwatch","icon-xl fas fa-stopwatch-20","icon-xl fas fa-store","icon-xl fas fa-store-alt","icon-xl fas fa-store-alt-slash","icon-xl fas fa-store-slash","icon-xl fas fa-stream","icon-xl fas fa-street-view","icon-xl fas fa-strikethrough","icon-xl fas fa-stroopwafel","icon-xl fas fa-subscript","icon-xl fas fa-subway","icon-xl fas fa-suitcase","icon-xl fas fa-suitcase-rolling","icon-xl fas fa-sun","icon-xl fas fa-superscript","icon-xl fas fa-surprise","icon-xl fas fa-swatchbook","icon-xl fas fa-swimmer","icon-xl fas fa-swimming-pool","icon-xl fas fa-synagogue","icon-xl fas fa-sync","icon-xl fas fa-sync-alt","icon-xl fas fa-syringe","icon-xl fas fa-table","icon-xl fas fa-table-tennis","icon-xl fas fa-tablet","icon-xl fas fa-tablet-alt","icon-xl fas fa-tablets","icon-xl fas fa-tachometer-alt","icon-xl fas fa-tag","icon-xl fas fa-tags","icon-xl fas fa-tape","icon-xl fas fa-tasks","icon-xl fas fa-taxi","icon-xl fas fa-teeth","icon-xl fas fa-teeth-open","icon-xl fas fa-temperature-high","icon-xl fas fa-temperature-low","icon-xl fas fa-tenge","icon-xl fas fa-terminal","icon-xl fas fa-text-height","icon-xl fas fa-text-width","icon-xl fas fa-th","icon-xl fas fa-th-large","icon-xl fas fa-th-list","icon-xl fas fa-theater-masks","icon-xl fas fa-thermometer","icon-xl fas fa-thermometer-empty","icon-xl fas fa-thermometer-full","icon-xl fas fa-thermometer-half","icon-xl fas fa-thermometer-quarter","icon-xl fas fa-thermometer-three-quarters","icon-xl fas fa-thumbs-down","icon-xl fas fa-thumbs-up","icon-xl fas fa-thumbtack","icon-xl fas fa-ticket-alt","icon-xl fas fa-times","icon-xl fas fa-times-circle","icon-xl fas fa-tint","icon-xl fas fa-tint-slash","icon-xl fas fa-tired","icon-xl fas fa-toggle-off","icon-xl fas fa-toggle-on","icon-xl fas fa-toilet","icon-xl fas fa-toilet-paper","icon-xl fas fa-toilet-paper-slash","icon-xl fas fa-toolbox","icon-xl fas fa-tools","icon-xl fas fa-tooth","icon-xl fas fa-torah","icon-xl fas fa-torii-gate","icon-xl fas fa-tractor","icon-xl fas fa-trademark","icon-xl fas fa-traffic-light","icon-xl fas fa-trailer","icon-xl fas fa-train","icon-xl fas fa-tram","icon-xl fas fa-transgender","icon-xl fas fa-transgender-alt","icon-xl fas fa-trash","icon-xl fas fa-trash-alt","icon-xl fas fa-trash-restore","icon-xl fas fa-trash-restore-alt","icon-xl fas fa-tree","icon-xl fas fa-trophy","icon-xl fas fa-truck","icon-xl fas fa-truck-loading","icon-xl fas fa-truck-monster","icon-xl fas fa-truck-moving","icon-xl fas fa-truck-pickup","icon-xl fas fa-tshirt","icon-xl fas fa-tty","icon-xl fas fa-tv","icon-xl fas fa-umbrella","icon-xl fas fa-umbrella-beach","icon-xl fas fa-underline","icon-xl fas fa-undo","icon-xl fas fa-undo-alt","icon-xl fas fa-universal-access","icon-xl fas fa-university","icon-xl fas fa-unlink","icon-xl fas fa-unlock","icon-xl fas fa-unlock-alt","icon-xl fas fa-upload","icon-xl fas fa-user","icon-xl fas fa-user-alt","icon-xl fas fa-user-alt-slash","icon-xl fas fa-user-astronaut","icon-xl fas fa-user-check","icon-xl fas fa-user-circle","icon-xl fas fa-user-clock","icon-xl fas fa-user-cog","icon-xl fas fa-user-edit","icon-xl fas fa-user-friends","icon-xl fas fa-user-graduate","icon-xl fas fa-user-injured","icon-xl fas fa-user-lock","icon-xl fas fa-user-md","icon-xl fas fa-user-minus","icon-xl fas fa-user-ninja","icon-xl fas fa-user-nurse","icon-xl fas fa-user-plus","icon-xl fas fa-user-secret","icon-xl fas fa-user-shield","icon-xl fas fa-user-slash","icon-xl fas fa-user-tag","icon-xl fas fa-user-tie","icon-xl fas fa-user-times","icon-xl fas fa-users","icon-xl fas fa-users-cog","icon-xl fas fa-users-slash","icon-xl fas fa-utensil-spoon","icon-xl fas fa-utensils","icon-xl fas fa-vector-square","icon-xl fas fa-venus","icon-xl fas fa-venus-double","icon-xl fas fa-venus-mars","icon-xl fas fa-vial","icon-xl fas fa-vials","icon-xl fas fa-video","icon-xl fas fa-video-slash","icon-xl fas fa-vihara","icon-xl fas fa-virus","icon-xl fas fa-virus-slash","icon-xl fas fa-viruses","icon-xl fas fa-voicemail","icon-xl fas fa-volleyball-ball","icon-xl fas fa-volume-down","icon-xl fas fa-volume-mute","icon-xl fas fa-volume-off","icon-xl fas fa-volume-up","icon-xl fas fa-vote-yea","icon-xl fas fa-vr-cardboard","icon-xl fas fa-walking","icon-xl fas fa-wallet","icon-xl fas fa-warehouse","icon-xl fas fa-water","icon-xl fas fa-wave-square","icon-xl fas fa-weight","icon-xl fas fa-weight-hanging","icon-xl fas fa-wheelchair","icon-xl fas fa-wifi","icon-xl fas fa-wind","icon-xl fas fa-window-close","icon-xl fas fa-window-maximize","icon-xl fas fa-window-minimize","icon-xl fas fa-window-restore","icon-xl fas fa-wine-bottle","icon-xl fas fa-wine-glass","icon-xl fas fa-wine-glass-alt","icon-xl fas fa-won-sign","icon-xl fas fa-wrench","icon-xl fas fa-x-ray","icon-xl fas fa-yen-sign","icon-xl fas fa-yin-yang"]';
        $icons = json_decode($icons,true);

        $str = "INSERT INTO ";
        $str .= "`";
        $str .= "icons";
        $str .= "`";
        $str .= " (`id`,`value`,`name`) \n";
        $str .= "VALUES \n";
        foreach($icons as $key => $icon){
                $str .= "\t(".($key+1).',\''.str_replace('icon-xl ','',$icon).'\',\''.str_replace('icon-xl ','',$icon).'\'),'." \n";
        }

        echo nl2br($str);exit;
    }
}

class PublicApi_Controller extends REST_Controller{
    public function __construct(){
        parent::__construct();

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: API-KEY, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header("Cache-Control: no-cache, must-revalidate");

        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }

        $this->load->helper(array('url', 'language','form','typography','app', 'permission', 'rest'));

        $this->load->database();
    }
}


class Api_Controller extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: API-KEY, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header("Cache-Control: no-cache, must-revalidate");

        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }

        $this->load->helper(array('url', 'language','form','typography','app', 'permission', 'rest'));

        if ($this->input->get_request_header('Api-Key')) {
            $apiKey = $this->input->get_request_header('Api-Key');
            if($apiKey != 'YsKqx8S0RCZqiWzAlgXI'){
                $this->response(array('error' => 403, "Message" => 'Not authorized'), 403);
            }
        }
        else{
            $this->response(array('error' => 403, "Message" => 'Not authorized'), 403);
        }

        $this->load->database();

    }
}
