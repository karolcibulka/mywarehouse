function __question(href,redirect,type,title,text,confirmButtonText,cancelButtonText){
    Swal.fire({
        title: title ? title : 'Are you sure?',
        text: text ? text : 'You want to delete it?',
        icon: type ? type :'warning',
        showCancelButton: true,
        confirmButtonText: confirmButtonText ? confirmButtonText :'Yes, delete it!',
        cancelButtonText: cancelButtonText ? cancelButtonText : 'Cancel',
    }).then(function(result) {
        if (result.value) {
            if(redirect){
                window.location.href = href;
            }
            else{
                __successMessage();
            }
        }
    });
}

function __successMessage(icon,title){
    Swal.fire({
        position: 'top-right',
        icon: icon ? icon : 'success',
        title: title ? title : 'Your work has been saved',
        showConfirmButton: false,
        timer: 1500
    });
}